// **************************************************
//
// Modification Tracking:
// $Revision:$
// $LastChangedBy:$
// $LastChangedDate:$
//
// **************************************************
package com.ibibl.searchengine.index;


/**
 * @author jdearmas
 * Trie datastructure to be used as an indexing structure of strings.
 * 
 * @since 1.0 
 */
public class TrieNode {

	/**
	 * The word that this node represents (if some word in the dictionary ends
	 * at this node.  If not, then null.
	 */
	private String word = null;
	
	/**
	 * The id of this string in the database.
	 */
	private Object id = null;
	
	/**
	 * The child nodes of this node.
	 */
	private TrieNodeMap children = new TrieNodeMap();
	
	/**
	 * Default constructor.
	 */
	public TrieNode(){
		
	}
	
	/**
	 * Inserts a new word to the trie.
	 * @param aWord the word to be inserted
	 */
	public void insert(String aWord){
		TrieNode node = this;
		
		for (int i = 0; i < aWord.length(); ++i){
			char a = aWord.charAt(i);
			if (!node.children.containsKey(a)){
				node.children.put(a, new TrieNode());
			}
			
			node = node.children.get(aWord.charAt(i));
		}
		
		node.word = aWord;
	}
	
	/**
	 * Inserts a new word with its id to the trie.
	 * @param aWord
	 * @param id
	 */
	public void insert(String aWord, Object id){
		TrieNode node = this;
		
		for (int i = 0; i < aWord.length(); ++i){
			char a = aWord.charAt(i);
			if (!node.children.containsKey(a)){
				node.children.put(a, new TrieNode());
			}
			
			node = node.children.get(aWord.charAt(i));
		}
		
		node.word = aWord;
		node.id = id;
	}
	
	/**
	 * Deletes a word from the trie.
	 * @param aWord The word to be deleted.
	 * @throws UnsupportedOperationException whenever the method is call
	 * because the method is not yet supported.
	 */
	public void delete(String aWord){
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Returns all the children node of this trie.
	 * @return the children
	 */
	public TrieNodeMap getChildren(){
		return this.children;
	}
	
	/**
	 * Returns the word corresponding to this node
	 * @return the String word.
	 */
	public String getWord(){
		return this.word;
	}
	
	/**
	 * Returns the id from database corresponding to this word.
	 * @return the id.
	 */
	public Object getId(){
		return this.id;
	}
	
}
