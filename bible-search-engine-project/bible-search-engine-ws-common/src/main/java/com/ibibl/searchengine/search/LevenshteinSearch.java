// **************************************************
//
// Modification Tracking:
// $Revision:$
// $LastChangedBy:$
// $LastChangedDate:$
//
// **************************************************
package com.ibibl.searchengine.search;

import java.util.ArrayList;
import java.util.List;

import com.ibibl.searchengine.index.Pair;
import com.ibibl.searchengine.index.TrieNode;
import com.ibibl.searchengine.index.TrieNodeMap;
import com.ibibl.searchengine.index.TrieNodePair;

/**
 * @author jdearmas
 *
 * @since 1.0  
 */
public class LevenshteinSearch {

	/**
	 * If true, the algorithm is case sensitive
	 */
	private static boolean CASE_SENSITIVE = false;
	
	/**
	 * Finds all the strings in the index (the trie) that are at a distance of
	 * less than or equal to maxCost of word word.
	 * @param word the word that we are searching for
	 * @param maxCost how far away from that word we search words.
	 * @param trie the datastructure that indexes all the words.
	 * @return The list of words that are withing a distance of maxCost from word.
	 */
	public static List<Pair<Object, String>> search(String word, int maxCost, TrieNode trie){
		
		//TODO: The results need to be a pair of string and distance. 
		List<Pair<Object, String>> results = new ArrayList<Pair<Object, String>>();
		
		int [] currentRow = new int[word.length()+1];
		for (int i = 0; i < currentRow.length; ++i){
			currentRow[i]=i;
		}
		
		TrieNodeMap children = trie.getChildren();
		TrieNodePair [] entries = children.entries();
		for (TrieNodePair entry : entries){
			char letter = entry.getLetter();
			TrieNode node = entry.getNode();
			
			searchRecursive(node, letter, word, currentRow, results, maxCost);
		}
			
		return results;
	}
	
	/**
	 * Recursive method for searching
	 * @param node the node from where we begin our search
	 * @param letter the letter we are considering at the moment
	 * @param word the word we are searching for
	 * @param previousRow the previous row of the dynamic programming Levenshtein alg.
	 * @param results The current results
	 * @param maxCost The distance.
	 */
	private static void searchRecursive(TrieNode node, char letter, String word,
			int [] previousRow, List<Pair<Object, String>> results, int maxCost){
		
		//1. Initialize required elements
		int columns = word.length() + 1;
		int [] currentRow = new int[word.length()+1];
		currentRow[0] = previousRow[0] + 1;
		
		//2. Build one row for the letter, with a column for each letter
		//   in the target word, plus one empty string at column 0.
		for (int column = 1; column < columns; ++column){
			int insertCost = currentRow[column - 1] + 1;
			int deleteCost = previousRow[column] + 1;
			
			char a = word.charAt(column - 1);
			char cletter = letter;
			int replaceCost = 0;
			a = CASE_SENSITIVE ? a : Character.toLowerCase(a);
			cletter = CASE_SENSITIVE ? letter : Character.toLowerCase(letter);
			
			if (a != cletter){
				replaceCost = previousRow[column - 1] + 1;
			}
			else{
				replaceCost = previousRow[column - 1];
			}
			
			currentRow[column] = Math.min(Math.min(insertCost,  deleteCost), replaceCost);
		}
		
		//3. If the last entry in the row indicates the optimal cost is less
		//than the maximum cost, and there is a word in this trie node, then 
		//add it
		if (currentRow[currentRow.length-1] <= maxCost && null != node.getWord()){
			results.add(new Pair<Object, String>(node.getId(), node.getWord()));
		}
		
		//4. If any entries in the row are less than the maximum cost,
		//then recursively search each branch of the trie:
		if (min(currentRow) <= maxCost){
			TrieNodeMap children = node.getChildren();
			TrieNodePair [] entries = children.entries();
			for (TrieNodePair entry : entries){
				char newLetter = entry.getLetter();
				TrieNode newNode = entry.getNode();
				
				searchRecursive(newNode, newLetter, word, currentRow, results, maxCost);
			}
		}
	}
	
	/**
	 * Returns the minimum value of the array.
	 * @param values An array with integer values.
	 * @return the minimum value of array. if the array has no values it returns
	 * Integer.MAX_VALUE
	 */
	private static int min(int [] values){
		int min = Integer.MAX_VALUE;
		for (int i = 0; i < values.length; ++i){
			if (min > values[i]){
				min = values[i];
			}
		}
		return min;
	}
}
