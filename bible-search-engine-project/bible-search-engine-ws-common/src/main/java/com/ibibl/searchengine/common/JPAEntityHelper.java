// **************************************************
//
// Modification Tracking:
// $Revision:$
// $LastChangedBy:$
// $LastChangedDate:$
//
// **************************************************

package com.ibibl.searchengine.common;

import java.lang.reflect.Field;
import java.lang.reflect.Method;


/**
 * This class will help with some reflection tasks on JPA entities.
 * @author jdearmas
 *
 * @since  1.0
 */
public class JPAEntityHelper {


	
	/**
	 * 
	 * @param klass
	 * @param id
	 * @throws NumberFormatException whenever the String provided cannot
	 * be converted to the proper data type.
	 * @return
	 */
	public static Object getIdObject(Class<?> klass, String id){
		Class<?> idType = JPAEntityHelper.getIdType(klass);
		if (idType.equals(String.class)){
			return id;
		}
		else if (idType.equals(Long.class)){
			return Long.parseLong(id);
		}
		else if (idType.equals(Integer.class)){
			return Integer.parseInt(id);
			
		}
		else if (idType.equals(Character.class)){
			if (id.length()>1) throw new IllegalArgumentException("The String id could not be converted to character");
			Character newId = id.charAt(0);
			return newId;
		}
		else{
			throw new IllegalArgumentException("The id class cannot be identified");
		}
	}

	/**
	 * Finds the method that has a ManyToOne annotation, and
	 * returns the name of the field for which that method is a getter.
	 * 
	 * ASSUMPTION: This function assume the standard Java way of naming setters
	 * and getters of fields of classes.
	 * 
	 * Returns null if no such method can be found.
	 * @param entityType The type of the class that will be explored
	 * @param joinColumnType The type of the field whose name we return
	 * @return the name of the field that has the ManyToOne annotation.
	 */
	public static String getJoinColumnFieldName(Class<?> entityType, 
			Class<?> joinColumnType){
		
		Method method = getManyToOneMethod(entityType, joinColumnType);
		String methodName = method.getName();
		StringBuilder fieldNameBuilder = new StringBuilder();
		if (methodName.startsWith("get")){
			String temp = methodName.substring(3);
			char a = Character.toLowerCase(temp.charAt(0));
			fieldNameBuilder.append(a);
			fieldNameBuilder.append(temp.substring(1));
			return fieldNameBuilder.toString();
		}
		else if (methodName.startsWith("is")){
			String temp = methodName.substring(2);
			char a = Character.toLowerCase(temp.charAt(0));
			fieldNameBuilder.append(a);
			fieldNameBuilder.append(temp.substring(0));
			return fieldNameBuilder.toString();
		}
		
		return null;
	}
	/**
	 * Returns the type of the id field of the entity
	 * @param entityType the entity type
	 * @return Returns the type of the entity
	 * @throws IllegalStateException whenever it is not true that the entity
	 * has exactly one @Id type. 
	 */
	public static Class<?> getIdType(Class<?> entityType) throws IllegalStateException {
		Method idMethod = getIdMethod(entityType);
		if (null == idMethod){
			Field idField = getIdField(entityType);
			if (null == idField){
				throw new IllegalStateException("The entity does not have "
						+ "a valid Id field");
			}
			return idField.getType();

		}
		return idMethod.getReturnType();
	}
	
	
	/**
	 * 
	 * @param entityType
	 * @param joinColumnType
	 * @return
	 */
	static Method getManyToOneMethod(Class<?> entityType, Class<?> joinColumnType){
		Method [] declaredMethods = entityType.getDeclaredMethods();
		Method joinMethod = null;
		for (Method method : declaredMethods){
			if (method.isAnnotationPresent(javax.persistence.ManyToOne.class)
					&& method.getReturnType().equals(joinColumnType)){
				
				joinMethod = method;
				break;
			}
		}
		
		return joinMethod;
	}

	/**
	 * Returns the Field that has the @Id annotation.
	 * Returns null if it is not true that exactly one
	 * field has that annotation.
	 * 
	 * @param entityType the type of the entity
	 * @return the field that contains the Id annotation
	 */
	static Field getIdField(Class<?> entityType){
		
		Field [] declaredFields = entityType.getDeclaredFields();
		Field idField = null;
		for (Field field : declaredFields){
			if (field.isAnnotationPresent(javax.persistence.Id.class)){
				if (null != idField){
					//There is more than one @Id annotation
					return null;
				}
				idField = field;
			}
		}
		return idField;
	}
	
	/**
	 * Returns the method that has the @Id annotation
	 * Returns null if it is not true that exactly one method
	 * has that annotation.
	 * @param entityType the type of the entity
	 * @return Returns the Method that is annotated with @Id or null if
	 * none or more than one of such methods exist.
	 */
	static Method getIdMethod(Class<?> entityType){

		Method [] declaredMethods = entityType.getDeclaredMethods();
		Method idMethod = null;
		for (Method method : declaredMethods){
			if (method.isAnnotationPresent(javax.persistence.Id.class)){
				if (null != idMethod){
					//There is more than one @Id annotation.
					return null;
				}
				idMethod = method;
			}
		}
		
		return idMethod;
	}
	
	public static String getIdFieldName(Class <?> entityType){
		Method method = getIdMethod(entityType);
		String methodName = method.getName();
		StringBuilder sb = new StringBuilder();
		sb.append(Character.toLowerCase(methodName.charAt(3)));
		sb.append(methodName.substring(4));
		return sb.toString();
	}
	
	/**
	 * Returns the method that sets the field of type fieldType in the
	 * association of type entityType
	 * @param entityType
	 * @param fieldType
	 * @return the Method to be invoked.
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 */
	public static Method getAssociationSetterMethod(Class<?> entityType, Class<?> fieldType) throws NoSuchMethodException, SecurityException{
		
		Method setterMethod = null;
		
		//1. Get the getter ManyToOne method for that field
		Method getterMethod = JPAEntityHelper.getManyToOneMethod(entityType, fieldType);
		
		//2. Get the setter method by changing the name
		//using the standards.
		String getterName = getterMethod.getName();
		StringBuilder setterBuilder = new StringBuilder();
		setterBuilder.append("set");
		setterBuilder.append(getterName.substring(3));

		//3. Return the setter method.
		setterMethod = entityType.getMethod(setterBuilder.toString(), fieldType);
		return setterMethod;
	}
	
}

