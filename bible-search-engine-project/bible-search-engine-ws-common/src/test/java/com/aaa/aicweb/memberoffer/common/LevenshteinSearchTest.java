// **************************************************
//
// Modification Tracking:
// $Revision:$
// $LastChangedBy:$
// $LastChangedDate:$
//
// **************************************************
package com.aaa.aicweb.memberoffer.common;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import com.ibibl.searchengine.index.Pair;
import com.ibibl.searchengine.index.TrieNode;
import com.ibibl.searchengine.search.LevenshteinSearch;

/**
 * @author jdearmas
 *
 * @since  
 */
public class LevenshteinSearchTest {
	
	public static void main(String [] args){
		
		System.out.println("Generating random strings");
		
		//1. Generate 50 000 random strings:
		
		String [] strings = {"Jadiel", "Jadie", "Jad", "cake", "caking", "asdf", "asdfs",
				"asdffd"};
				 
		/**
		int size = 100000;
		String AB = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 ,.";
		Random rnd = new Random();
		String [] strings = new String[size];
		for (int i = 0 ; i < strings.length; ++i){
			int sLength = 50;
			StringBuilder sb = new StringBuilder(sLength);
			for (int j = 0; j < sLength; ++j){
				sb.append(AB.charAt(rnd.nextInt(AB.length())));
			}
			strings[i] = sb.toString();
		}
		**/
		
		System.out.println("Creating trie");
		long startTime = System.currentTimeMillis();
		//2. Create a Trie Index
		TrieNode root = new TrieNode();
		for (String word : strings){
			root.insert(word);
		}
		long stopTime = System.currentTimeMillis();
		System.out.println("Time creating trie: "+(stopTime - startTime));
		
		System.out.println("Finding similar words");
		String targetWord = "asdfd";
		//3. Find the similar words.
		int distance = 4;
		startTime = System.currentTimeMillis();
		List<Pair<Object, String>> similarWords = LevenshteinSearch.search(targetWord, distance, root);
		stopTime = System.currentTimeMillis();
		System.out.println("Time finding similar words: "+(stopTime - startTime));
		
		System.out.println("\n\nSIMILAR WORDS: ");
		for (Pair<Object, String> pair : similarWords){
			System.out.println("\t"+pair.getSecond());
		}
			
				
	}
}
