// **************************************************
//
// Modification Tracking:
// $Revision:$
// $LastChangedBy:$
// $LastChangedDate:$
//
// **************************************************
package com.aaa.aicweb.memberoffer.common;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;

import org.junit.Test;

import com.ibibl.searchengine.common.JPAEntityHelper;


/**
 * @author jdearmas
 *
 * @since 1.0  
 */
public class JPAEntityHelperTest {

	public static void main(String[] args) throws NoSuchMethodException, SecurityException{
		
		//Class<?> associationClass = SycsCoordinatorClub.class;
		//Class<?> fieldClass = SycsCoordinator.class;
		//Method method = associationClass.getDeclaredMethod("setCoordinator", SycsCoordinator.class);
		
		//System.out.println("methodName: "+method.getName());
		try {
			
			//Method method1 = JPAEntityHelper.getAssociationSetterMethod(associationClass, fieldClass);
			
			//System.out.println(method.getName());
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Tests that the right class type is returned
	 */
	//@Test
	public void testEntityWithId(){
		
		
		//1. Testing an String class
		//Class<?> idClass = JPAEntityHelper.getIdType(Club.class);
		//assertTrue("The idClass is not Long", idClass.equals(String.class));
		
		//2. Testing a Long class
		//Class<?> idClass1 = JPAEntityHelper.getIdType(SycsCoordinator.class);
		//assertTrue("The idClass is not Long", idClass1.equals(Long.class));
		
	}
	
	/**
	 * Tests that exception is thrown.
	 */
	//@Test
	public void testThrowingException(){
		
		boolean thrown = false;
		try{
			//JPAEntityHelper.getIdType(SycsCoordinatorClub.class);
		}
		catch(IllegalStateException e){
			thrown = true;
		}
		
		assertTrue("The IllegalStateException was not thrown", thrown);
		
	}
}
