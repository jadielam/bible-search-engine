package com.ibibl.searchengine.ejb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.ibibl.bible.BibleReference;
import com.ibibl.bible.Reference;

public class AppFrequencyInsertion {

	
	public static void main(String [] args) throws Exception{
		FrequencyInsertion insertion = new FrequencyInsertion();
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("BiblePersistence");
		EntityManager em = emf.createEntityManager();
		BibleReferenceDatabaseEJB ejb = new BibleReferenceDatabaseEJB(em);
		em.getTransaction().begin();
		Map<BibleReference, Integer> freqMap = insertion.
				calculateFrequency("/home/jadiel/total_crawled/");
		
		Set<Entry<BibleReference, Integer>> entrySet = freqMap.entrySet();
		int counter = 0;
		System.out.println(entrySet.size());
		for (Entry<BibleReference, Integer> e : entrySet){
			BibleReference ref = e.getKey();
			Integer freq = e.getValue();
			ejb.updateBibleVerseFreq(ref, freq);
			counter++;
			if (counter % 100 == 0){
				System.out.println(counter);
			}
		}
		
		em.flush();
		em.getTransaction().commit();
		em.close();
		emf.close();
		
		
	}
	
	private static void findBibleReference(String a){
		
	}
	
	private static void printMap(Map<Reference, Integer> map){
		List<Pair<Reference, Integer>> sortedReferences = new ArrayList<Pair<Reference, Integer>>();
		
		for (Reference ref : map.keySet()){
			Pair<Reference, Integer> pair = new Pair<Reference, Integer>(ref, map.get(ref));
			sortedReferences.add(pair);
			
		}
		Collections.sort(sortedReferences, (p1, p2) -> Integer.compare(p1.getSecond(), p2.getSecond())); //Note that we are doing reverse ordering here.
		
		int count = 0;
		for (int i = sortedReferences.size()-1; i >=0; i--){
			Pair<Reference, Integer> pair = sortedReferences.get(i);
			String refText = pair.getFirst().toString();
			Integer freq = pair.getSecond();
			System.out.println(refText + ": "+ freq);
			count++;
			if (count> 200) break;
		}
		
	}
}


class Pair<F, S> {
    private final F first; //first member of pair
    private final S second; //second member of pair
    
    public Pair(F first, S second) {
        this.first = first;
        this.second = second;
    }

    public F getFirst() {
        return first;
    }

    public S getSecond() {
        return second;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((first == null) ? 0 : first.hashCode());
		result = prime * result + ((second == null) ? 0 : second.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pair other = (Pair) obj;
		if (first == null) {
			if (other.first != null)
				return false;
		} else if (!first.equals(other.first))
			return false;
		if (second == null) {
			if (other.second != null)
				return false;
		} else if (!second.equals(other.second))
			return false;
		return true;
	}
    
    
}