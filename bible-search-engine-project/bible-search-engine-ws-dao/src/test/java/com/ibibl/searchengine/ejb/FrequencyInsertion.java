package com.ibibl.searchengine.ejb;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.regex.Matcher;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.ibibl.bible.BibleOutline;
import com.ibibl.bible.BibleReference;
import com.ibibl.bible.BibleVersion;
import com.ibibl.bible.NIVBibleOutline;
import com.ibibl.bible.Reference;
import com.ibibl.bible.parser.BibleReferenceLexer;
import com.ibibl.bible.parser.BibleReferenceListenerImpl;
import com.ibibl.bible.parser.BibleReferenceParser;
import com.ibibl.bible.parser.BibleReferenceReg;
import com.ibibl.bible.parser.BibleWorksFormatParser;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

public class FrequencyInsertion {
	
	public FrequencyInsertion(){
		
	}
	
	public Map<BibleReference, Integer> calculateFrequency(String folderPath) throws Exception{
		Map<BibleReference, Integer> freqMap = new HashMap<BibleReference, Integer>();
		
		//1. For each file in the folder
		File folder = new File(folderPath);
		File[] listOfFiles = folder.listFiles();
			
		BibleOutline outline = NIVBibleOutline.getInstance();

		for (File file : listOfFiles){
			String content = Files.toString(file, Charsets.UTF_8);
			Matcher matcher = BibleReferenceReg.REF_PATTERN.matcher(content);
			
			while (matcher.find()){
				String refString = matcher.group();
				List<Reference> references = this.parse(refString, outline);
				for (Reference ref : references){
					
					List<BibleReference> bibleReferences = ref.getBibleReferences();
					for (BibleReference bref : bibleReferences){
						if (freqMap.containsKey(bref)){
							freqMap.put(bref, freqMap.get(ref)+1);
						}
						else{
							freqMap.put(bref, 1);
						}

					}
				}
			}
		}
		//2. Return the map.
		return freqMap;
		
	}
	
	private List<Reference> parse(String refString, BibleOutline outline){
		ANTLRInputStream input = new ANTLRInputStream(refString);
		BibleReferenceLexer lexer = new BibleReferenceLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		BibleReferenceParser parser = new BibleReferenceParser(tokens);
		ParseTree tree = parser.ref();
		
		ParseTreeWalker walker = new ParseTreeWalker();
		List<Reference> references = new ArrayList<Reference>();
		BibleReferenceListenerImpl listener = new BibleReferenceListenerImpl(outline, references);
		walker.walk(listener, tree);
		references = listener.getCollectedReferences();
		return references;
	}
}
