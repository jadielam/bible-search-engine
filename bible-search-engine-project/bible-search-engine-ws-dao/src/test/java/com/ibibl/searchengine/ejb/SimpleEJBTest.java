// **************************************************
//
// Modification Tracking:
// $Revision:$
// $LastChangedBy:$
// $LastChangedDate:$
//
// **************************************************
package com.ibibl.searchengine.ejb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Collection;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TransactionRequiredException;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ibibl.searchengine.ejb.SimpleEJB;


/**
 * @author jdearmas
 * @param <T> This is the runtime class of the EJB to be tested
 * @param <S> The is the runtime class of the persistence entity whose
 * EJB will be tested.
 *
 * @since 1.0
 */
public abstract class SimpleEJBTest<T extends SimpleEJB<S>, S extends Object> {
	
	/**
	 * The entity manager to be passed to the EJB.
	 */
	static EntityManager entityManagerEJB;
	
	/**
	 * The entity manager factory.
	 */
	static EntityManagerFactory emFactory;
	
	/**
	 * The entity manager used by JUnit tests for cleaning purposes.
	 */
	static EntityManager entityManagerJUnit;
	
	/**
	 * The ejb to be tested.
	 */
	T ejb;
	
	/**
	 * The class type of entity S
	 */
	Class<S> entityKlass;
	
	/**
	 * Object to be added always to the database
	 */
	S addObject1;
	
	/**
	 * A duplicate object that has the same business key as
	 * addObject1, but potentially no primary key, if it is
	 * that the business key is not the primary key of this
	 * kind of entities.
	 */
	S duplicateObject1;
	
	/**
	 * The query to delete all entities of type S in the persistence context.
	 */
	private static String DELETE_QUERY_TEMPLATE = "DELETE FROM {0}";
	
	/**
	 * The query to find all the entities of type S in the persistence context.
	 */
	private static String FINDALL_QUERY_TEMPLATE = "SELECT e FROM {0} e";
	
	/**
	 * Initializes the ejb
	 */
	
	public abstract void initializeEJB();
	
	/**
	 * Sets the class of the entity of type S
	 */
	
	public abstract void setEntityClass();
	
	
	//The following methods will initialize all the entities that
	//will be used in the test.
	
	/**
	 * Initializes addObject1
	 */
	@Before
	public abstract void initializeAddObject();
	
	/**
	 * Initializes duplicateObject1
	 */
	@Before
	public abstract void initializeDuplicateObject();
	
	/**
	 * Takes the original object and makes a new 
	 * object that has the same primary key as the original
	 * but whose all business key fields have been changed, 
	 * 
	 * If the business key and the primary key are the same,
	 * we simply keep the same business key.
	 * 
	 * @param originalObject the object to be blueprinted for update
	 * @return an to be used as update object
	 */
	abstract S createUpdateObject(S originalObject);
	
	abstract Serializable getAddedEntityId();
	
	/**
	 * Initializes the entity manager
	 */
	@BeforeClass
	public static void initializePersistence(){
		
		SimpleEJBTest.emFactory = Persistence.createEntityManagerFactory("BiblePersistence");
		SimpleEJBTest.entityManagerEJB = emFactory.createEntityManager();
		SimpleEJBTest.entityManagerJUnit = emFactory.createEntityManager();
		
	}

	/**
	 * Closes the entity manager.
	 * 
	 */
	@AfterClass
	public static void closePersistenceResources(){
		entityManagerEJB.close();
		entityManagerJUnit.close();
		emFactory.close();
	}
	
	/**
	 * Removes all entities of type S from the database.
	 */
	@Before
	public void removeAllEntities(){
	
		setEntityClass();
		initializeEJB();
		
		String queryString = MessageFormat.format(DELETE_QUERY_TEMPLATE, 
				this.entityKlass.getSimpleName());
		
		SimpleEJBTest.entityManagerJUnit.getTransaction().begin();
		Query q = SimpleEJBTest.entityManagerJUnit.createQuery(queryString);
		q.executeUpdate();
		SimpleEJBTest.entityManagerJUnit.getTransaction().commit();
		
	}
	
	/**
	 * Clears the entity manager of the ejb.
	 */
	@Before
	public void cleanEJBEntityManager(){
		entityManagerEJB.clear();
	}
	/**
	 * Tests if an element is added to the database:
	 * 1. Adds object to the database.
	 * 2. Makes findall query and checks that there is one
	 * element in the list.
	 * 3. Compares element received with element added to see if they
	 * are equal.
	 */
	//@Test
	public void testAdd() throws Exception{
		
		
		//1. Adding object to database
		
		try{
			this.beginTransaction();
			this.ejb.add(this.addObject1);
			this.closeTransaction();
		}
		catch (Exception e){
		
			throw e;
		}
		
		this.beginTransaction();
		//S received = this.ejb.getById(this.entityKlass, getAddedEntityId());
		this.closeTransaction();
		//assertEquals("The elements are not equal to each other", received, this.addObject1);
	}
	
	/**
	 * 1. Adds an entity to the database.
	 * 2. Then adds a duplicate entity.  Here the duplicate entity is duplicate by having
	 * the same business key than the original entity.
	 * 3. The database should throw an exception saying that this is a duplicate.
	 * @throws Exception 
	 */
	//@Test
	public void testAddDuplicate() throws Exception{
		
		//1. Adds the entity to the database.
		
		try{
			this.beginTransaction();
			this.ejb.add(this.addObject1);
			this.closeTransaction();
		}
		catch (Exception e){
		
			throw e;
		}
		
		//2. Check that an execption is thrown
		//I am not using @Test(expectedException = Exception.class) because
		//I still not know the type of the exception that might be thrown.
		boolean thrown = false;
		try{
			this.beginTransaction();
			this.ejb.add(this.duplicateObject1);
			this.closeTransaction();
		}
		catch (Exception e){
			thrown = true;
			e.printStackTrace();
		}
		
		assertTrue(thrown);
	}
	
	/**
	 * 1. Adds the entity to the database.
	 * 2. Then adds a duplicate entity.  Here we are testing the inverse of the
	 * previous method: THe entity has the same primary key as the entity
	 * that we just inserted, but all its business key elements are different.
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws TransactionRequiredException 
	 * @throws EntityExistsException 
	 * @throws InstantiationException 
	 */
	//@Test
	public void testAddDuplicate1() throws EntityExistsException, TransactionRequiredException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		//1. Adds the entity to the database.
	
		this.beginTransaction();
		this.ejb.add(this.addObject1);
		this.closeTransaction();
			
		//2. Check that an execption is thrown
		//I am not using @Test(expectedException = Exception.class) because
		//I still not know the type of the exception that might be thrown.
		boolean thrown = false;
		S duplicateObject = this.createUpdateObject(this.addObject1);
		
		try{
			this.beginTransaction();
			this.ejb.add(duplicateObject);
			this.closeTransaction();
		}
		catch (Exception e){
			thrown = true;
		}
		
		assertTrue(thrown);

	}
	
	/**
	 * 1. Adds entity to database.
	 * 2. Creates a new entity with the same primary key as the entity just added,
	 * but with all the business key fields changed.
	 * 3. Updates a field in the entity and calls update in the database.
	 * 4. Checks that the previous entity and that this entity are equal.
	 * 
	 * This one needs to be implemented by the child class, because
	 * we need to modify one of the fields of the entity, and I don't want to use
	 * reflection to do that.
	 * 
	 * This test fails because of this bug:
	 * https://hibernate.atlassian.net/browse/HHH-6813
	 * 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws TransactionRequiredException 
	 * @throws EntityExistsException 
	 * @throws InstantiationException 
	 */
	//@Test
	public void testUpdateEntity() throws EntityExistsException, TransactionRequiredException, IllegalArgumentException, IllegalAccessException, InstantiationException{
		
		//1. Adds entity to the database
		this.beginTransaction();
		this.ejb.add(this.addObject1);
		this.closeTransaction();
		
		//2. Create the update entity
		S updateEntity = this.createUpdateObject(this.addObject1);
		
		//If they are not equal, this means that their primary key and
		//their business key are different.
		if (!updateEntity.equals(this.addObject1)){
			//3. Updates the entity in the database.
			this.beginTransaction();
			this.ejb.update(updateEntity);
			this.closeTransaction();
			
			assertEquals("The entity was not updated in the database", updateEntity,
					this.addObject1);
		}
	}
	
	/**
	 * 1. Adds an entity to the database.
	 * 2. Removes entity from the database.
	 * 3. Does a findall query to corroborate that entity was removed.
	 * @throws Exception 
	 */
	//@Test
	public void testRemoveEntity() throws Exception{
		
		//1. Add entity to database
		this.beginTransaction();
		this.ejb.add(this.addObject1);
		this.closeTransaction();
				
		//2. Remove entity from database.
		try{
			this.beginTransaction();
			//this.ejb.remove(this.entityKlass, this.getAddedEntityId());
			this.closeTransaction();
		
		}
		catch(Exception e){
			fail("The element that was inserted couldn't be removed,"
					+ "because exception was thrown: "+e.toString());
		}
		
		//3. Does a find entity to be sure that the entity was removed.
		this.beginTransaction();
		//S foundEntity = this.ejb.getById(this.entityKlass, this.getAddedEntityId());
		this.closeTransaction();
		//assertNull("The object was not removed", foundEntity);
	}
	
	/**
	 * 1. Adds an entity to the database
	 * 2. Calls the byId method and asserts that the object returned is not null
	 * @throws Exception 
	 */
	//@Test
	public void testFindEntity() throws Exception{
		
		//1. Add entity to database
		this.beginTransaction();
		this.ejb.add(this.addObject1);
		this.closeTransaction();
				
		//2. Find entity in database
		//S foundEntity = this.ejb.getById(this.entityKlass, this.getAddedEntityId());
				
		//assertNotNull("The object was not found", foundEntity);
	}
	
	
	/**
	 * Tests that nothing is found at the beginning when nothing is added
	 * @throws Exception 
	 */
	//@Test
	public void testFindAllEntities() throws Exception{
		
		try {
			
			this.beginTransaction();
			Collection<S> entries = this.ejb.findAll(this.entityKlass, null, null);
			this.closeTransaction();
			assertEquals("Items were found when none should be there", entries.size(), 0);
		} catch (Exception e) {
			
			throw e;
			//fail("Some exception was thrown");
		}
	}
	
	/**
	 * Tests that one entity that has been inserted is found
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws TransactionRequiredException 
	 * @throws EntityExistsException 
	 * @throws InstantiationException 
	 */
	//@Test
	public void testFindAllEntities1() throws EntityExistsException, TransactionRequiredException, IllegalArgumentException, IllegalAccessException, InstantiationException{
		
		this.beginTransaction();
		this.ejb.add(this.addObject1);
		this.closeTransaction();
	
		try {
			this.beginTransaction();
			Collection<S> entries = this.ejb.findAll(this.entityKlass, null, null);
			this.closeTransaction();
			assertEquals("The number of items found was not 1", entries.size(), 1);
		}
		catch (Exception e){
			e.printStackTrace();
			fail("Some exception was thrown");
		}
	}
	
	/**
	 * Begins a new transaction
	 */
	private void beginTransaction(){
		EntityTransaction transaction = entityManagerEJB.getTransaction();
		if (!transaction.isActive()){
			transaction.begin();
		}
	}
	
	/**
	 * Commits or rollbacks the transaction
	 */
	private void closeTransaction(){
		EntityTransaction transaction = entityManagerEJB.getTransaction();
		if (transaction.getRollbackOnly()){
			transaction.rollback();
			return;
		}
		
		if (transaction.isActive()){
			transaction.commit();
		}
	}
}
