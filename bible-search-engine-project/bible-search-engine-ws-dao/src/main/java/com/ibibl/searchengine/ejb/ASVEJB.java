package com.ibibl.searchengine.ejb;


import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.ibibl.bible.ASVBibleOutline;

@Stateless
@LocalBean
public class ASVEJB extends BibleEJB{
	
	public ASVEJB(){
		super(ASVBibleOutline.getInstance(), "ASV");
	}

}
