package com.ibibl.searchengine.ejb;

import com.ibibl.bible.R95BibleOutline;

public class R95EJB extends BibleEJB {

	public R95EJB(){
		super(R95BibleOutline.getInstance(), "R95");
	}
}
