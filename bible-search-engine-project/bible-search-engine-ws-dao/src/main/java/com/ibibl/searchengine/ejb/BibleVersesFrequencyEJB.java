package com.ibibl.searchengine.ejb;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Singleton;

import com.ibibl.searchengine.model.BibleVerseModel;

@Stateless
@LocalBean
public class BibleVersesFrequencyEJB {

	@EJB
	@Singleton
	SimpleEJB<BibleVerseModel> ejb;
	
	private Map<Integer, Integer> freqMap;
	
	public BibleVersesFrequencyEJB() {
		this.freqMap = new HashMap<Integer, Integer>();
	}
	
	@PostConstruct
	private void populateFrequencyMap() throws Exception {
		Collection<BibleVerseModel> allVerses = this.ejb.findAll(BibleVerseModel.class, null, null);
		Map<Integer, Integer> tempFreqMap = new HashMap<Integer, Integer>();
		for (BibleVerseModel verse : allVerses){
			tempFreqMap.put(verse.getId(), verse.getFreq());
		}
		this.freqMap = Collections.unmodifiableMap(tempFreqMap);
	}
	
	public Map<Integer, Integer> getFrequencyMap(){
		return this.freqMap;
	}
	
	public Integer getVerseFrequency(Integer id){
		return this.freqMap.get(id);
	}
	
}
