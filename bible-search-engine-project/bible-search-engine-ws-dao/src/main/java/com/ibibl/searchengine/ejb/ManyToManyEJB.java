// **************************************************
//
// Modification Tracking:
// $Revision:$
// $LastChangedBy:$
// $LastChangedDate:$
//
// **************************************************
package com.ibibl.searchengine.ejb;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import com.ibibl.searchengine.common.JPAEntityHelper;

/**
 *
 * @author JDeArmas
 * @param <R> Association class
 * @param <S> First entity class
 * @param <T> Second entity class
 * @since 1.0
 */
@Stateless
@LocalBean
public class ManyToManyEJB<R extends Object, S extends Object, T extends Object> {

	/**
	 * Logger of all the transactions performed by the EJB
	 */
	private static Logger LOGGER = Logger.getLogger(ManyToManyEJB.class.getName());
	
	/**
	 * This entity manager is container managed.
	 */
	@PersistenceContext(unitName = "BiblePersistence")
	private EntityManager entityManager;
	
	/**
	 * The template for the addAssociation query
	 * 
	 * INSERT INTO AssociationTable(Id1, Id2, Other_fields)
	 * 
	 */
	
	
	/**
	 * Default constructor
	 */
	public ManyToManyEJB (){
		
		
	}
	
	/**
	 * Adds a relationship to the association between entities
	 * of type S and T.
	 * @param sId The id of entity of type S
	 * @param klassS The type of the S entity
	 * @param tId The id of entity of type T
	 * @param klassT The type of the T entity
	 * @throws IllegalArgumentException if klassS or klassT don't
	 * denote an entity type, or if sId OR tId are not a valid type
	 * for the corresponding entity's primary key, or if sId OR tId
	 * are null, or if the association class does not have proper setter methods
	 * for that type.
	 */
	public void addRelationship(Class<R> associationClass, Class<S> klass1, String id1, 
			Class<T> klass2, String id2, Map<String, String> queryParams) 
			throws InstantiationException, ClassNotFoundException, 
			IllegalAccessException{
		
		try{
			
			//1. Find the first element
			S s = null;
			Object newId1 = JPAEntityHelper.getIdObject(klass1, id1);
			s = this.entityManager.find(klass1, newId1);
			
			//2. Find the second element
			T t = null;
			Object newId2 = JPAEntityHelper.getIdObject(klass2, id2);
			t = this.entityManager.find(klass2, newId2);
			
			//3. Create an association class
			Object association = Class.forName(associationClass.getName()).newInstance();
			
			//4. Use the setter methods to associate the two elements above
			Method klass1Setter = JPAEntityHelper.getAssociationSetterMethod(associationClass, klass1);
			Method klass2Setter = JPAEntityHelper.getAssociationSetterMethod(associationClass, klass2);
			
			klass1Setter.invoke(association, s);
			klass2Setter.invoke(association, t);
			
			//5. persist the association to the database.
			try{
				this.entityManager.merge(association);
			}
			catch(javax.persistence.EntityExistsException e){
				//Ignore the exception because that means that the
				//entry is already in the database.
			}
			
		}
		catch (IllegalArgumentException iae){
			throw iae;
		}
		catch(IllegalAccessException iae){
			throw new IllegalArgumentException();
		}
		catch (ClassNotFoundException cnf){
			throw new IllegalArgumentException();
		} catch (NoSuchMethodException e) {
			throw new IllegalArgumentException();
		} catch (SecurityException e) {
			throw new IllegalArgumentException();
		} catch (InvocationTargetException e) {
			throw new IllegalArgumentException();
		}
	}
	
	/**
	 * This method assumes that the ManyToOne Association was done using 
	 * a class 
	 * @param associationClass
	 * @param klass1
	 * @param id1
	 * @param klass2
	 * @param id2
	 */
	public void removeRelationship(Class<R> associationClass, Class<S> klass1, String id1, Class<T> klass2, String id2){
		
		//1. Instantiate the Composite key class.
		Method [] methods = associationClass.getDeclaredMethods();
		
		Class<?> embeddedIdType = null;
		for (Method method : methods){
			if (null != method.getAnnotation(javax.persistence.EmbeddedId.class)){
				embeddedIdType = method.getReturnType(); 
				break;
			}
		}
		if (null == embeddedIdType){
			throw new IllegalArgumentException();
		}
		
		Object embeddedId = null;
		try {
			embeddedId = embeddedIdType.newInstance();
		} catch (InstantiationException e) {
			throw new IllegalArgumentException();
		} catch (IllegalAccessException e) {
			throw new IllegalArgumentException();
		}
		if (null == embeddedId){
			throw new IllegalArgumentException();
		}	
		
		
		//2. Find the method to set key 1 and set it
		//3. Find the method to set key 2 and set it
		try{
			this.setAssociationMethodSetter(associationClass, klass1, embeddedIdType, embeddedId, id1);
			this.setAssociationMethodSetter(associationClass, klass2, embeddedIdType, embeddedId, id2);
		}
		catch (IllegalArgumentException e){
			throw e;
		}
		
		Object association = this.entityManager.find(associationClass, embeddedId);
		if (null != association){
			this.entityManager.remove(association);
		}
			
	}
	
	
	public R getRelationship(Class<R> associationClass, Class<S> klass1, String id1, Class<T> klass2, String id2){
		
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Returns all the 
	 * @param associationClass
	 * @param propertyClass
	 * @param id1
	 * @return
	 * @throws IllegalArgumentException
	 */
	public List<R> getAssociationsByFirstEntity(Class<R> associationClass, Class<S> propertyClass, 
			String id1) throws IllegalArgumentException {
		
		//How to go about this one:
		//1. JPL query that does it
		String SELECT_QUERY = "SELECT e From {0} e WHERE e.{1}=:objectId";
		
		//2. Find the Strings to substitute in the query
		//2.1 Find entity name
		String entityName = associationClass.getSimpleName();
		
		//2.2 Find property name
		String propertyName = null;
		try{
			propertyName = getPropertyName(associationClass, propertyClass);
		}
		catch (IllegalArgumentException e){
			throw e;
		}
		
								
		//3 Execute the query and return the results.
		String finalQuery = MessageFormat.format(SELECT_QUERY, entityName, propertyName);
		Query query = this.entityManager.createQuery(finalQuery);
		Class<?> idType = JPAEntityHelper.getIdType(propertyClass);
		Object parsedId1 = this.parseValue(idType, id1);
		query.setParameter("objectId", parsedId1);
		@SuppressWarnings("cast")
		List<R> results = (List<R>) query.getResultList();
		
		return results;
	}
		
	private void setAssociationMethodSetter(Class<?> associationClass, Class<?> fieldType, Class<?> embeddedIdType, Object embeddedId, String id1){
		
		Method getterMethod = null;
		
		//1 Find a method with the JoinColumn annotation and return type
		//equal to the given class?
		Method [] methods = associationClass.getDeclaredMethods();
		for (Method method : methods){
			if (null != method.getAnnotation(javax.persistence.JoinColumn.class)
					&& method.getReturnType().equals(fieldType)){
				getterMethod = method;
				break;
			}
		}
		if (null == getterMethod) throw new IllegalArgumentException(); 
		
		//2 Get the MapsId annotation and get the attribute name from there.
		String attributeName = null;
		try{
			attributeName = getterMethod.getAnnotation(javax.persistence.MapsId.class).value();
		}
		catch (java.lang.NullPointerException e){
			throw new IllegalArgumentException();
		}
		
		if (null == attributeName) throw new IllegalArgumentException();
		
		
		//3 Get the setter function name from there and figure out the
		//type of the argument and convert the string to that type.
		StringBuilder sb = new StringBuilder();
		sb.append("set");
		sb.append(Character.toUpperCase(attributeName.charAt(0)));
		sb.append(attributeName.substring(1));
		String setterMethodName = sb.toString();
		Method setterMethod = null;
		try{
			
			Method [] embeddedTypeMethods = embeddedIdType.getDeclaredMethods();
			for (Method method : embeddedTypeMethods){
				if (method.getName().equals(setterMethodName)){
					setterMethod = method;
					break;
				}
			}
		}
		catch (IllegalArgumentException e){
			throw e;
		} catch (SecurityException e) {
			throw new IllegalArgumentException();
		}
		if (null == setterMethod) throw new IllegalArgumentException();
		
		Class<?> [] parameters = setterMethod.getParameterTypes();
		if (1 != parameters.length) throw new IllegalArgumentException();
		Class<?> argumentType = parameters[0];
		Object id1Object = parseValue(argumentType, id1);
		
		//4 Call the setter method
		try {
			setterMethod.invoke(embeddedId, id1Object);
		} catch (IllegalAccessException e) {
			throw new IllegalArgumentException();
		} catch (IllegalArgumentException e) {
			throw e;
		} catch (InvocationTargetException e) {
			throw new IllegalArgumentException();
		}
		
	}
	
	/**
	 * 
	 * @param klass
	 * @param id
	 * @throws NumberFormatException whenever the String provided cannot
	 * be converted to the proper data type.
	 * @return
	 */
	private Object parseValue(Class<?> valueType, String value){
		
		if (valueType.equals(String.class)){
			return value;
		}
		else if (valueType.equals(Long.class)){
			return Long.parseLong(value);
		}
		else if (valueType.equals(Integer.class)){
			return Integer.parseInt(value);
			
		}
		else if (valueType.equals(Character.class)){
			if (value.length()>1) throw new IllegalArgumentException("The String id could not be converted to character");
			Character newId = value.charAt(0);
			return newId;
		}
		else{
			throw new IllegalArgumentException("The id class cannot be identified");
		}
	}
	
	/**
	 * Finds the name of the property of type propertyType in the class
	 * associationClass
	 * @param associationClass
	 * @param propertyClass
	 * @return
	 */
	private String getPropertyName(Class<R> associationClass, Class<?> propertyClass)
				throws IllegalArgumentException
	{
		
		//0. Get the name of the attribute of the EmbeddedId class
		String embeddedIdAttributeName = "id";
		//0.1 Get the method with EmbeddedId annotation
		Method embeddedIdMethod = null;
		Method [] methods = associationClass.getDeclaredMethods();
		for (Method method : methods){
			if (null != method.getAnnotation(javax.persistence.EmbeddedId.class)){
				embeddedIdMethod = method;
				break;
			}
		}
		if (null == embeddedIdMethod) throw new IllegalArgumentException();
		
		//0.1 Get the return type of the method
		try{
			String methodName = embeddedIdMethod.getName();
			StringBuilder sb = new StringBuilder();
			sb.append(Character.toLowerCase(methodName.charAt(3)));
			sb.append(methodName.substring(4));
			embeddedIdAttributeName = sb.toString();
		}
		catch (java.lang.NullPointerException e){
			throw new IllegalArgumentException();
		}
		catch (Exception e){
			throw new IllegalArgumentException();
		}
				
		//1. Get the method that contains the MapsId annotation
		Method getterMethod = null;
		for (Method method : methods){
			if (null != method.getAnnotation(javax.persistence.JoinColumn.class)
					&& method.getReturnType().equals(propertyClass)){
				getterMethod = method;
				break;
			}
		}
		if (null == getterMethod) throw new IllegalArgumentException(); 
		
		//2 Get the MapsId annotation and get the attribute name from there.
		String attributeName = null;
		try{
			attributeName = getterMethod.getAnnotation(javax.persistence.MapsId.class).value();
		}
		catch (java.lang.NullPointerException e){
			throw new IllegalArgumentException();
		}
		catch (Exception e){
			throw new IllegalArgumentException();
		}
		
		if (null == attributeName) throw new IllegalArgumentException();
		
		StringBuilder sb = new StringBuilder();
		sb.append(embeddedIdAttributeName);
		sb.append(".");
		sb.append(attributeName);
		return sb.toString();
		
	}
	
}
