package com.ibibl.searchengine.ejb;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Stateless;

import com.ibibl.bible.ESVBibleOutline;

@LocalBean
@Startup
@Singleton
public class ESVEJB extends BibleEJB{

	public ESVEJB(){
		super(ESVBibleOutline.getInstance(), "ESV");
	}
	
}
