package com.ibibl.searchengine.ejb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import com.ibibl.bible.BibleVersion;
import com.ibibl.bible.Book;
import com.ibibl.bible.Language;
import com.ibibl.searchengine.viewmodel.BibleVerseView;
import com.ibibl.searchengine.viewmodel.PaginationResultsView;


@Stateless
@LocalBean
public class BibleAdvancedSearchEJB {

	private static Logger LOGGER = Logger.getLogger(BibleAdvancedSearchEJB.class);
	private static int DEFAULT_NUMBER_OF_ROWS = 10;
	private static String AUTHOR_FIELD_NAME = "author_jadiel";
	private static String BOOK_FIELD_NAME = "book";
	private static String SECTION_FIELD_NAME = "section_jadiel";
	private static String REFERENCE_SORT_ORDER = "reference";
	private static String RELEVANCE_SORT_ORDER = "relevance";
	private static String DEFAULT_VERSION_ABBV = "NIV";
	
	private Set<String> sortOptions;
	
	@EJB
	private SolrBibleEJB solrEJB;
	
	@EJB
	private BibleMetadataEJB metadataEJB;
	
	public BibleAdvancedSearchEJB(){
		this.sortOptions = new HashSet<String>();
		this.sortOptions.add("reference");
		this.sortOptions.add("relevance");
	}
	
	public PaginationResultsView<BibleVerseView> getFilteredResults(
			String luceneQuery, ArrayList<String> authorFilters, ArrayList<String> bookFilters,
			ArrayList<String> sectionFilters, String version, String sortOrder, Integer start, Integer size) throws Exception{
		
		
		SolrClient client = this.solrEJB.getSolrClient();
		SolrQuery query = new SolrQuery();
		
		query.setRequestHandler("/advancedsearch");
		
		//1. luceneQuery
		if (null == luceneQuery || luceneQuery.length() == 0) query.setQuery("*:*");
		query.setQuery(luceneQuery);
		
		
		//2. authorFilters
		StringBuilder authorFilterSB = new StringBuilder("");
		if (authorFilters.size() > 0){
			for (int i = 0; i < authorFilters.size() - 1; ++i){
				authorFilterSB.append(AUTHOR_FIELD_NAME);
				authorFilterSB.append(":");
				authorFilterSB.append(authorFilters.get(i));
				authorFilterSB.append(" OR ");
			}
			
			String lastAuthor = authorFilters.get(authorFilters.size()-1);
			authorFilterSB.append(AUTHOR_FIELD_NAME);
			authorFilterSB.append(":");
			authorFilterSB.append(lastAuthor);
		}
		
		//3. bookFilters
		StringBuilder bookFilterSB = new StringBuilder("");
		if (bookFilters.size() > 0){
			for (int i = 0; i < bookFilters.size() - 1; ++i){
				bookFilterSB.append(BOOK_FIELD_NAME);
				bookFilterSB.append(":");
				bookFilterSB.append(bookFilters.get(i));
				bookFilterSB.append(" OR ");
			}
			String lastBook = bookFilters.get(bookFilters.size()-1);
			bookFilterSB.append(BOOK_FIELD_NAME);
			bookFilterSB.append(":");
			bookFilterSB.append(lastBook);
		}
		
		//4. sectionFilters
		StringBuilder sectionFilterSB = new StringBuilder("");
		if (sectionFilters.size() > 0){
			for (int i = 0; i < sectionFilters.size() -1 ; ++i){
				sectionFilterSB.append(SECTION_FIELD_NAME);
				sectionFilterSB.append(":");
				sectionFilterSB.append(sectionFilters.get(i));
				sectionFilterSB.append(" OR ");
			}
			String lastSection = sectionFilters.get(sectionFilters.size() - 1);
			sectionFilterSB.append(SECTION_FIELD_NAME);
			sectionFilterSB.append(":");
			sectionFilterSB.append(lastSection);
		}
		
		query.setFilterQueries(authorFilterSB.toString(), 
				bookFilterSB.toString(), 
				sectionFilterSB.toString());
		
		//5. version
		if (null == version){
			version = DEFAULT_VERSION_ABBV;
		}
		String versionFieldName = this.getVersionFieldName(version);
		query.set("df", versionFieldName);
				
		//6. sortOrder
		if (null == sortOrder){
			query.set("sort", "book asc, chapter asc, verse asc");
		}
		else{
			if (REFERENCE_SORT_ORDER.equals(sortOrder)){
				query.set("sort", "book asc, chapter asc, verse asc");
			}
			else if (RELEVANCE_SORT_ORDER.equals(sortOrder)){
				query.set("sort", "popularity desc");
			}
			else {
				query.set("sort", "book asc, chapter asc, verse asc");
			}
		}
						
		//7. Start and size
		int startInt = 0;
		if (null != start) startInt = start.intValue();
		int sizeInt = BibleAdvancedSearchEJB.DEFAULT_NUMBER_OF_ROWS;
		if (null != size) sizeInt = size.intValue();
		query.set("start", startInt);
		query.set("rows", sizeInt);
		
		//8. Display fields
		StringBuilder displayFieldsString = new StringBuilder("book chapter verse ");
		String displayVersionField = this.getDisplayVersionFieldName(version);
		displayFieldsString.append(displayVersionField);
		query.set("fl", displayFieldsString.toString());
		
		
		//9. GEtting the query results
		QueryResponse queryResponse = null;
		try {
			queryResponse = client.query(query);
		} catch (SolrServerException | IOException e) {
			throw e;
		}
		LOGGER.info(queryResponse.getResponseHeader());
		SolrDocumentList solrResults = queryResponse.getResults();
		List<BibleVerseView> verses = new ArrayList<BibleVerseView>();
		for (SolrDocument solrDocument : solrResults){
			BibleVerseView bibleVerseView = this.convertToBibleVerseView(solrDocument, version, displayVersionField);
			verses.add(bibleVerseView);
		}
		
		int totalCountInt = (int) solrResults.getNumFound();
		int startResponseInt = (int) solrResults.getStart();
		
		PaginationResultsView<BibleVerseView> toReturn = new PaginationResultsView<BibleVerseView>(totalCountInt, startResponseInt, verses,
				BibleVerseView.TYPE, BibleVerseView.VERSION);
		return toReturn;
	}
	
	private BibleVerseView convertToBibleVerseView(SolrDocument document, String version, String versionField){
		String book = (String)document.getFieldValue("book");
		Integer chapter = (Integer) document.getFieldValue("chapter");
		Integer verse = (Integer)document.getFieldValue("verse");
		String text = (String)document.getFieldValue(versionField);
		
		BibleVersion versionStruct = BibleVersion.fromAbbreviation(version);
		Book bookStruct = Book.fromAbbreviation(book);
		
		BibleVerseView verseView = new BibleVerseView(bookStruct, versionStruct, chapter, verse, text);
		return verseView;
	}
	
	private String getVersionFieldName(String version){
		if (null == version ||!metadataEJB.isVersion(version)){
			version = DEFAULT_VERSION_ABBV;
		}
		StringBuilder sb = new StringBuilder();
		sb.append(version);
		sb.append("_version_raw_");
		String language = this.versionLanguage(version);
		sb.append(language);
		sb.append("_t");
		return sb.toString();
	}
	
	private String getDisplayVersionFieldName(String version){
		if (null == version || !this.metadataEJB.isVersion(version)){
			version = DEFAULT_VERSION_ABBV;
		}
		StringBuilder sb = new StringBuilder();
		sb.append(version);
		sb.append("_version_");
		String language = this.versionLanguage(version);
		sb.append(language);
		sb.append("_t");
		return sb.toString();
	}
	
	private String versionLanguage(String version){
		BibleVersion versionStruct = BibleVersion.fromAbbreviation(version);
		if (null == versionStruct) return "eng";
		
		Language language = versionStruct.getLanguage();
		if (Language.English.equals(language)){
			return "eng";
		}
		else if (Language.Spanish.equals(language)){
			return "spa";
		}
		return "eng";	
	}
	/**
	 * Returns true if this is a valid sort option. Otherwise it returns false.
	 * @param option
	 * @return
	 */
	public boolean isSortOption(String option){
		if (this.sortOptions.contains(option)) return true;
		return false;
	}
}
