package com.ibibl.searchengine.ejb;

import com.ibibl.bible.R60BibleOutline;

public class R60EJB extends BibleEJB {

	public R60EJB(){
		super(R60BibleOutline.getInstance(), "R60");
	}
}
