package com.ibibl.searchengine.ejb;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Stateless;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;

/**
 * SOLR client to the Bible core in SOLR
 * @author jadiel
 *
 */
@Singleton
@Startup
@LocalBean
public class SolrBibleEJB {

	private SolrClient solr;
	
	/**
	 * Default constructor that does nothing.
	 */
	public SolrBibleEJB(){
		
	}
	
	@PostConstruct
	public void intializeSolrClient(){
		String serverUrl = "http://localhost:8983/solr/bible";
		this.solr = new HttpSolrClient(serverUrl);
	}
	
	public SolrClient getSolrClient(){
		return this.solr;
	}
	
	
}
