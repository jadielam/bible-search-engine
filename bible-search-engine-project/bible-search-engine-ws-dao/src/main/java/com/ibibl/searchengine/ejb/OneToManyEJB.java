// **************************************************
//
// Modification Tracking:
// $Revision:$
// $LastChangedBy:$
// $LastChangedDate:$
//
// **************************************************
package com.ibibl.searchengine.ejb;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import com.ibibl.searchengine.common.JPAEntityHelper;

/**
 * @author jdearmas
 *
 * @since 1.0
 */
@Stateless
@LocalBean
public class OneToManyEJB <S extends Object, T extends Object> {
	
	private static Logger LOGGER = Logger.getLogger(OwnedEJB.class.getName());
	
	@PersistenceContext(unitName = "BiblePersistence")
	private EntityManager entityManager;
	
	private static final String FINDALL_QUERY = "SELECT c FROM {0} c  WHERE c.{1} = :id";
	
	private static final String FINDBYID_QUERY = "SELECT c FROM {0} c WHERE c.{1} = :childId AND c.{2} = :parent";
	
	/**
	 * Default constructor created, just in case.
	 */
	public OneToManyEJB(){
		//Created just in case
	}
	
	
	@SuppressWarnings("unchecked")
	public Collection<T> findAll(Class<S> parentClass, Class<T> childClass, 
			String parentClassId, Integer startIndex, Integer size) throws Exception{
			
		Collection<T> entities = new ArrayList<T>();
		try{
			String childClassName = childClass.getSimpleName();
			String joinFieldName = JPAEntityHelper.getJoinColumnFieldName(childClass, parentClass);
			String queryString = MessageFormat.format(FINDALL_QUERY, childClassName, joinFieldName);
			Query query = this.entityManager.createQuery(queryString);
			
			//1. getting the parent object
			Class<?> idType = JPAEntityHelper.getIdType(parentClass);
			Object parsedId = this.parseValue(idType, parentClassId);
			S parentObject = this.entityManager.find(parentClass, parsedId);
			if (null == parentObject){
				throw new Exception("Parent object not found");
			}
			query.setParameter("id", parentObject);
			
			//Getting the results
			entities = query.getResultList();
		}
		//TODO: Put more finegrained exceptions here.
		catch (Exception e){
			
			throw e;
		}
		return entities;
		
	}
	
	public T findById(Class<S> parentClass, Class<T> childClass,
			String parentClassId, String childClassId) throws Exception{
		
		try{
			String childClassName = childClass.getSimpleName();
			String childClassIdName = JPAEntityHelper.getIdFieldName(childClass);
			String joinFieldName = JPAEntityHelper.getJoinColumnFieldName(childClass, parentClass);
			String queryString = MessageFormat.format(FINDBYID_QUERY, childClassName, childClassIdName, joinFieldName);
			Query query = this.entityManager.createQuery(queryString);
			
			//1. getting the parent object
			Class<?> parentIdType = JPAEntityHelper.getIdType(parentClass);
			Object parsedId = this.parseValue(parentIdType, parentClassId);
			S parentObject = this.entityManager.find(parentClass, parsedId);
			if (null == parentObject){
				throw new Exception("Parent object not found");
			}
			//2. Getting the child id
			Class<?> childIdType = JPAEntityHelper.getIdType(childClass);
			Object childParsedId = this.parseValue(childIdType, childClassId);
			query.setParameter("parent", parentObject);
			query.setParameter("childId", childParsedId);
			T t = (T) query.getSingleResult();
			return t;
		}
		//TODO: Put more finegrained exceptions here.
		catch (Exception e){
			
			throw e;
		}
		
		

	}
	
	public void delete(Class<S> parentClass, Class<T> childClass,
			String parentClassId, String childClassId){
		
	}
	
	public void postChildToParent(Class<S> parentClass, Class<T> childClass,
			String parentClassId, T child) throws Exception{
		
		//1. get the parent
		//1. getting the parent object
		Class<?> parentIdType = JPAEntityHelper.getIdType(parentClass);
		Object parsedId = this.parseValue(parentIdType, parentClassId);
		S parentObject = this.entityManager.find(parentClass, parsedId);
		if (null == parentObject){
			throw new Exception("Parent object not found");
		}

		//2. set the parent in the child
		this.setAssociationMethodSetter(childClass, parentClass, child, parentObject);
				
		//3. persist the child.
		this.entityManager.merge(child);
				
	}
			

	/**
	 * 
	 * @param klass
	 * @param id
	 * @throws NumberFormatException whenever the String provided cannot
	 * be converted to the proper data type.
	 * @return
	 */
	private Object parseValue(Class<?> valueType, String value){
		
		if (valueType.equals(String.class)){
			return value;
		}
		else if (valueType.equals(Long.class)){
			return Long.parseLong(value);
		}
		else if (valueType.equals(Integer.class)){
			return Integer.parseInt(value);
			
		}
		else if (valueType.equals(Character.class)){
			if (value.length()>1) throw new IllegalArgumentException("The String id could not be converted to character");
			Character newId = value.charAt(0);
			return newId;
		}
		else{
			throw new IllegalArgumentException("The id class cannot be identified");
		}
	}
	
private void setAssociationMethodSetter(Class<?> childClass, 
		Class<?> fieldType,	T child, S parentObject){
		
		Method getterMethod = null;
		
		//1 Find a method with the JoinColumn annotation and return type
		//equal to the given class?
		Method [] methods = childClass.getDeclaredMethods();
		for (Method method : methods){
			if (null != method.getAnnotation(javax.persistence.JoinColumn.class)
					&& method.getReturnType().equals(fieldType)){
				getterMethod = method;
				break;
			}
		}
		if (null == getterMethod) throw new IllegalArgumentException(); 
		
		//2. Generate the name of the setter method from the getter method
		StringBuilder sb = new StringBuilder("");
		sb.append("set");
		sb.append(getterMethod.getName().substring(3));
		String setterMethodName = sb.toString();
		
		//3. Find the setter method.
		Method setterMethod = null;
		try{
			for (Method method : methods){
				if (method.getName().equals(setterMethodName)){
					setterMethod = method;
					break;
				}
			}
		}
		catch (IllegalArgumentException e){
			throw e;
		} catch (SecurityException e) {
			throw new IllegalArgumentException();
		}
		if (null == setterMethod) throw new IllegalArgumentException();
		
		Class<?> [] parameters = setterMethod.getParameterTypes();
		if (1 != parameters.length) throw new IllegalArgumentException();
		
		//4 Call the setter method
		try {
			setterMethod.invoke(child, parentObject);
		} catch (IllegalAccessException e) {
			throw new IllegalArgumentException();
		} catch (IllegalArgumentException e) {
			throw e;
		} catch (InvocationTargetException e) {
			throw new IllegalArgumentException();
		}
		
	}
	

}
