package com.ibibl.searchengine.ejb;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Stateless;

import com.ibibl.bible.KJVBibleOutline;

@LocalBean
@Startup
@Singleton
public class KJVEJB extends BibleEJB{

	public KJVEJB(){
		super(KJVBibleOutline.getInstance(), "KJV");
	}
}
