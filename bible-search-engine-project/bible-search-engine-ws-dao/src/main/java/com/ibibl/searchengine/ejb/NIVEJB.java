package com.ibibl.searchengine.ejb;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Stateless;

import com.ibibl.bible.NIVBibleOutline;

@LocalBean
@Startup
@Singleton
public class NIVEJB extends BibleEJB {

	public NIVEJB(){
		super(NIVBibleOutline.getInstance(), "NIV");
	}
	
	
}
