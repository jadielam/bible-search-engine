package com.ibibl.searchengine.ejb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import com.ibibl.bible.BibleRange;
import com.ibibl.bible.BibleReference;
import com.ibibl.bible.BibleReferenceRange;
import com.ibibl.bible.BibleVersion;
import com.ibibl.bible.Book;
import com.ibibl.bible.Language;
import com.ibibl.bible.Reference;
import com.ibibl.searchengine.viewmodel.BibleObject;
import com.ibibl.searchengine.viewmodel.BibleRangeView;
import com.ibibl.searchengine.viewmodel.BibleVerseView;
import com.ibibl.searchengine.viewmodel.ListView;

@Stateless
@LocalBean
public class BibleEJB {

	
	private static Logger LOGGER = Logger.getLogger(BibleEJB.class);
	
	
	@EJB
	SolrBibleEJB solrEJB;
	
	public BibleEJB(){
	
	}
	
		
	private String getVersionFieldName(String version){
		StringBuilder sb = new StringBuilder();
		sb.append(version);
		sb.append("_version_");
		String language = this.versionLanguage(version);
		sb.append(language);
		sb.append("_t");
		return sb.toString();
	}
	
	
	private String versionLanguage(String version){
		BibleVersion versionStruct = BibleVersion.fromAbbreviation(version);
		if (null == versionStruct) return "eng";
		
		Language language = versionStruct.getLanguage();
		if (Language.English.equals(language)){
			return "eng";
		}
		else if (Language.Spanish.equals(language)){
			return "spa";
		}
		return "eng";	
	}
	
	private BibleVerseView convertToBibleVerseView(SolrDocument document, String version, String versionField){
		String book = (String)document.getFieldValue("book");
		Integer chapter = (Integer) document.getFieldValue("chapter");
		Integer verse = (Integer)document.getFieldValue("verse");
		String text = (String)document.getFieldValue(versionField);
		
		BibleVersion versionStruct = BibleVersion.fromAbbreviation(version);
		Book bookStruct = Book.fromAbbreviation(book);
		
		BibleVerseView verseView = new BibleVerseView(bookStruct, versionStruct, chapter, verse, text);
		return verseView;
	}

	public BibleVerseView getBibleVerse(BibleReference ref, String versionAbbv) {
		
		Book book = ref.getBook();
		String bookAbbv = book.getAbv();
		String chapterString = Integer.toString(ref.getChapter());
		String verseString = Integer.toString(ref.getVerse());
		
		SolrClient client = this.solrEJB.getSolrClient();
		SolrQuery query = new SolrQuery();
		
		//1. Setting the request handle
		query.setRequestHandler("/select");
		
		//2. Setting the filters
		query.setQuery("*:*");
		query.setFilterQueries("book:"+bookAbbv+" AND chapter:"+chapterString+" AND verse:"+verseString);
		
		//3. Setting the display fields to be returned
		String versionField = this.getVersionFieldName(versionAbbv);
		StringBuilder fieldsString = new StringBuilder();
		fieldsString.append("book chapter verse ");
		fieldsString.append(versionField);
		query.set("fl", fieldsString.toString());
		
		QueryResponse queryResponse;
		try {
			queryResponse = client.query(query);
		} catch (SolrServerException | IOException e) {
			return null;
		}
		SolrDocumentList solrResults = queryResponse.getResults();
		if (0 == solrResults.size()) return null;
		SolrDocument firstDocument = solrResults.get(0);
		BibleVerseView toReturn = this.convertToBibleVerseView(firstDocument, versionAbbv, versionField);
		return toReturn;
	}
	
		
	
	/**
	 * Returns the references as a ListView<BibleRangeView>
	 * @param references
	 * @return
	 */
	public ListView<BibleRangeView> getBibleVersesBibleRangeView(List<Reference> references, String versionAbbv){
		List<BibleRangeView> toReturn = new ArrayList<BibleRangeView>();
		for (Reference ref : references){
			try{
				List<BibleReference> refs = ref.getBibleReferences();
				Map<BibleReference, String> refsText = new HashMap<BibleReference, String>();
				for (BibleReference bref : refs){
					BibleVerseView verseView = this.getBibleVerse(bref, versionAbbv);
					if (null != verseView){
						String text = verseView.getText();
						refsText.put(bref, text);	
					}
				}
				
				BibleRange range = null;
				if (ref instanceof BibleReferenceRange){
					BibleReferenceRange brange = (BibleReferenceRange)ref;
					range = new BibleRange(brange, refsText);
				}
				else if (ref instanceof BibleReference){
					BibleReference bref = (BibleReference)ref;
					BibleReferenceRange brange = BibleReferenceRange.getBibleReferenceRange(bref, bref);
					range = new BibleRange(brange, refsText);
				}
				BibleRangeView verses = new BibleRangeView(range);
				toReturn.add(verses);
			}
			catch(IllegalStateException | IllegalArgumentException a){
				continue;
			}
		}
		ListView<BibleRangeView> viewToReturn = new ListView<BibleRangeView>(toReturn, 
				BibleRangeView.TYPE, BibleRangeView.VERSION);
		return viewToReturn;
	}
	
	/**
	 * Returns the references as a ListView<BibleVerseView>
	 * @param references
	 * @return
	 */
	public ListView<BibleVerseView> getBibleVersesBibleVerseView(List<Reference> references,
			String versionAbbv){
		List<BibleVerseView> toReturn = new ArrayList<BibleVerseView>();
		for (Reference ref : references){
			try{
				List<BibleReference> refs = ref.getBibleReferences();
				for (BibleReference bref : refs){
					BibleVerseView verse = this.getBibleVerse(bref, versionAbbv);
					toReturn.add(verse);
				}
			}
			catch(Exception e){
				continue;
			}
			
		}
		ListView<BibleVerseView> viewToReturn = new ListView<BibleVerseView>(toReturn, BibleVerseView.TYPE, BibleVerseView.VERSION);
		return viewToReturn;
	}
	
	@Deprecated
	public ListView<BibleObject> getBibleVerses(List<Reference> references, String versionAbbv){
		List<BibleObject> toReturn = new ArrayList<BibleObject>();
		for (Reference ref : references){
			if (ref instanceof BibleReference){
				try{
					BibleReference bref = (BibleReference)ref;
					BibleVerseView verse = this.getBibleVerse(bref, versionAbbv);
					toReturn.add(verse);
				}
				catch(Exception e){
					continue;
				}
			}
			else if (ref instanceof BibleReferenceRange){
				try{
					BibleReferenceRange brange = (BibleReferenceRange)ref;
					List<BibleReference> refs = ref.getBibleReferences();
					Map<BibleReference, String> refsText = new HashMap<BibleReference, String>();
					for (BibleReference bref : refs){
						String text = this.getBibleVerse(bref, versionAbbv).getText();
						refsText.put(bref, text);
					}
					BibleRange range = new BibleRange(brange, refsText);
					BibleRangeView verses = new BibleRangeView(range);
					toReturn.add(verses);
				}
				catch(IllegalStateException | IllegalArgumentException a){
					continue;
				}
			}
		}
		ListView<BibleObject> viewToReturn = new ListView<BibleObject>(toReturn, BibleObject.TYPE, BibleObject.VERSION);
		return viewToReturn;
	}
}
