package com.ibibl.searchengine.ejb;

import com.ibibl.bible.NVIBibleOutline;

public class NVIEJB extends BibleEJB {

	public NVIEJB(){
		super(NVIBibleOutline.getInstance(), "NVI");
	}
}
