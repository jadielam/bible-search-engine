package com.ibibl.searchengine.ejb.bibledictionary;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;

@Singleton
@Startup
@LocalBean
public class SolrBibleDictionaryEJB {

	private SolrClient solr;
	
	/**
	 * Default constructor that does nothing
	 */
	public SolrBibleDictionaryEJB(){
		
	}
	
	@PostConstruct
	public void initializeSolrClient(){
		String serverUrl = "http://localhost:8983/solr/bibledictionary";
		this.solr = new HttpSolrClient(serverUrl);
	}
	
	public SolrClient getSolrClient(){
		return this.solr;
	}
}
