package com.ibibl.searchengine.ejb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Singleton;

import org.apache.log4j.Logger;

import com.ibibl.searchengine.model.BibleVerse;
import com.ibibl.isabel.processor.Processor;
import com.ibibl.isabel.processor.SimpleTokenizer;
import com.ibibl.isabel.index.TrieNode;
import com.ibibl.isabel.ranking.ClosenessRanking;
import com.ibibl.isabel.ranking.PhraseRanker;
import com.ibibl.isabel.ranking.Ranker;
import com.ibibl.isabel.ranking.SimpleVerseRanker;



@Stateless
@LocalBean
public class BibleVerseSearchEngineEJB {

	@EJB
	@Singleton
	private SimpleEJB<BibleVerse> ejb;
	
	private static Logger LOGGER = Logger.getLogger(BibleVerseSearchEngineEJB.class);
	
	private Processor processor;
	private TrieNode index;
	private Ranker ranker;
	
	public BibleVerseSearchEngineEJB(){
		this.processor = new SimpleTokenizer();
		this.ranker = new PhraseRanker();
		this.index = new TrieNode();
	}
	
	@PostConstruct
	private void buildIndex() throws Exception{
		
		LOGGER.info("Building Bible index");
		Collection<BibleVerse> allVerses = this.ejb.findAll(BibleVerse.class, null, null);
		
		for (BibleVerse bibleVerse : allVerses){
			String verseText = bibleVerse.getText();
			String [] tokens = this.processor.getTokens(verseText);
			Map<String, VerseEntry> wordPositionsMap = new HashMap<String, VerseEntry>();
			
			for (int i = 0; i < tokens.length; ++i){
				String word = tokens[i];
				if (wordPositionsMap.containsKey(word)){
					wordPositionsMap.get(word).positions.add(i);
				}
				else{
					List<Integer> positions = new ArrayList<Integer>();
					positions.add(i);
					VerseEntry verseEntry = new VerseEntry(bibleVerse.getId(), 
							bibleVerse.getVersion().getId(), positions);
					
					wordPositionsMap.put(word, verseEntry);
				}
			}
			
			Set<Entry<String, VerseEntry>> entrySet = wordPositionsMap.entrySet();
			for (Entry<String, VerseEntry> entry : entrySet){
				String word = entry.getKey();
				VerseEntry verseEntry = entry.getValue();
				List<Integer> positions = verseEntry.positions;
				
				int [] positionsArray = new int[positions.size()];
				
				int i=0;
				for (Integer a : positions){
					positionsArray[i]=a;
					i++;
				}
				long a = verseEntry.documentId;
				this.index.insert(word, verseEntry.documentId, positionsArray, 
						verseEntry.collection);
			}
			
		}
		
		
	}
	
	
	public List<BibleVerse> getRankedBibleVerses(String query) throws Exception{
		
		List<BibleVerse> bibleVerses = new ArrayList<BibleVerse>();
		
		String [] tokens = this.processor.getTokens(query);
		
		long [] documentIds = this.ranker.rank(this.index, tokens, 20, "NIV");
		
		for (long documentId : documentIds){
			
			BibleVerse verse = this.ejb.getById(BibleVerse.class, Long.toString(documentId));
			bibleVerses.add(verse);
		}
				
		return bibleVerses;
	}
	
	private class VerseEntry{
		
		public Long documentId;
		
		public String collection;
		
		public List<Integer> positions;
		
		public VerseEntry(Long documentId, String collection, List<Integer> positions){
			this.documentId = documentId;
			this.collection = collection;
			this.positions = positions;
		}
	}
}
