package com.ibibl.searchengine.ejb;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Stateless;

import com.ibibl.bible.NKJBibleOutline;

@Startup
@LocalBean
@Singleton
public class NKJEJB extends BibleEJB {

	public NKJEJB(){
		super(NKJBibleOutline.getInstance(), "NKJ");
	}
}
