package com.ibibl.searchengine.ejb;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Stateless;

import com.ibibl.bible.NLTBibleOutline;

@LocalBean
@Startup
@Singleton
public class NLTEJB extends BibleEJB {

	public NLTEJB(){
		super(NLTBibleOutline.getInstance(), "NLT");
	}
}
