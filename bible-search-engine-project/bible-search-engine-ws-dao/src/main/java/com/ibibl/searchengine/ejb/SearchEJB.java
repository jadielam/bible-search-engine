package com.ibibl.searchengine.ejb;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.transaction.Transactional;

import com.ibibl.searchengine.common.JPAEntityHelper;
import com.ibibl.searchengine.index.Pair;
import com.ibibl.searchengine.index.TrieNode;
import com.ibibl.searchengine.search.LevenshteinSearch;

/**
 * @author jdearmas
 * @param <T> The type of the entity that we are searching for.
 *
 * @since 1.0
 */
@Stateless
@LocalBean
public class SearchEJB <T extends Object> {
	
	@PersistenceContext(unitName = "BiblePersistence")
	private EntityManager em;
	
	@PersistenceUnit(unitName = "BiblePersistence")
	private EntityManagerFactory factory;
	
	private EntityManagerFactory factory1;
	
	TrieNode index;
	
	/**
	 * Constructor with passing the entityManager.
	 * @param entityManager
	 */
	public SearchEJB(){
				
	}
	
	@PostConstruct
	private void startUp(){
		
	}
		
	/**
	 * Finds all the entities in the database of the given klass. If no entity is found, it
	 * returns an empty list.  From which entity it will start to return is determined by startIndex, 
	 * and the number of entities it returns is determined by the size parameter.
	 * @param entityType the class of the entity.
	 * @param fieldName 
	 * @param startIndex From where to start returning entities. If null, defaults to 0, which is
	 * the beginning of the list of entities.
	 * @param size How many entities to return. If null, it returns all of them.
	 * @return the list of entities in the database
	 * @throws Exception if there is something wrong that prevents the query from executing.
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public Collection<String> getCloseStrings(Class<T> entityType, String fieldName,
			String target, Integer distance) throws Exception	{
		
		try {
			buildIndex(entityType, fieldName);
		} catch (Exception e) {
			throw e;
		}
		Collection<Pair<Object, String>> closeEntries = this.getCloseEntries(entityType, 
				fieldName, target, distance);
		
		Collection<String> closeWords = new ArrayList<String>(closeEntries.size());
		for (Pair<Object, String> pair : closeEntries){
			closeWords.add(pair.getSecond());
		}
		return closeWords;
	}

	/**
	 * 
	 * @param entityType
	 * @param fieldName
	 * @param target
	 * @param distance
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public Collection<Pair<Object, String>> getCloseEntries(Class<T> entityType, String fieldName,
			String target, Integer distance) throws Exception{
		
		try {
			buildIndex(entityType, fieldName);
		} catch (Exception e) {
			throw e;
		}
		Collection<Pair<Object, String>> closeEntries = new ArrayList<Pair<Object, String>>();
		closeEntries = LevenshteinSearch.search(target, distance, this.index);
		return closeEntries;
		
	}
	
	Collection<Pair<Object, String>> findAllStrings(Class<?> entityType, String fieldName) throws Exception{
		
		EntityManager localEM = this.factory.createEntityManager();
		Collection<Object []> entities = new ArrayList<Object[]>();
		
		try {
			String idName = JPAEntityHelper.getIdFieldName(entityType);
			String queryTemplate = "SELECT c.{0}, c.{1} from {2} c";
			String queryString = MessageFormat.format(queryTemplate, idName, fieldName, entityType.getSimpleName());
			Query query = localEM.createQuery(queryString);
			entities = query.getResultList();
			Collection<Pair<Object, String>> toReturn = new ArrayList<Pair<Object, String>>(entities.size());
			
			for (Object [] entries : entities){
				Object id = entries[0];
				String string = (String)entries[1];
				toReturn.add(new Pair<Object, String>(id, string));
			}
			return toReturn;
		}
		catch (Exception e){
			throw e;
		}
	}
	
	private void buildIndex(Class<T> entityType, String fieldName) throws Exception{
		if (null == this.index){
			this.index = new TrieNode();
			
			try {
				Collection<Pair<Object, String>> words = this.findAllStrings(entityType, fieldName);
				for (Pair<Object, String> pair : words){
					this.index.insert(pair.getSecond(), pair.getFirst());
				}
				
				Timer timer = new Timer();
				timer.schedule(new IndexRebuilder<T>(entityType, fieldName), 0, 100000);
			} catch (Exception e) {
				throw e;
				
			}
		}
	}
	
	private class IndexRebuilder<T> extends TimerTask {

		private Class<T> entityType;
		private String fieldName;
		
		/**
		 * Default constructor.
		 */
		public IndexRebuilder(Class<T> entityType,
				String fieldName){
			this.entityType = entityType;
			this.fieldName = fieldName;
		}
		

		
		/** 
		 * @see java.util.TimerTask#run()
		 */
		@Override
		public void run() {
			
			TrieNode temporaryIndex = new TrieNode();
			Collection<Pair<Object, String>> data = new ArrayList<Pair<Object, String>>();
			try {
				
				data = SearchEJB.this.findAllStrings(this.entityType, this.fieldName);
				for (Pair<Object, String> pair : data){
					temporaryIndex.insert(pair.getSecond(), pair.getFirst());
				}
				SearchEJB.this.index = temporaryIndex;
				
			} catch (Exception e) {
				e.printStackTrace();				
			}
		}
		
	}
	
}


