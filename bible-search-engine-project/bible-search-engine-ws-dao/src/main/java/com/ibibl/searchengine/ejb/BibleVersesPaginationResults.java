package com.ibibl.searchengine.ejb;

import java.util.List;

import com.ibibl.searchengine.viewmodel.BibleVerseView;

public class BibleVersesPaginationResults {

	private final int totalResults;
	private final int startIndex;
	private final List<BibleVerseView> bibleVerses;
	
	public BibleVersesPaginationResults (int totalResults, int startIndex,
			List<BibleVerseView> bibleVerses){
		this.totalResults = totalResults;
		this.startIndex = startIndex;
		this.bibleVerses = bibleVerses;
	}

	public int getTotalResults() {
		return totalResults;
	}

	public int getStartIndex() {
		return startIndex;
	}

	public List<BibleVerseView> getBibleVerses() {
		return bibleVerses;
	}
}
