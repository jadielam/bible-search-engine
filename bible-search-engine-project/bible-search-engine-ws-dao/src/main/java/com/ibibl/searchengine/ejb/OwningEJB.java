// **************************************************
//
// Modification Tracking:
// $Revision:$
// $LastChangedBy:$
// $LastChangedDate:$
//
// **************************************************
package com.ibibl.searchengine.ejb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.resource.NotSupportedException;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.log4j.Logger;


/**
 * @author jdearmas
 *
 * @since 1.0
 */
@Stateless
@LocalBean
public class OwningEJB<S extends Object, T extends Object> {

	/**
	 * Logger of all the transactions performed by the EJB
	 */
	private static Logger LOGGER = Logger.getLogger(OwnedEJB.class.getName());
	
	/**
	 * This entity manager is container managed.
	 */
	@PersistenceContext(unitName = "BiblePersistence")
	private EntityManager entityManager;
	
	/**
	 * The template for the findAll query.
	 * It is used to build the query at runtime.
	 */
	private static final String FINDALL_QUERY = "find e from {0} e";
	
	/**
	 * Default constructor created, just in case.
	 */
	public OwningEJB(){
		//Created just in case
		
	}
	
	/**
	 * Finds the owned entity in the Persistence context that has id as primary key and returns it.
	 * @param ownedClass The class of the entity that we want
	 * @param id the priamry key of the entity
	 * @return the found entity instance, or null if no entity is found with that id.
	 * @throws IllegalArgumentException if the first argument does not denote an entity type
	 * or if the second argument is null or is not a valid type for that entity primary key.
	 */
	private S getOwnedById(Class<S> ownedClass, Object id) throws IllegalArgumentException {
		try{
			S s = this.entityManager.find(ownedClass, id);
			return s; 
		}
		catch(IllegalArgumentException iae){
			throw iae;
		}
	}
	
	/**
	 * THIS IS NOT SUPPORTED YET.
	 * Finds the entity of class ownedClass whose id is ownedEntityId.
	 * It then finds the collection of class owningClass in ownedClass and returns
	 * the portion of the collection specified by <code>startIndex</code> and
	 * <code>size</code>.
	 * @param ownedClass The parent class that containst the collection.
	 * @param owningClass The type of the collection
	 * @param ownedEntityId The id of the parent class.
	 * @param startIndex From where to start returning entities. If null, 
	 * defaults to index zero.
	 * @param size How many entities to return. If null, it returns all of them.
	 * @return the list of entities in the database.
	 * @throws Exception if there is something wrong that prevents the query from 
	 * executing.
	 */
	@SuppressWarnings("unchecked")
	public List<T> findAll(Class<S> ownedClass, Class<T> owningClass, Long ownedEntityId,
			Integer startIndex, Integer size)
		throws Exception {

		SortedMap<Long, T> entities = null;
		
		//1. Get the Collection of owning class entitites.
		try{
			S s = this.getOwnedById(ownedClass, ownedEntityId);
			//entities = (SortedMap<Long, T>) s.getCollection(owningClass);
			if (null == entities){
				throw new Exception("The Collection could not be found");
			}
		}
		catch (Exception e){
			throw new Exception("The Collection could not be found");
		}
		
		//2. Gather and return only the entities in the range defined by startIndex
		//and size
		int start = 0;
		int total = entities.size();
		if (null != startIndex) start = Math.max(0,  startIndex.intValue());
		if (null != size) total = Math.max(0, Math.min(total, size.intValue()));
		
		List<T> toReturn = new ArrayList<T>();
		int counter = 0;
		for (T t : entities.values()){
			if (counter >= start && counter < (start + total)){
				toReturn.add(t);
			}
			counter += 1;
		}
			
		throw new NotSupportedException();
		//In order to support it, I need to fix the issue with the commented line:
		//entities = (SortedMap<Long, T>) s.getCollection(owningClass);
		//return toReturn;
	}
	
	/**
	 * Finds the entity of type <code>owningClass<code> in the PersistenceContext 
	 * that has <code>owningId</code> as primary key, and whose
	 * parent entity of type <code>ownedClass</code> has <code>ownedId</code>
	 * as primary key.
	 * @param ownedClass The type of the parent entity
	 * @param ownedId The id of the parent entity
	 * @param owningClass The type of the child entity
	 * @param owningId The id of the child entity.
	 * @return the child entity whose id is <code>owningId</code> if it is found.
	 * Otherwise it returns null.
	 * @throws IllegalArgumentException 
	 * @throws Exception 
	 */
	public T getById(Class<S> ownedClass, Long ownedId, 
			Class<T> owningClass, Long owningId)
			throws IllegalArgumentException, Exception {
		
		SortedMap<Long, T> entities = null;
		T toReturn = null;
		try{
			S s = this.getOwnedById(ownedClass, ownedId);
			//entities = (SortedMap<Long, T>) s.getCollection(owningClass);
			if (null == entities){
				throw new Exception("The Collection could not be found.");
			}
			toReturn = entities.get(owningId);
		}
		catch(IllegalArgumentException  iae){
			throw iae;
		}
		
		throw new NotSupportedException();
		//In order to support it I need to solve the issue with the commented line:
		//entities = (SortedMap<Long, T>) s.getCollection(owningClass);
		//return toReturn;
	}
	
	//this.ejb.advancedSearch(this.ownedType, ownedEntityId,
	//		this.owningType, queryParameters);

	public Collection<T> advancedSearch(Class<S> ownedClass, Long ownedId,
			Class<T> owningClass, Long owningId, 
			MultivaluedMap<String, String> queryParams){
		
		return null;
		
	}
	
}
