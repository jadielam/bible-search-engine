package com.ibibl.searchengine.ejb;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Singleton;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import com.ibibl.bible.BibleVersion;
import com.ibibl.bible.Book;
import com.ibibl.bible.Language;
import com.ibibl.searchengine.viewmodel.BibleVerseView;
import com.ibibl.searchengine.viewmodel.PaginationResultsView;


@Stateless
@LocalBean
public class BibleSearchEngineEJB {

	private static int DEFAULT_NUMBER_OF_ROWS = 10;
	@EJB
	@Singleton
	private SolrBibleEJB solr;
	
	public BibleSearchEngineEJB(){
		
	}
	
	public PaginationResultsView<BibleVerseView> getRankedBibleVerses(String queryText, Integer start, Integer size,
			String version) throws Exception{
		
		int startInt = 0;
		if (null != start) startInt = start.intValue();
		int sizeInt = BibleSearchEngineEJB.DEFAULT_NUMBER_OF_ROWS;
		if (null != size) sizeInt = size.intValue();
		
		SolrClient client = this.solr.getSolrClient();
		SolrQuery query = new SolrQuery();
		
		//0. Set handler
		query.setRequestHandler("/bibleverse");
		
		//0. Set the query text
		query.setQuery(queryText);
		
		//3. Set the display fields
		String versionField = this.getVersionFieldName(version);
		StringBuilder fieldsString = new StringBuilder();
		fieldsString.append("book chapter verse ");
		fieldsString.append(versionField);
		query.set("fl", fieldsString.toString());
					
		//8. Add the pagination data
		query.set("start", startInt);
		query.set("size", sizeInt);
		
		//Getting back the response
		QueryResponse queryResponse = client.query(query);
		List<BibleVerseView> verses = new ArrayList<BibleVerseView>();
		
		SolrDocumentList solrResults = queryResponse.getResults();
		for (SolrDocument solrDocument : solrResults){
			BibleVerseView bibleVerseView = convertToBibleVerseView(solrDocument, version, versionField);
			verses.add(bibleVerseView);
		}
				
		int totalCountInt = (int)solrResults.getNumFound();
		int startResponseInt = (int)solrResults.getStart();
		
		PaginationResultsView<BibleVerseView> toReturn = new PaginationResultsView<BibleVerseView>(totalCountInt, startResponseInt, verses, 
				BibleVerseView.TYPE, BibleVerseView.VERSION);
		return toReturn;
	}
	
	private BibleVerseView convertToBibleVerseView(SolrDocument document, String version, String versionField){
		String book = (String)document.getFieldValue("book");
		Integer chapter = (Integer) document.getFieldValue("chapter");
		Integer verse = (Integer)document.getFieldValue("verse");
		String text = (String)document.getFieldValue(versionField);
		
		BibleVersion versionStruct = BibleVersion.fromAbbreviation(version);
		Book bookStruct = Book.fromAbbreviation(book);
		
		BibleVerseView verseView = new BibleVerseView(bookStruct, versionStruct, chapter, verse, text);
		return verseView;
	}
	
	private String getVersionFieldName(String version){
		StringBuilder sb = new StringBuilder();
		sb.append(version);
		sb.append("_version_");
		String language = this.versionLanguage(version);
		sb.append(language);
		sb.append("_t");
		return sb.toString();
	}
	
	private String versionLanguage(String version){
		BibleVersion versionStruct = BibleVersion.fromAbbreviation(version);
		if (null == versionStruct) return "eng";
		
		Language language = versionStruct.getLanguage();
		if (Language.English.equals(language)){
			return "eng";
		}
		else if (Language.Spanish.equals(language)){
			return "spa";
		}
		return "eng";	
	}
		
	
}