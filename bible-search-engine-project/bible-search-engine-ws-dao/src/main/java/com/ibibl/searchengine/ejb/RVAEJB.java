package com.ibibl.searchengine.ejb;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.ibibl.bible.RVABibleOutline;

@Stateless
@LocalBean
public class RVAEJB extends BibleEJB{

	public RVAEJB(){
		super(RVABibleOutline.getInstance(), "RVA");
	}
}
