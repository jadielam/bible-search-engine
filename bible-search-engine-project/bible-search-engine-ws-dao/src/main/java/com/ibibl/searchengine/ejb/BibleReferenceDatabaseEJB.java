package com.ibibl.searchengine.ejb;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.ibibl.bible.BibleReference;
import com.ibibl.searchengine.model.BibleVerseModel;

@Stateless
@LocalBean
public class BibleReferenceDatabaseEJB {

	@PersistenceContext(unitName = "BiblePersistence")
	private EntityManager entityManager;
	
	public BibleReferenceDatabaseEJB(){
		
	}
	
	public BibleReferenceDatabaseEJB(EntityManager em){
		this.entityManager = em;
	}
	
	/**
	 * Returns the BibleVerseModel corresponding to the given BibleReference
	 * Returns null if the BibleVerseModel cannot be found.
	 * @param ref
	 * @throws IllegalStateException if entityManager has not been initialized
	 * @return
	 */
	public BibleVerseModel getBibleVerse(BibleReference ref) throws IllegalStateException {
		
		if (null == this.entityManager) throw new IllegalStateException();
		
		CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();
		CriteriaQuery<BibleVerseModel> cq = cb.createQuery(BibleVerseModel.class);
		Root<BibleVerseModel> root = cq.from(BibleVerseModel.class);
		
		Path<String> bookPath = root.get("book");
		Path<Integer> chapterPath = root.get("chapter");
		Path<Integer> versePath = root.get("verse");
		
		Predicate bookPredicate = cb.equal(bookPath, ref.getBook().getAbv());
		Predicate chapterPredicate = cb.equal(chapterPath, ref.getChapter());
		Predicate versePredicate = cb.equal(versePath, ref.getVerse());
		
		Predicate conjunction = cb.conjunction();
		conjunction = cb.and(bookPredicate, chapterPredicate, versePredicate);
		cq.where(conjunction);
		
		TypedQuery<BibleVerseModel> q = this.entityManager.createQuery(cq);
		List<BibleVerseModel> verses = q.getResultList();
		if (verses.size() == 1){
			return verses.get(0);
		}
		return null;
	}
	
	/**
	 * Updates the frequency value for the Bible verse that corresponds to ref
	 * @param ref the Bible reference
	 * @param frequency the frequency with which it is used out there.
	 * @throws IllegalStateException if the entityManager has not been initialized.
	 */
	public void updateBibleVerseFreq(BibleReference ref, int frequency) throws IllegalStateException{
		
		if (null == this.entityManager) throw new IllegalStateException();
		
		BibleVerseModel verse = this.getBibleVerse(ref);
		
		int validatedFreq = Math.max(0,  frequency);
		if (null != verse){
			BibleVerseModel verse1 = this.entityManager.merge(verse);
			verse1.setFreq(validatedFreq);
		}
	}
}
