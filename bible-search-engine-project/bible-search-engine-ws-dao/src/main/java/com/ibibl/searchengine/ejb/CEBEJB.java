package com.ibibl.searchengine.ejb;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Stateless;

import com.ibibl.bible.CEBBibleOutline;

@LocalBean
@Startup
@Singleton
public class CEBEJB extends BibleEJB{

	public CEBEJB(){
		super(CEBBibleOutline.getInstance(), "CEB");
	}
}
