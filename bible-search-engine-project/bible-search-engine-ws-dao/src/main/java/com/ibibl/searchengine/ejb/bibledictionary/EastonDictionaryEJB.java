package com.ibibl.searchengine.ejb.bibledictionary;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Singleton;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;

import com.ibibl.searchengine.viewmodel.BibleDictionaryView;
import com.ibibl.searchengine.viewmodel.ListView;

@Stateless
@LocalBean
public class EastonDictionaryEJB {

	private static final String FILTER_FIELD_NAME = "dictionaryName:EASTON";
	
	@EJB
	@Singleton
	private SolrBibleDictionaryEJB solr;
	
	public ListView<BibleDictionaryView> getEastonEntries(String queryText) throws SolrServerException, IOException{
		
		SolrClient client = this.solr.getSolrClient();
		SolrQuery query = new SolrQuery();
		
		query.setRequestHandler("/title");
		query.setQuery(queryText);
		query.set("fq", FILTER_FIELD_NAME);
		
		QueryResponse queryResponse = client.query(query);
		List<BibleDictionaryView> entries = queryResponse.getBeans(BibleDictionaryView.class);
		ListView<BibleDictionaryView> toReturn = new ListView<>(entries, BibleDictionaryView.TYPE, BibleDictionaryView.VERSION);
		return toReturn;
	}
}
