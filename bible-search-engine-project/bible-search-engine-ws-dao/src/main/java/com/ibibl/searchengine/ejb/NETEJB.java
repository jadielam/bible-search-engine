package com.ibibl.searchengine.ejb;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Stateless;

import com.ibibl.bible.NETBibleOutline;


@LocalBean
@Startup
@Singleton
public class NETEJB extends BibleEJB {

	public NETEJB(){
		super(NETBibleOutline.getInstance(), "NET");
	}
}
