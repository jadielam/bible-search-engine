package com.ibibl.searchengine.ejb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.log4j.Logger;

import com.ibibl.bible.ASVBibleOutline;
import com.ibibl.bible.BibleOutline;
import com.ibibl.bible.CEBBibleOutline;
import com.ibibl.bible.ESVBibleOutline;
import com.ibibl.bible.KJVBibleOutline;
import com.ibibl.bible.NETBibleOutline;
import com.ibibl.bible.NIVBibleOutline;
import com.ibibl.bible.NKJBibleOutline;
import com.ibibl.bible.NLTBibleOutline;
import com.ibibl.bible.NVIBibleOutline;
import com.ibibl.bible.R60BibleOutline;
import com.ibibl.bible.R95BibleOutline;
import com.ibibl.bible.RVABibleOutline;
import com.ibibl.bible.Reference;
import com.ibibl.bible.BibleVersion;
import com.ibibl.bible.parser.BibleReferenceLexer;
import com.ibibl.bible.parser.BibleReferenceListenerImpl;
import com.ibibl.bible.parser.BibleReferenceParser;
import com.ibibl.bible.parser.BibleReferenceReg;
import com.ibibl.searchengine.viewmodel.BibleObject;
import com.ibibl.searchengine.viewmodel.BibleRangeView;
import com.ibibl.searchengine.viewmodel.BibleVerseView;
import com.ibibl.searchengine.viewmodel.ListView;

@Stateless
@LocalBean
public class BibleReferenceEJB {

	private static Logger LOGGER = Logger.getLogger(BibleReferenceEJB.class);
	
	private Map<BibleVersion, BibleOutline> versionOutlineMap;
	
	@EJB
	BibleEJB ejb;
	
	public BibleReferenceEJB(){
		this.versionOutlineMap = new HashMap<>();
		this.versionOutlineMap.put(BibleVersion.ASV, ASVBibleOutline.getInstance());
		this.versionOutlineMap.put(BibleVersion.CEB, CEBBibleOutline.getInstance());
		this.versionOutlineMap.put(BibleVersion.ESV, ESVBibleOutline.getInstance());
		this.versionOutlineMap.put(BibleVersion.KJV, KJVBibleOutline.getInstance());
		this.versionOutlineMap.put(BibleVersion.NET, NETBibleOutline.getInstance());
		this.versionOutlineMap.put(BibleVersion.NIV, NIVBibleOutline.getInstance());
		this.versionOutlineMap.put(BibleVersion.NKJ, NKJBibleOutline.getInstance());
		this.versionOutlineMap.put(BibleVersion.NLT, NLTBibleOutline.getInstance());
		this.versionOutlineMap.put(BibleVersion.NVI, NVIBibleOutline.getInstance());
		this.versionOutlineMap.put(BibleVersion.R60, R60BibleOutline.getInstance());
		this.versionOutlineMap.put(BibleVersion.R95, R95BibleOutline.getInstance());
		this.versionOutlineMap.put(BibleVersion.RVA, RVABibleOutline.getInstance());
		
	}
	
	public ListView<BibleRangeView> getReferencesBibleRangeView(String query, String versionAbbv){
		
		LOGGER.info("Received query: "+query);
		
		if (this.isPotentialBibleReference(query)){
			List<Reference> references = this.getBibleReferences(query, versionAbbv);
			ListView<BibleRangeView> objects = this.ejb.getBibleVersesBibleRangeView(references, versionAbbv);
			LOGGER.info("Number of ranges to return: "+references.size());
			return objects;
		}
		
		else return null;
	}
	
	public ListView<BibleVerseView> getReferencesBibleVerseView(String query, String versionAbbv){
		if (this.isPotentialBibleReference(query)){
			List<Reference> references = this.getBibleReferences(query, versionAbbv);
			ListView<BibleVerseView> objects = this.ejb.getBibleVersesBibleVerseView(references, versionAbbv);
			return objects;
		}
		else return null;
	}
	
	@Deprecated
	public ListView<BibleObject> getReferences(String query, String versionAbbv){
		if (this.isPotentialBibleReference(query)){
			List<Reference> references = this.getBibleReferences(query, versionAbbv);
			ListView<BibleObject> objects = this.ejb.getBibleVerses(references, versionAbbv);
			return objects;
		}
		else return null;
	}

	
	private boolean isPotentialBibleReference(String text){
		if (null == text) return false;
		Pattern pattern = BibleReferenceReg.REF_PATTERN;
		Matcher matcher = pattern.matcher(text);
		if (matcher.find()){
			return true;
		}
		return false;
	}
	
	private List<Reference> getBibleReferences(String text, String versionAbbv){
		BibleVersion versionStruct = BibleVersion.fromAbbreviation(versionAbbv);
		if (null == versionStruct) versionStruct = BibleVersion.NKJ;
		
		BibleOutline outline = this.versionOutlineMap.get(versionStruct);
		ANTLRInputStream input = new ANTLRInputStream(text);
		BibleReferenceLexer lexer = new BibleReferenceLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		BibleReferenceParser parser = new BibleReferenceParser(tokens);
		ParseTree tree = parser.ref();
		
		ParseTreeWalker walker = new ParseTreeWalker();
		List<Reference> references = new ArrayList<Reference>();
		BibleReferenceListenerImpl listener = new BibleReferenceListenerImpl(outline, references);
		walker.walk(listener, tree);
		
		references = listener.getCollectedReferences();
		return references;
	}


	
}
