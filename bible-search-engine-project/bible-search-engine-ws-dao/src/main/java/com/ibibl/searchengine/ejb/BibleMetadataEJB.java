package com.ibibl.searchengine.ejb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import com.ibibl.searchengine.viewmodel.BibleAuthorView;
import com.ibibl.searchengine.viewmodel.BibleBookView;
import com.ibibl.searchengine.viewmodel.BibleSectionView;
import com.ibibl.searchengine.viewmodel.BibleVersionView;
import com.ibibl.searchengine.viewmodel.ListView;

/**
 * This EJB initializes its data by getting the unique values from SOLR
 * for the fields of Bible authors, Bible sections and Bible books.
 * @author jadiel
 *
 */
@Singleton
@Startup
@LocalBean
@DependsOn("SolrBibleEJB")
public class BibleMetadataEJB {

	@EJB
	SolrBibleEJB ejb;
	
	private ListView<BibleAuthorView> bibleAuthors;
	private ListView<BibleSectionView> bibleSections;
	private ListView<BibleBookView> bibleBooks;
	private ListView<BibleVersionView> bibleVersions;
	private Set<String> authorNames;
	private Set<String> sectionNames;
	private Set<String> bookAbbvs;
	private Set<String> versionAbbvs;
	
	/**
	 * Default constructor
	 */
	public BibleMetadataEJB(){
	
	}
	
	@PostConstruct
	private void init() throws Exception{
		
		
		//1. Doing the authors query
		List<String> authorNames = this.getUniqueElements("author_jadiel");
		this.authorNames = new HashSet<String>(authorNames);
		this.bibleAuthors = this.convertStringsToAuthors(authorNames);
		
		//2. Doing the sections query
		List<String> sectionNames = this.getUniqueElements("section_jadiel");
		this.sectionNames = new HashSet<String>(sectionNames);
		this.bibleSections = this.convertStringsToSections(sectionNames);
		
		//3. Doing the books query
		List<String> bookNames = this.getUniqueElements("book");
		this.bookAbbvs = new HashSet<String>(bookNames);
		this.bibleBooks = this.convertStringsToBooks(bookNames);
		
		//4. Doing the versions query.
		List<String> versionAbbvs = this.getUniqueVersionFields();
		this.versionAbbvs = new HashSet<String>(versionAbbvs);
		this.bibleVersions = this.convertStringsToVersions(versionAbbvs);
	}
	
	private ListView<BibleVersionView> convertStringsToVersions(List<String> versionAbbvs){
		List<BibleVersionView> temp = new ArrayList<BibleVersionView>();
		for (String abbv : versionAbbvs){
			BibleVersionView a = new BibleVersionView(abbv);
			temp.add(a);
		}
		return new ListView<BibleVersionView>(temp, BibleVersionView.TYPE, BibleVersionView.VERSION);
	}
	
	private ListView<BibleAuthorView> convertStringsToAuthors(List<String> authorNames){
		List<BibleAuthorView> temp = new ArrayList<BibleAuthorView>();
		for (String authorName : authorNames){
			BibleAuthorView a = new BibleAuthorView(authorName);
			temp.add(a);
		}
		return new ListView<BibleAuthorView>(temp, BibleAuthorView.TYPE, BibleAuthorView.VERSION);
	}
	
	private ListView<BibleSectionView> convertStringsToSections(List<String> sectionNames){
		List<BibleSectionView> temp = new ArrayList<BibleSectionView>();
		for (String section : sectionNames){
			BibleSectionView view = new BibleSectionView(section);
			temp.add(view);
		}
		return new ListView<BibleSectionView>(temp, BibleSectionView.TYPE, BibleSectionView.VERSION);
	}
	
	private ListView<BibleBookView> convertStringsToBooks(List<String> bookNames){
		List<BibleBookView> temp = new ArrayList<BibleBookView>();
		for (String book : bookNames){
			BibleBookView view = new BibleBookView(book);
			temp.add(view);
		}
		return new ListView<BibleBookView>(temp, BibleBookView.TYPE, BibleBookView.VERSION);
	}
	
	/**
	 * Queries a verse from the Bible in order to get all the versions available in the index.
	 * @return
	 */
	private List<String> getUniqueVersionFields(){
		
		List<String> toReturn = new ArrayList<String>();
		
		SolrClient solr = this.ejb.getSolrClient();
		
		SolrQuery query = new SolrQuery();
		query.setRequestHandler("/select");
		query.setQuery("*:*");
		query.setFilterQueries("book:Gen AND chapter:1 AND verse:1");
		QueryResponse queryResponse;
		try{
			queryResponse = solr.query(query);
		}
		catch(SolrServerException | IOException e){
			return toReturn;
		}
		SolrDocumentList list = queryResponse.getResults();
		try{
			SolrDocument doc = list.get(0);
			Collection<String> fieldNames = doc.getFieldNames();
			for (String name : fieldNames){
				if (name.length()>=12){
					String substring = name.substring(3, 11);
					if ("_version".equals(substring)){
						String abbv = name.substring(0, 3);
						toReturn.add(abbv);
					}
				}
			}
		}
		catch(Exception e){
			return toReturn;
		}
		
		return toReturn;
	}
	
	/**
	 * Returns the List of all the unique elements of the field indicated by facetFieldName
	 * @param facetFieldName
	 * @return
	 * @throws Exception
	 */
	private List<String> getUniqueElements(String facetFieldName) throws Exception{
		SolrClient solr = this.ejb.getSolrClient();
		
		//1. Doing the authors query
		SolrQuery query = new SolrQuery();
		query.setRequestHandler("/select");
		query.setFacet(true);
		query.set("facet.field", facetFieldName);
		query.set("facet.sort", "index");
		QueryResponse queryResponse;
		try{
			queryResponse = solr.query(query);
		}
		catch (SolrServerException | IOException e){
			throw e;
		}
		
		List<FacetField> facetFields = queryResponse.getFacetFields();
		if (facetFields.size()<1) throw new Exception();
		FacetField facetField = facetFields.get(0);
		List<Count> values = facetField.getValues();
		List<String> valueNames = new ArrayList<String>();
		for (Count count : values){
			valueNames.add(count.getName());
		}
		return valueNames;
	}
	
	
	/**
	 * Returns the list of authors of the books of the Bible
	 * @return
	 */
	public ListView<BibleAuthorView> getBibleAuthors(){
		return this.bibleAuthors;
	}
	
	/**
	 * Returns the list of sections of the books of the Bible
	 * @return
	 */
	public ListView<BibleSectionView> getBibleSections(){
		return this.bibleSections;
	}
	
	/**
	 * Returns the list of books in the Bible
	 * @return
	 */
	public ListView<BibleBookView> getBibleBooks(){
		return this.bibleBooks;
	}
	
	/**
	 * Returns the list of versions in the index.
	 * @return
	 */
	public ListView<BibleVersionView> getBibleVersions(){
		return this.bibleVersions;
	}
	
	/**
	 * Returns true if this is valid a book abbv, otherwise it returns false.
	 * @param bookAbbv
	 * @return
	 */
	public boolean isBook(String bookAbbv){
		if (this.bookAbbvs.contains(bookAbbv)) return true;
		return false;
	}
	
	/**
	 * Returns true if this is a valid author name, otherwise it returns false
	 * @param authorName
	 * @return
	 */
	public boolean isAuthor(String authorName){
		if (this.authorNames.contains(authorName)) return true;
		return false;
	}
	
	/**
	 * Returns true if this is a valid version abbv. Otherwise returns false
	 * @param versionAbbv
	 * @return
	 */
	public boolean isVersion(String versionAbbv){
		if (this.versionAbbvs.contains(versionAbbv)) return true;
		return false;
		
	}
	
	/**
	 * Returns true if this is a valid section name. Otherwise it returns false.
	 * @param section
	 * @return
	 */
	public boolean isSection(String section){
		if (this.sectionNames.contains(section)) return true;
		return false;
	}
}
