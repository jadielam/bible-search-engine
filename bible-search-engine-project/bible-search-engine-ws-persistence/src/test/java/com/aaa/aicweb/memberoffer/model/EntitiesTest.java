// **************************************************
//
// Modification Tracking:
// $Revision:$
// $LastChangedBy:$
// $LastChangedDate:$
//
// **************************************************
package com.aaa.aicweb.memberoffer.model;

import java.sql.Connection;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import junit.framework.TestCase;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.apache.derby.jdbc.EmbeddedDataSource;
import org.apache.derby.tools.ij;
import org.apache.naming.java.javaURLContextFactory;


/**
 * @author jdearmas
 *
 * @since  
 */
public abstract class EntitiesTest {

	/**
	 * The entity manager factory
	 */
	static EntityManagerFactory emFactory;
	
	/**
	 * The entity manager class
	 */
	static EntityManager em;
	
	/**
	 * Creates an in-memory database using Derby and creates an entity manager
	 * for that database, and creates all the tables in the newly created 
	 * database.
	 * @throws Exception whenever something goes wrong.
	 */
	//@BeforeClass
	public static void setUpClass() throws Exception {
		
		//1. Create the entity manager to be used by all the test methods
		emFactory = Persistence.createEntityManagerFactory("MemberOfferPersistence");
		em = emFactory.createEntityManager();
				
	}
	
	
	/**
	 * <p>After tests are done, it closes the entityManager and the entityFactory
	 * and it also unbinds the non-jta-datasource that was created before the tests.
	 * </p> 
	 * @throws Exception whenever some closing of resources goes wrong.
	 */
	//@AfterClass
	public static void tearDownClass() throws Exception {
		
		em.close();
		emFactory.close();
		
		InitialContext ic = new InitialContext();
		
		ic.unbind("java:comp/env/jdbc/MemberOfferDS");
	}
}
