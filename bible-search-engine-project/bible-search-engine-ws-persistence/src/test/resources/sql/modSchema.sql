USE [sycs_new_jadiel]
GO
/****** Object:  Table [dbo].[source_type]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[source_type](
	[source_type_cd] [char](1) NOT NULL,
	[source_type_desc] [varchar](20) NOT NULL,
	[lst_updt_dt] [datetime] NOT NULL,
 CONSTRAINT [pk1_source_type] PRIMARY KEY NONCLUSTERED 
(
	[source_type_cd] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[phn_nbr_typ]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[phn_nbr_typ](
	[phn_nbr_typ_cd] [char](1) NOT NULL,
	[phn_nbr_typ_desc] [varchar](30) NOT NULL,
	[lst_updt_dt] [datetime] NOT NULL,
 CONSTRAINT [pk_phn_nbr_typ] PRIMARY KEY NONCLUSTERED 
(
	[phn_nbr_typ_cd] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[next_value]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[next_value](
	[table_nm] [varchar](20) NOT NULL,
	[column_nm] [varchar](30) NOT NULL,
	[id] [numeric](10, 0) NOT NULL,
	[lst_updt_dt] [datetime] NOT NULL,
 CONSTRAINT [pk_next_value] PRIMARY KEY NONCLUSTERED 
(
	[table_nm] ASC,
	[column_nm] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sycs_coord]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sycs_coord](
	[sycs_coord_id] [int] IDENTITY(1,1) NOT NULL,
	[sycs_coord_fst_nm] [varchar](25) NOT NULL,
	[sycs_coord_lst_nm] [varchar](25) NOT NULL,
	[sycs_coord_email_ad] [varchar](255) NOT NULL,
	[sycs_coord_titl_nm] [varchar](50) NULL,
	[sycs_coord_email_notif] [char](1) NULL,
	[receive_new_offer_notify] [char](1) NOT NULL,
	[recieve_email_updates_notify] [char](1) NULL,
	[lst_updt_dt] [datetime] NOT NULL,
 CONSTRAINT [pk1_sycs_coord] PRIMARY KEY NONCLUSTERED 
(
	[sycs_coord_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sycs_coord_secur_grp]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sycs_coord_secur_grp](
	[sycs_coord_secur_grp_cd] [char](1) NOT NULL,
	[sycs_coord_secur_grp_desc] [varchar](30) NOT NULL,
	[sycs_coord_secur_level_id] [tinyint] NOT NULL,
	[lst_updt_dt] [datetime] NOT NULL,
 CONSTRAINT [pk1_sycs_coord_secur_grp] PRIMARY KEY NONCLUSTERED 
(
	[sycs_coord_secur_grp_cd] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[offer_for_type_group]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[offer_for_type_group](
	[offer_for_type_group_cd] [char](1) NOT NULL,
	[offer_for_type_group_desc] [varchar](20) NULL,
	[lst_updt_dt] [datetime] NOT NULL,
 CONSTRAINT [pk1_offer_for_type_group] PRIMARY KEY NONCLUSTERED 
(
	[offer_for_type_group_cd] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mrg_clb]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mrg_clb](
	[Mrg_Clb_Id] [numeric](12, 0) IDENTITY(1,1) NOT NULL,
	[Trans_Appl_Meth_Id] [numeric](10, 0) NULL,
	[Mrg_Pct] [decimal](6, 3) NULL,
	[Mrg_Dte] [datetime] NULL,
	[gain_clb_id] [char](3) NOT NULL,
	[lose_clb_id] [char](3) NOT NULL,
	[mrg_stat] [char](1) NULL,
	[pay_ind] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[lcl_mrcht_sts_history]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[lcl_mrcht_sts_history](
	[lcl_mrcht_sts_history_id] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[lcl_mrcht_id] [numeric](10, 0) NOT NULL,
	[status_dt] [datetime] NULL,
	[status] [varchar](10) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[lcl_mrcht_avail_sts]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[lcl_mrcht_avail_sts](
	[st_prvnc_cd] [char](2) NOT NULL,
	[lcl_mrcht_id] [numeric](10, 0) NOT NULL,
	[lst_updt_dt] [datetime] NOT NULL,
	[lcl_mrcht_hot_deal_id] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[lcl_mrcht_avail_country]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[lcl_mrcht_avail_country](
	[lcl_mrcht_id] [numeric](10, 0) NOT NULL,
	[cntry_cd] [char](3) NOT NULL,
	[lst_updt_dt] [datetime] NOT NULL,
	[lcl_mrcht_hot_deal_id] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[lcl_mrcht_avail_clb]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[lcl_mrcht_avail_clb](
	[lcl_mrcht_id] [numeric](10, 0) NOT NULL,
	[lcl_mrcht_hot_deal_id] [numeric](9, 0) NOT NULL,
	[clb_id] [char](3) NOT NULL,
	[lst_updt_dt] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[merchant]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[merchant](
	[lcl_mrcht_id] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[lcl_mrcht_nm] [varchar](120) NOT NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[merchant] ADD [lcl_mrcht_web_ad] [varchar](255) NULL
SET ANSI_PADDING ON
ALTER TABLE [dbo].[merchant] ADD [lcl_mrcht_logo_file_nm] [varchar](100) NULL
ALTER TABLE [dbo].[merchant] ADD [lst_updt_dt] [datetime] NOT NULL
ALTER TABLE [dbo].[merchant] ADD  CONSTRAINT [pk1_merchant] PRIMARY KEY NONCLUSTERED 
(
	[lcl_mrcht_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[clb]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[clb](
	[Clb_Id] [char](3) NOT NULL,
	[Clb_Typ_Id] [numeric](10, 0) NOT NULL,
	[Clb_Nm] [varchar](40) NOT NULL,
	[Addr_Ln_1] [varchar](35) NULL,
	[Addr_Ln_2] [varchar](35) NULL,
	[Zip_Cd] [char](10) NULL,
	[Cty_Nm] [varchar](28) NULL,
	[St_Cd] [char](2) NULL,
	[Web_Addr_Id] [char](40) NULL,
	[active_ind] [bit] NULL,
 CONSTRAINT [pk1_clb] PRIMARY KEY NONCLUSTERED 
(
	[Clb_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[category]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[category](
	[cat_typ_id] [numeric](10, 0) NOT NULL,
	[cat_typ_desc] [varchar](40) NOT NULL,
	[lst_updt_dt] [datetime] NOT NULL,
	[locale] [varchar](5) NOT NULL,
	[cat_id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [pk1_category] PRIMARY KEY NONCLUSTERED 
(
	[cat_typ_id] ASC,
	[locale] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[busn_acss_typ]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[busn_acss_typ](
	[busn_acss_id] [int] IDENTITY(1,1) NOT NULL,
	[busn_acss_typ_id] [numeric](10, 0) NOT NULL,
	[busn_acss_typ_desc] [varchar](50) NOT NULL,
	[lst_updt_dt] [datetime] NOT NULL,
	[locale] [varchar](5) NOT NULL,
	[sort_order] [int] NULL,
 CONSTRAINT [pk1_busn_acss_typ] PRIMARY KEY NONCLUSTERED 
(
	[busn_acss_typ_id] ASC,
	[locale] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[image_code]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[image_code](
	[image_code_id] [int] IDENTITY(1,1) NOT NULL,
	[type] [char](1) NOT NULL,
	[description] [varchar](50) NULL,
 CONSTRAINT [pk_image_code] PRIMARY KEY NONCLUSTERED 
(
	[image_code_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[discount_type]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[discount_type](
	[discount_id] [int] IDENTITY(1,1) NOT NULL,
	[discount_type_code] [varchar](10) NOT NULL,
	[discount_type_desc] [varchar](100) NOT NULL,
	[active_ind] [char](1) NOT NULL,
	[sort_num] [tinyint] NOT NULL,
 CONSTRAINT [pk1_discount_type] PRIMARY KEY NONCLUSTERED 
(
	[discount_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[deleted_lcl_mrcht_loc]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[deleted_lcl_mrcht_loc](
	[lcl_mrcht_id] [numeric](10, 0) NULL,
	[lcl_mrcht_loc_id] [numeric](10, 0) NULL,
	[lcl_mrcht_loc_nm] [varchar](120) NULL,
	[username] [varchar](120) NULL,
	[del_dt] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cty]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[cty](
	[cty_cd] [char](13) NOT NULL,
	[st_prvnc_cd] [char](2) NOT NULL,
	[cty_nm] [varchar](255) NULL,
	[cty_aaa_cd] [char](9) NULL,
	[lst_updt_dt] [datetime] NOT NULL,
 CONSTRAINT [pk_cty] PRIMARY KEY NONCLUSTERED 
(
	[cty_cd] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[core_data_change_update_log]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[core_data_change_update_log](
	[core_data_change_log_id] [int] NOT NULL,
	[core_data_name] [varchar](100) NULL,
	[address_1] [varchar](100) NOT NULL,
	[address_2] [varchar](100) NOT NULL,
	[cty_nm] [varchar](50) NULL,
	[st_prvnc_cd] [char](2) NULL,
	[zip_pstl_cd] [char](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[core_data_change_type]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[core_data_change_type](
	[change_type_cd] [char](1) NOT NULL,
	[change_desc] [varchar](255) NOT NULL,
	[lst_updt_dt] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[core_data_change_log]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[core_data_change_log](
	[core_data_change_log_id] [int] IDENTITY(1,1) NOT NULL,
	[source_id] [varchar](20) NOT NULL,
	[clb_id] [char](3) NULL,
	[source_type_cd] [char](1) NOT NULL,
	[lcl_mrcht_id] [numeric](10, 0) NULL,
	[lcl_mrcht_loc_id] [numeric](10, 0) NULL,
	[change_type_cd] [char](1) NOT NULL,
	[change_details] [text] NULL,
	[change_dt] [datetime] NULL,
	[core_data_name] [varchar](100) NULL,
	[address_1] [varchar](100) NOT NULL,
	[address_2] [varchar](100) NOT NULL,
	[cty_nm] [varchar](50) NULL,
	[st_prvnc_cd] [char](2) NULL,
	[zip_pstl_cd] [char](10) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cntry]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[cntry](
	[cntry_cd] [char](3) NOT NULL,
	[cntry_nm] [varchar](40) NULL,
	[lst_updt_dt] [datetime] NOT NULL,
 CONSTRAINT [pk_cntry] PRIMARY KEY NONCLUSTERED 
(
	[cntry_cd] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[club_merchant_offer]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[club_merchant_offer](
	[club_merchant_offer_id] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[lcl_mrcht_id] [numeric](10, 0) NOT NULL,
	[clb_id] [char](3) NOT NULL,
	[lst_updt_dt] [datetime] NOT NULL,
 CONSTRAINT [pk1_club_merchant_offer] PRIMARY KEY NONCLUSTERED 
(
	[club_merchant_offer_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[clb_phone]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[clb_phone](
	[Clb_Id] [char](3) NOT NULL
) ON [PRIMARY]
SET ANSI_PADDING ON
ALTER TABLE [dbo].[clb_phone] ADD [Phone_Type_Cd] [varchar](1) NOT NULL
ALTER TABLE [dbo].[clb_phone] ADD [Phn_Area_Cd] [numeric](3, 0) NULL
ALTER TABLE [dbo].[clb_phone] ADD [Phn_Num] [numeric](7, 0) NULL
ALTER TABLE [dbo].[clb_phone] ADD  CONSTRAINT [pk1_clb_phone] PRIMARY KEY NONCLUSTERED 
(
	[Clb_Id] ASC,
	[Phone_Type_Cd] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[merchant_phone]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[merchant_phone](
	[phone_id] [bigint] IDENTITY(1,1) NOT NULL,
	[phn_nbr_typ_cd] [char](1) NOT NULL,
	[phone_type] [varchar](10) NULL,
	[lcl_mrcht_id] [numeric](10, 0) NOT NULL,
	[lcl_mrcht_phn_nbr_area_nbr] [char](3) NOT NULL,
	[lcl_mrcht_phn_nbr_xchg_nbr] [char](3) NOT NULL,
	[lcl_mrcht_phn_nbr_sufx_nbr] [char](4) NOT NULL,
	[lcl_mrcht_phn_nbr_ext_nbr] [char](5) NULL,
	[lst_updt_dt] [datetime] NOT NULL,
 CONSTRAINT [pk1_merchant_phone] PRIMARY KEY NONCLUSTERED 
(
	[phone_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[offer_for_type_cat]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[offer_for_type_cat](
	[offer_for_type_group_cd] [char](1) NOT NULL,
	[offer_for_type_cat_cd] [char](6) NOT NULL,
	[offer_for_type_cat_desc] [varchar](20) NULL,
	[sort_num] [tinyint] NOT NULL,
	[lst_updt_dt] [datetime] NOT NULL,
 CONSTRAINT [pk1_offer_for_type_cat] PRIMARY KEY NONCLUSTERED 
(
	[offer_for_type_group_cd] ASC,
	[offer_for_type_cat_cd] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[merchant_contact]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[merchant_contact](
	[contact_id] [int] IDENTITY(1,1) NOT NULL,
	[lcl_mrcht_id] [numeric](10, 0) NOT NULL,
	[lcl_mrcht_cntc_fst_nm] [varchar](40) NOT NULL,
	[lcl_mrcht_cntc_lst_nm] [varchar](40) NOT NULL,
	[lcl_mrcht_cntc_email_ad] [varchar](255) NULL,
	[lst_updt_dt] [datetime] NOT NULL,
 CONSTRAINT [pk1_merchant_contact] PRIMARY KEY NONCLUSTERED 
(
	[contact_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sycs_coord_phn_nbr]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[sycs_coord_phn_nbr](
	[sycs_coord_id] [int] NOT NULL,
	[phn_nbr_typ_cd] [char](1) NOT NULL,
	[sycs_coord_phn_nbr_area_nbr] [char](3) NOT NULL,
	[sycs_coord_phn_nbr_xchg_nbr] [char](3) NOT NULL,
	[sycs_coord_phn_nbr_sufx_nbr] [char](4) NOT NULL,
	[lst_updt_dt] [datetime] NOT NULL,
 CONSTRAINT [pk1_sycs_coord_phn_nbr] PRIMARY KEY NONCLUSTERED 
(
	[sycs_coord_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sycs_coord_clb]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sycs_coord_clb](
	[sycs_coord_id] [int] NOT NULL,
	[clb_id] [char](3) NOT NULL,
	[sycs_coord_secur_grp_cd] [char](1) NOT NULL,
 CONSTRAINT [pk1_sycs_coord_clb] PRIMARY KEY NONCLUSTERED 
(
	[sycs_coord_id] ASC,
	[clb_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sub_category]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sub_category](
	[cat_typ_id] [numeric](10, 0) NOT NULL,
	[subcat_id] [numeric](10, 0) NOT NULL,
	[subcat_desc] [varchar](100) NOT NULL,
	[lst_updt_dt] [datetime] NOT NULL,
	[locale] [varchar](5) NOT NULL,
	[cat_id] [int] NULL,
	[sub_id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [pk1_sub_category] PRIMARY KEY NONCLUSTERED 
(
	[subcat_id] ASC,
	[locale] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[st_provnc]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[st_provnc](
	[st_prvnc_cd] [char](2) NOT NULL,
	[cntry_cd] [char](3) NOT NULL,
	[st_prvnc_nm] [varchar](30) NULL,
	[lst_updt_dt] [datetime] NOT NULL,
 CONSTRAINT [pk_st_provnc] PRIMARY KEY NONCLUSTERED 
(
	[st_prvnc_cd] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[offer_for_type_subcat]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[offer_for_type_subcat](
	[offer_for_type_group_cd] [char](1) NOT NULL,
	[offer_for_type_cat_cd] [char](6) NOT NULL,
	[offer_for_type_subcat_cd] [varchar](6) NOT NULL,
	[sort_num] [tinyint] NOT NULL,
	[lst_updt_dt] [datetime] NOT NULL,
 CONSTRAINT [pk1_offer_for_type_subcat] PRIMARY KEY NONCLUSTERED 
(
	[offer_for_type_group_cd] ASC,
	[offer_for_type_cat_cd] ASC,
	[offer_for_type_subcat_cd] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[merchant_address]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[merchant_address](
	[address_id] [bigint] IDENTITY(1,1) NOT NULL,
	[lcl_mrcht_id] [numeric](10, 0) NOT NULL,
	[address_type] [varchar](10) NULL,
	[lcl_mrcht_ad] [varchar](255) NULL,
	[lcl_mrcht_ad2] [varchar](200) NULL,
	[lcl_mrcht_zip_pstl_cd] [char](10) NULL,
	[st_prvnc_cd] [char](2) NULL,
	[cty_nm] [varchar](255) NULL,
	[lcl_mrcht_cnty_nm] [varchar](40) NULL,
	[geocode_latitude] [varchar](50) NULL,
	[geocode_longitude] [varchar](50) NULL,
	[lst_updt_dt] [datetime] NOT NULL,
 CONSTRAINT [pk1_merchant_address] PRIMARY KEY NONCLUSTERED 
(
	[address_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tdr_source_type_cd]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tdr_source_type_cd](
	[address_id] [bigint] IDENTITY(1,1) NOT NULL,
	[source_type_cd] [char](1) NULL,
	[source_id] [numeric](12, 0) NULL,
	[linked_date] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[manual_override]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[manual_override](
	[address_id] [bigint] IDENTITY(1,1) NOT NULL,
	[manual_override_ind] [bit] NULL,
	[manual_override_by] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[offer_for_type]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[offer_for_type](
	[offer_for_type_id] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[offer_for_type_group_cd] [char](1) NOT NULL,
	[offer_for_type_cat_cd] [char](6) NULL,
	[offer_for_type_subcat_cd] [varchar](6) NULL,
	[offer_for_type_desc] [varchar](80) NOT NULL,
	[sort_num] [tinyint] NOT NULL,
	[lst_updt_dt] [datetime] NOT NULL,
 CONSTRAINT [pk1_offer_for_type] PRIMARY KEY NONCLUSTERED 
(
	[offer_for_type_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ofr_typ]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ofr_typ](
	[ofr_id] [int] IDENTITY(1,1) NOT NULL,
	[ofr_typ_id] [numeric](10, 0) NOT NULL,
	[ofr_typ_desc] [varchar](100) NOT NULL,
	[lst_updt_dt] [datetime] NOT NULL,
	[locale] [varchar](5) NOT NULL,
	[sort_num] [tinyint] NULL,
 CONSTRAINT [pk1_ofr_typ] PRIMARY KEY NONCLUSTERED 
(
	[ofr_typ_id] ASC,
	[locale] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[merchant_offer]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[merchant_offer](
	[lcl_mrcht_id] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[lcl_mrcht_sts_desc] [char](10) NOT NULL,
	[lcl_mrcht_ofr_init_contr_dt] [datetime] NOT NULL,
	[lcl_mrcht_ofr_expir_dt] [datetime] NOT NULL,
	[ofr_typ_amt] [numeric](8, 2) NULL,
	[first_approved_date] [datetime] NULL
) ON [PRIMARY]
SET ANSI_PADDING ON
ALTER TABLE [dbo].[merchant_offer] ADD [lcl_mrcht_tb_splr_cd] [char](3) NULL
ALTER TABLE [dbo].[merchant_offer] ADD [lcl_mrcht_ofr_assoc] [char](3) NOT NULL
ALTER TABLE [dbo].[merchant_offer] ADD [lcl_mrcht_del_dt] [datetime] NULL
ALTER TABLE [dbo].[merchant_offer] ADD [last_updated_by] [varchar](100) NULL
ALTER TABLE [dbo].[merchant_offer] ADD [lst_updt_dt] [datetime] NOT NULL
ALTER TABLE [dbo].[merchant_offer] ADD [discount_id] [int] NULL
ALTER TABLE [dbo].[merchant_offer] ADD [sub_id] [int] NULL
ALTER TABLE [dbo].[merchant_offer] ADD [ofr_id] [int] NULL
ALTER TABLE [dbo].[merchant_offer] ADD  CONSTRAINT [pk1_merchant_offer] PRIMARY KEY NONCLUSTERED 
(
	[lcl_mrcht_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[aar_core_data_sycs]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[aar_core_data_sycs](
	[lcl_mrcht_id] [numeric](10, 0) NOT NULL,
	[svc_prov_sycs_disc_prts] [tinyint] NOT NULL,
	[svc_prov_sycs_disc_lbr] [tinyint] NOT NULL,
	[svc_prov_sycs_commt_tx] [varchar](255) NULL,
	[lst_updt_dt] [datetime] NOT NULL,
	[parts_labor_discount_dollar_limit] [decimal](18, 0) NOT NULL,
	[parts_labor_discount_exclusions] [varchar](255) NULL,
	[sycs_typ_cd] [char](1) NULL,
 CONSTRAINT [pk1_aar_core_data_sycs] PRIMARY KEY NONCLUSTERED 
(
	[lcl_mrcht_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Image_Offer]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Image_Offer](
	[image_id] [int] IDENTITY(1,1) NOT NULL,
	[lcl_mrcht_id] [numeric](10, 0) NOT NULL,
 CONSTRAINT [pk1_Image_Offer] PRIMARY KEY NONCLUSTERED 
(
	[image_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[merchant_keyword_search]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[merchant_keyword_search](
	[lcl_mrcht_id] [numeric](10, 0) NOT NULL,
	[Keyword_search] [varchar](255) NOT NULL,
 CONSTRAINT [pk1_merchant_keyword_search] PRIMARY KEY NONCLUSTERED 
(
	[lcl_mrcht_id] ASC,
	[Keyword_search] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[nat_prtnr_blk]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[nat_prtnr_blk](
	[Clb_Id] [char](3) NOT NULL,
	[lcl_mrcht_id] [numeric](10, 0) NOT NULL
) ON [PRIMARY]
SET ANSI_PADDING ON
ALTER TABLE [dbo].[nat_prtnr_blk] ADD [block_clb_id] [char](3) NOT NULL
ALTER TABLE [dbo].[nat_prtnr_blk] ADD [lst_updt_dt] [datetime] NOT NULL
ALTER TABLE [dbo].[nat_prtnr_blk] ADD  CONSTRAINT [pk_nat_prtnr_blk] PRIMARY KEY NONCLUSTERED 
(
	[Clb_Id] ASC,
	[lcl_mrcht_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[merchant_offer_category]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[merchant_offer_category](
	[cat_typ_id] [numeric](10, 0) NOT NULL,
	[subcat_id] [numeric](10, 0) NOT NULL,
	[lcl_mrcht_id] [numeric](10, 0) NOT NULL,
	[lst_updt_dt] [datetime] NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [pk1_merchant_offer_category] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[merchant_hot_deal]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[merchant_hot_deal](
	[lcl_mrcht_hot_deal_id] [numeric](9, 0) IDENTITY(1,1) NOT NULL,
	[lcl_mrcht_id] [numeric](10, 0) NOT NULL,
	[lcl_mrcht_hot_deal_eff_dt] [datetime] NOT NULL,
	[lcl_mrcht_hot_deal_expir_dt] [datetime] NOT NULL,
	[partner_offer_only_ind] [bit] NOT NULL,
	[ofr_typ_amt] [numeric](8, 2) NULL,
	[lst_updt_dt] [datetime] NOT NULL,
 CONSTRAINT [pk1_merchant_hot_deal] PRIMARY KEY NONCLUSTERED 
(
	[lcl_mrcht_hot_deal_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[merchant_busn_acss]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[merchant_busn_acss](
	[lcl_mrcht_id] [numeric](10, 0) NOT NULL,
	[lst_updt_dt] [datetime] NOT NULL,
	[busn_acss_id] [int] NOT NULL,
 CONSTRAINT [pk1_merchant_busn_acss] PRIMARY KEY NONCLUSTERED 
(
	[lcl_mrcht_id] ASC,
	[busn_acss_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[merchant_txt]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[merchant_txt](
	[lcl_mrcht_id] [numeric](10, 0) NOT NULL,
	[locale] [varchar](5) NOT NULL,
	[lcl_mrcht_svc_desc] [text] NOT NULL,
	[lcl_mrcht_ofr_desc] [text] NOT NULL,
	[lcl_mrcht_ofr_restrict] [text] NULL,
	[short_desc] [varchar](100) NULL,
	[ofr_title] [varchar](50) NULL,
	[source_data_ofr_desc] [text] NULL,
	[source_data_ofr_restrict] [text] NULL,
	[source_data_short_desc] [varchar](100) NOT NULL,
	[club_data_ofr_desc] [text] NULL,
	[club_data_ofr_restrict] [text] NULL,
	[club_data_short_desc] [varchar](100) NULL,
	[offer_restrictions_url] [varchar](255) NULL,
 CONSTRAINT [pk1_Lcl_mrcht_txt] PRIMARY KEY NONCLUSTERED 
(
	[lcl_mrcht_id] ASC,
	[locale] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[merchant_offer_hot_deal_category]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[merchant_offer_hot_deal_category](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[lcl_mrcht_hot_deal_id] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[merchant_image]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[merchant_image](
	[lcl_mrcht_id] [numeric](10, 0) NOT NULL,
	[type] [char](1) NULL,
	[image_name] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[merchant_hot_deal_txt]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[merchant_hot_deal_txt](
	[lcl_mrcht_id] [numeric](10, 0) NOT NULL,
	[lcl_mrcht_hot_deal_link_lbl_nm] [varchar](35) NULL,
	[lcl_mrcht_hot_deal_ofr] [varchar](500) NULL,
	[locale] [varchar](5) NOT NULL,
	[short_desc] [varchar](100) NULL,
	[restrictions_desc] [text] NULL,
	[lcl_mrcht_hot_deal_id] [numeric](9, 0) NOT NULL,
	[offer_restrictions_url] [varchar](255) NULL,
 CONSTRAINT [pk1_merchantt_hot_deal_txt] PRIMARY KEY NONCLUSTERED 
(
	[lcl_mrcht_hot_deal_id] ASC,
	[lcl_mrcht_id] ASC,
	[locale] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[merchant_hot_deal_busn_acss]    Script Date: 02/06/2015 09:46:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[merchant_hot_deal_busn_acss](
	[busn_acss_typ_id] [numeric](10, 0) NOT NULL,
	[lcl_mrcht_hot_deal_id] [numeric](9, 0) NOT NULL,
	[lst_updt_dt] [datetime] NULL,
	[locale] [varchar](5) NULL,
 CONSTRAINT [pk1_merchant_hot_deal_busn_acss] PRIMARY KEY NONCLUSTERED 
(
	[busn_acss_typ_id] ASC,
	[lcl_mrcht_hot_deal_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  ForeignKey [FK_merchant_offer_aar_core_data_sycs]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[aar_core_data_sycs]  WITH NOCHECK ADD  CONSTRAINT [FK_merchant_offer_aar_core_data_sycs] FOREIGN KEY([lcl_mrcht_id])
REFERENCES [dbo].[merchant_offer] ([lcl_mrcht_id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[aar_core_data_sycs] CHECK CONSTRAINT [FK_merchant_offer_aar_core_data_sycs]
GO
/****** Object:  ForeignKey [FK_clb_phone]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[clb_phone]  WITH NOCHECK ADD  CONSTRAINT [FK_clb_phone] FOREIGN KEY([Clb_Id])
REFERENCES [dbo].[clb] ([Clb_Id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[clb_phone] CHECK CONSTRAINT [FK_clb_phone]
GO
/****** Object:  ForeignKey [FK_club_merchant_offer_clb_id]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[club_merchant_offer]  WITH NOCHECK ADD  CONSTRAINT [FK_club_merchant_offer_clb_id] FOREIGN KEY([clb_id])
REFERENCES [dbo].[clb] ([Clb_Id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[club_merchant_offer] CHECK CONSTRAINT [FK_club_merchant_offer_clb_id]
GO
/****** Object:  ForeignKey [FK_Image_Offer_Merchant_Offer]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[Image_Offer]  WITH NOCHECK ADD  CONSTRAINT [FK_Image_Offer_Merchant_Offer] FOREIGN KEY([lcl_mrcht_id])
REFERENCES [dbo].[merchant_offer] ([lcl_mrcht_id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[Image_Offer] CHECK CONSTRAINT [FK_Image_Offer_Merchant_Offer]
GO
/****** Object:  ForeignKey [FK_manual_override_merchant_address]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[manual_override]  WITH NOCHECK ADD  CONSTRAINT [FK_manual_override_merchant_address] FOREIGN KEY([address_id])
REFERENCES [dbo].[merchant_address] ([address_id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[manual_override] CHECK CONSTRAINT [FK_manual_override_merchant_address]
GO
/****** Object:  ForeignKey [FK_merchant_address]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[merchant_address]  WITH NOCHECK ADD  CONSTRAINT [FK_merchant_address] FOREIGN KEY([lcl_mrcht_id])
REFERENCES [dbo].[merchant] ([lcl_mrcht_id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[merchant_address] CHECK CONSTRAINT [FK_merchant_address]
GO
/****** Object:  ForeignKey [FK_merchant_address_st_provnc]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[merchant_address]  WITH NOCHECK ADD  CONSTRAINT [FK_merchant_address_st_provnc] FOREIGN KEY([st_prvnc_cd])
REFERENCES [dbo].[st_provnc] ([st_prvnc_cd])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[merchant_address] CHECK CONSTRAINT [FK_merchant_address_st_provnc]
GO
/****** Object:  ForeignKey [FK_busn_acss_id_busn_acss_typ]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[merchant_busn_acss]  WITH NOCHECK ADD  CONSTRAINT [FK_busn_acss_id_busn_acss_typ] FOREIGN KEY([busn_acss_id])
REFERENCES [dbo].[busn_acss_typ] ([busn_acss_id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[merchant_busn_acss] CHECK CONSTRAINT [FK_busn_acss_id_busn_acss_typ]
GO
/****** Object:  ForeignKey [FK_merchant_busn_merchant]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[merchant_busn_acss]  WITH NOCHECK ADD  CONSTRAINT [FK_merchant_busn_merchant] FOREIGN KEY([lcl_mrcht_id])
REFERENCES [dbo].[merchant_offer] ([lcl_mrcht_id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[merchant_busn_acss] CHECK CONSTRAINT [FK_merchant_busn_merchant]
GO
/****** Object:  ForeignKey [FK_merchant_contact]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[merchant_contact]  WITH NOCHECK ADD  CONSTRAINT [FK_merchant_contact] FOREIGN KEY([lcl_mrcht_id])
REFERENCES [dbo].[merchant] ([lcl_mrcht_id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[merchant_contact] CHECK CONSTRAINT [FK_merchant_contact]
GO
/****** Object:  ForeignKey [FK_merchant_hot_deal_merchant_category]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[merchant_hot_deal]  WITH NOCHECK ADD  CONSTRAINT [FK_merchant_hot_deal_merchant_category] FOREIGN KEY([lcl_mrcht_id])
REFERENCES [dbo].[merchant_offer] ([lcl_mrcht_id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[merchant_hot_deal] CHECK CONSTRAINT [FK_merchant_hot_deal_merchant_category]
GO
/****** Object:  ForeignKey [FK_merchant_hot_deal_busn_acss_merchant_hot_deal]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[merchant_hot_deal_busn_acss]  WITH NOCHECK ADD  CONSTRAINT [FK_merchant_hot_deal_busn_acss_merchant_hot_deal] FOREIGN KEY([lcl_mrcht_hot_deal_id])
REFERENCES [dbo].[merchant_hot_deal] ([lcl_mrcht_hot_deal_id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[merchant_hot_deal_busn_acss] CHECK CONSTRAINT [FK_merchant_hot_deal_busn_acss_merchant_hot_deal]
GO
/****** Object:  ForeignKey [FK_merchantt_hot_deal_txt_merchant_hot_deal]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[merchant_hot_deal_txt]  WITH NOCHECK ADD  CONSTRAINT [FK_merchantt_hot_deal_txt_merchant_hot_deal] FOREIGN KEY([lcl_mrcht_hot_deal_id])
REFERENCES [dbo].[merchant_hot_deal] ([lcl_mrcht_hot_deal_id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[merchant_hot_deal_txt] CHECK CONSTRAINT [FK_merchantt_hot_deal_txt_merchant_hot_deal]
GO
/****** Object:  ForeignKey [FK_merchant_image_image_code]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[merchant_image]  WITH NOCHECK ADD  CONSTRAINT [FK_merchant_image_image_code] FOREIGN KEY([type])
REFERENCES [dbo].[image_code] ([type])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[merchant_image] CHECK CONSTRAINT [FK_merchant_image_image_code]
GO
/****** Object:  ForeignKey [FK_merchant_image_Image_Offer]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[merchant_image]  WITH NOCHECK ADD  CONSTRAINT [FK_merchant_image_Image_Offer] FOREIGN KEY([lcl_mrcht_id])
REFERENCES [dbo].[Image_Offer] ([lcl_mrcht_id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[merchant_image] CHECK CONSTRAINT [FK_merchant_image_Image_Offer]
GO
/****** Object:  ForeignKey [FK_merchant_keyword_search]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[merchant_keyword_search]  WITH NOCHECK ADD  CONSTRAINT [FK_merchant_keyword_search] FOREIGN KEY([lcl_mrcht_id])
REFERENCES [dbo].[merchant_offer] ([lcl_mrcht_id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[merchant_keyword_search] CHECK CONSTRAINT [FK_merchant_keyword_search]
GO
/****** Object:  ForeignKey [FK_merchant_offer]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[merchant_offer]  WITH NOCHECK ADD  CONSTRAINT [FK_merchant_offer] FOREIGN KEY([lcl_mrcht_id])
REFERENCES [dbo].[merchant] ([lcl_mrcht_id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[merchant_offer] CHECK CONSTRAINT [FK_merchant_offer]
GO
/****** Object:  ForeignKey [FK_merchant_offer_discount_type]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[merchant_offer]  WITH NOCHECK ADD  CONSTRAINT [FK_merchant_offer_discount_type] FOREIGN KEY([discount_id])
REFERENCES [dbo].[discount_type] ([discount_id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[merchant_offer] CHECK CONSTRAINT [FK_merchant_offer_discount_type]
GO
/****** Object:  ForeignKey [FK_merchant_offer_merchnat_club]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[merchant_offer]  WITH NOCHECK ADD  CONSTRAINT [FK_merchant_offer_merchnat_club] FOREIGN KEY([lcl_mrcht_id])
REFERENCES [dbo].[club_merchant_offer] ([lcl_mrcht_id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[merchant_offer] CHECK CONSTRAINT [FK_merchant_offer_merchnat_club]
GO
/****** Object:  ForeignKey [FK_merchant_offer_ofr_typ]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[merchant_offer]  WITH NOCHECK ADD  CONSTRAINT [FK_merchant_offer_ofr_typ] FOREIGN KEY([ofr_id])
REFERENCES [dbo].[ofr_typ] ([ofr_id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[merchant_offer] CHECK CONSTRAINT [FK_merchant_offer_ofr_typ]
GO
/****** Object:  ForeignKey [FK_merchant_offer_sub_category]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[merchant_offer]  WITH NOCHECK ADD  CONSTRAINT [FK_merchant_offer_sub_category] FOREIGN KEY([sub_id])
REFERENCES [dbo].[sub_category] ([sub_id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[merchant_offer] CHECK CONSTRAINT [FK_merchant_offer_sub_category]
GO
/****** Object:  ForeignKey [FK_merchant_offer_category_merchant_offer]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[merchant_offer_category]  WITH NOCHECK ADD  CONSTRAINT [FK_merchant_offer_category_merchant_offer] FOREIGN KEY([lcl_mrcht_id])
REFERENCES [dbo].[merchant_offer] ([lcl_mrcht_id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[merchant_offer_category] CHECK CONSTRAINT [FK_merchant_offer_category_merchant_offer]
GO
/****** Object:  ForeignKey [FK_offer_sub_category_merchant_offer]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[merchant_offer_hot_deal_category]  WITH NOCHECK ADD  CONSTRAINT [FK_offer_sub_category_merchant_offer] FOREIGN KEY([id])
REFERENCES [dbo].[merchant_offer_category] ([id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[merchant_offer_hot_deal_category] CHECK CONSTRAINT [FK_offer_sub_category_merchant_offer]
GO
/****** Object:  ForeignKey [FK_merchant_phone]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[merchant_phone]  WITH NOCHECK ADD  CONSTRAINT [FK_merchant_phone] FOREIGN KEY([lcl_mrcht_id])
REFERENCES [dbo].[merchant] ([lcl_mrcht_id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[merchant_phone] CHECK CONSTRAINT [FK_merchant_phone]
GO
/****** Object:  ForeignKey [FK_merchant_phone_phn_nbr_typ]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[merchant_phone]  WITH NOCHECK ADD  CONSTRAINT [FK_merchant_phone_phn_nbr_typ] FOREIGN KEY([phn_nbr_typ_cd])
REFERENCES [dbo].[phn_nbr_typ] ([phn_nbr_typ_cd])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[merchant_phone] CHECK CONSTRAINT [FK_merchant_phone_phn_nbr_typ]
GO
/****** Object:  ForeignKey [FK_merchant_txt]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[merchant_txt]  WITH NOCHECK ADD  CONSTRAINT [FK_merchant_txt] FOREIGN KEY([lcl_mrcht_id])
REFERENCES [dbo].[merchant_offer] ([lcl_mrcht_id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[merchant_txt] CHECK CONSTRAINT [FK_merchant_txt]
GO
/****** Object:  ForeignKey [FK_nat_prtnr_blk_clb]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[nat_prtnr_blk]  WITH NOCHECK ADD  CONSTRAINT [FK_nat_prtnr_blk_clb] FOREIGN KEY([Clb_Id])
REFERENCES [dbo].[clb] ([Clb_Id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[nat_prtnr_blk] CHECK CONSTRAINT [FK_nat_prtnr_blk_clb]
GO
/****** Object:  ForeignKey [FK_nat_prtnr_blk_merchant_offer]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[nat_prtnr_blk]  WITH NOCHECK ADD  CONSTRAINT [FK_nat_prtnr_blk_merchant_offer] FOREIGN KEY([lcl_mrcht_id])
REFERENCES [dbo].[merchant_offer] ([lcl_mrcht_id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[nat_prtnr_blk] CHECK CONSTRAINT [FK_nat_prtnr_blk_merchant_offer]
GO
/****** Object:  ForeignKey [FK_offer_for_type_offer_for_type_subcat]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[offer_for_type]  WITH NOCHECK ADD  CONSTRAINT [FK_offer_for_type_offer_for_type_subcat] FOREIGN KEY([offer_for_type_group_cd], [offer_for_type_cat_cd], [offer_for_type_subcat_cd])
REFERENCES [dbo].[offer_for_type_subcat] ([offer_for_type_group_cd], [offer_for_type_cat_cd], [offer_for_type_subcat_cd])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[offer_for_type] CHECK CONSTRAINT [FK_offer_for_type_offer_for_type_subcat]
GO
/****** Object:  ForeignKey [FK_offer_for_type_cat_offer_for_type_group]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[offer_for_type_cat]  WITH NOCHECK ADD  CONSTRAINT [FK_offer_for_type_cat_offer_for_type_group] FOREIGN KEY([offer_for_type_group_cd])
REFERENCES [dbo].[offer_for_type_group] ([offer_for_type_group_cd])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[offer_for_type_cat] CHECK CONSTRAINT [FK_offer_for_type_cat_offer_for_type_group]
GO
/****** Object:  ForeignKey [FK_offer_for_type_cat_offer_for_type_subcat]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[offer_for_type_subcat]  WITH NOCHECK ADD  CONSTRAINT [FK_offer_for_type_cat_offer_for_type_subcat] FOREIGN KEY([offer_for_type_group_cd], [offer_for_type_cat_cd])
REFERENCES [dbo].[offer_for_type_cat] ([offer_for_type_group_cd], [offer_for_type_cat_cd])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[offer_for_type_subcat] CHECK CONSTRAINT [FK_offer_for_type_cat_offer_for_type_subcat]
GO
/****** Object:  ForeignKey [FK_offer_for_type_subcat_offer_for_type_group]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[offer_for_type_subcat]  WITH NOCHECK ADD  CONSTRAINT [FK_offer_for_type_subcat_offer_for_type_group] FOREIGN KEY([offer_for_type_group_cd])
REFERENCES [dbo].[offer_for_type_group] ([offer_for_type_group_cd])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[offer_for_type_subcat] CHECK CONSTRAINT [FK_offer_for_type_subcat_offer_for_type_group]
GO
/****** Object:  ForeignKey [FK_offer_for_type_ofr_typ]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[ofr_typ]  WITH NOCHECK ADD  CONSTRAINT [FK_offer_for_type_ofr_typ] FOREIGN KEY([ofr_typ_id])
REFERENCES [dbo].[offer_for_type] ([offer_for_type_id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[ofr_typ] CHECK CONSTRAINT [FK_offer_for_type_ofr_typ]
GO
/****** Object:  ForeignKey [FK_st_provnc_cntry]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[st_provnc]  WITH NOCHECK ADD  CONSTRAINT [FK_st_provnc_cntry] FOREIGN KEY([cntry_cd])
REFERENCES [dbo].[cntry] ([cntry_cd])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[st_provnc] CHECK CONSTRAINT [FK_st_provnc_cntry]
GO
/****** Object:  ForeignKey [FK_sub_category]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[sub_category]  WITH NOCHECK ADD  CONSTRAINT [FK_sub_category] FOREIGN KEY([cat_id])
REFERENCES [dbo].[category] ([cat_id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[sub_category] CHECK CONSTRAINT [FK_sub_category]
GO
/****** Object:  ForeignKey [FK_sycs_coord_clb_clb]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[sycs_coord_clb]  WITH NOCHECK ADD  CONSTRAINT [FK_sycs_coord_clb_clb] FOREIGN KEY([clb_id])
REFERENCES [dbo].[clb] ([Clb_Id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[sycs_coord_clb] CHECK CONSTRAINT [FK_sycs_coord_clb_clb]
GO
/****** Object:  ForeignKey [FK_sycs_coord_clb_sycs_coord]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[sycs_coord_clb]  WITH NOCHECK ADD  CONSTRAINT [FK_sycs_coord_clb_sycs_coord] FOREIGN KEY([sycs_coord_id])
REFERENCES [dbo].[sycs_coord] ([sycs_coord_id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[sycs_coord_clb] CHECK CONSTRAINT [FK_sycs_coord_clb_sycs_coord]
GO
/****** Object:  ForeignKey [FK_sycs_coord_clb_sycs_coord_secur_grp]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[sycs_coord_clb]  WITH NOCHECK ADD  CONSTRAINT [FK_sycs_coord_clb_sycs_coord_secur_grp] FOREIGN KEY([sycs_coord_secur_grp_cd])
REFERENCES [dbo].[sycs_coord_secur_grp] ([sycs_coord_secur_grp_cd])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[sycs_coord_clb] CHECK CONSTRAINT [FK_sycs_coord_clb_sycs_coord_secur_grp]
GO
/****** Object:  ForeignKey [FK_sycs_coord_phn_nbr_sycs_coord]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[sycs_coord_phn_nbr]  WITH NOCHECK ADD  CONSTRAINT [FK_sycs_coord_phn_nbr_sycs_coord] FOREIGN KEY([sycs_coord_id])
REFERENCES [dbo].[sycs_coord] ([sycs_coord_id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[sycs_coord_phn_nbr] CHECK CONSTRAINT [FK_sycs_coord_phn_nbr_sycs_coord]
GO
/****** Object:  ForeignKey [FK_tdr_source_type_cd_merchant_address]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[tdr_source_type_cd]  WITH NOCHECK ADD  CONSTRAINT [FK_tdr_source_type_cd_merchant_address] FOREIGN KEY([address_id])
REFERENCES [dbo].[merchant_address] ([address_id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[tdr_source_type_cd] CHECK CONSTRAINT [FK_tdr_source_type_cd_merchant_address]
GO
/****** Object:  ForeignKey [FK_tdr_source_type_cd_source_type_cd]    Script Date: 02/06/2015 09:46:29 ******/
ALTER TABLE [dbo].[tdr_source_type_cd]  WITH NOCHECK ADD  CONSTRAINT [FK_tdr_source_type_cd_source_type_cd] FOREIGN KEY([source_type_cd])
REFERENCES [dbo].[source_type] ([source_type_cd])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[tdr_source_type_cd] CHECK CONSTRAINT [FK_tdr_source_type_cd_source_type_cd]
GO
