package com.ibibl.searchengine.viewmodel;


public class BibleSectionView extends ViewObject{

	public static final String TYPE = "BIBLE_SECTION_OBJECT";
	
	public static final String VERSION = "1.0";
	
	private final String sectionName;
	
	public BibleSectionView(String sectionName){
		super(TYPE, VERSION);
		this.sectionName = sectionName;
	}

	public String getSectionName() {
		return sectionName;
	}
}
