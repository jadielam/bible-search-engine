package com.ibibl.searchengine.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity for table Bible_verse
 * -idBible_verse (int(11), pk, nn)
 * -text(varchar(600), nn)
 * -version(fk, varchar(4), nn)
 * -bible_book(varchar(24), nn)
 * -bible_chapter(int, nn)
 * -bible_verse(int, nn)
 * 
 * @author jadiel
 *
 */
@Entity
@Table(name = "Bible_verse")
public class BibleVerseModel {

	private Integer id;
	
	private String book;
	
	private Integer chapter;
	
	private Integer verse;
	
	private Integer freq;
	
	public BibleVerseModel(){
		this.freq = 0;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idBible_verse")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "bible_book", nullable = false)
	@NotNull
	@Size(min = 1, max = 24)
	public String getBook() {
		return book;
	}

	public void setBook(String book) {
		this.book = book;
	}

	@Column(name = "bible_chapter", nullable = false)
	@NotNull
	@Min(1)
	@Max(150)
	public Integer getChapter() {
		return chapter;
	}

	public void setChapter(Integer chapter) {
		this.chapter = chapter;
	}

	@Column(name = "bible_verse", nullable = false)
	@NotNull
	@Min(1)
	@Max(176)
	public Integer getVerse() {
		return verse;
	}
	
	public void setVerse(Integer verse) {
		this.verse = verse;
	}
	
	
	@Column(name = "freq", nullable = false)
	@NotNull
	@Min(0)
	public Integer getFreq() {
		return freq;
	}

	public void setFreq(Integer freq) {
		this.freq = freq;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((book == null) ? 0 : book.hashCode());
		result = prime * result + ((chapter == null) ? 0 : chapter.hashCode());
		result = prime * result + ((verse == null) ? 0 : verse.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BibleVerseModel other = (BibleVerseModel) obj;
		if (book == null) {
			if (other.book != null)
				return false;
		} else if (!book.equals(other.book))
			return false;
		if (chapter == null) {
			if (other.chapter != null)
				return false;
		} else if (!chapter.equals(other.chapter))
			return false;
		if (verse == null) {
			if (other.verse != null)
				return false;
		} else if (!verse.equals(other.verse))
			return false;
		return true;
	}

	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(this.book);
		sb.append(" ");
		sb.append(this.chapter.toString());
		sb.append(":");
		sb.append(this.verse.toString());
		sb.append(" ");
		return sb.toString();
	}
	
}
