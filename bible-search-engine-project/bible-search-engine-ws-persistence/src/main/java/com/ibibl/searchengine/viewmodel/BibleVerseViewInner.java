package com.ibibl.searchengine.viewmodel;

import javax.xml.bind.annotation.XmlTransient;

import com.ibibl.bible.BibleVersion;
import com.ibibl.bible.Book;

public class BibleVerseViewInner extends BibleVerseView {

	public BibleVerseViewInner(BibleVerseView verse){
		super(verse);
		
	}
	public BibleVerseViewInner(Book book, BibleVersion version,
			Integer chapterNumber, Integer verseNumber, String text) {
		super(book, version, chapterNumber, verseNumber, text);
	
	}

	@Override
	@XmlTransient
	public Book getBook(){
		return super.getBook();
	}
	
	@Override
	@XmlTransient
	public BibleVersion getBibleVersion(){
		return super.getBibleVersion();
	}
	
	@Override
	@XmlTransient
	public Integer getChapterNumber(){
		return super.getChapterNumber();
	}
	
	@Override
	@XmlTransient
	public String getVersion(){
		return super.getVersion();
	}
	
	@Override
	@XmlTransient
	public String getType(){
		return super.getType();
	}
}
