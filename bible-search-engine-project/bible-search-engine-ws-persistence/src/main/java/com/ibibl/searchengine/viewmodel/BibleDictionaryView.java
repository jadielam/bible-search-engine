package com.ibibl.searchengine.viewmodel;

import org.apache.solr.client.solrj.beans.Field;

public class BibleDictionaryView extends ViewObject{
	
	public static final String TYPE = "DICTIONARY_OBJECT";
	
	public static final String VERSION = "1.0";
	
	@Field("dictionaryName")
	private String dictionaryName;
	
	@Field("entryTitle")
	private String entryTitle;
	
	@Field("entryText")
	private String entryText;
	
	public BibleDictionaryView(){
		super(TYPE, VERSION);
	}
	public BibleDictionaryView(String dictionaryName, 
			String entryTitle, String entryText){
		super(TYPE, VERSION);
		this.dictionaryName = dictionaryName;
		this.entryTitle = entryTitle;
		this.entryText = entryText;
	}

	public String getDictionaryName() {
		return dictionaryName;
	}

	public void setDictionaryName(String dictionaryName) {
		this.dictionaryName = dictionaryName;
	}

	public String getEntryTitle() {
		return entryTitle;
	}

	public void setEntryTitle(String entryTitle) {
		this.entryTitle = entryTitle;
	}

	public String getEntryText() {
		return entryText;
	}

	public void setEntryText(String entryText) {
		this.entryText = entryText;
	}


}
