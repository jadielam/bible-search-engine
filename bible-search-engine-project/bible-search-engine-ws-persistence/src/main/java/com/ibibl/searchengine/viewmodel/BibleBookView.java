package com.ibibl.searchengine.viewmodel;

import org.apache.commons.lang3.Validate;

import com.ibibl.bible.Book;

public class BibleBookView extends ViewObject {

	public static final String TYPE = "BIBLE_BOOK_OBJECT";
	
	public static final String VERSION = "1.0";
	
	private final String bookName;
	
	private final String bookAbbv;
	
	private final int order;
	
	public BibleBookView(String abbv){
		super(TYPE, VERSION);
		Book book = Book.fromAbbreviation(abbv);
		Validate.notNull(book);
		this.bookAbbv = book.getAbv();
		this.bookName = book.getName();
		this.order = book.getOrder();
	}

	public String getBookName() {
		return bookName;
	}

	public String getBookAbbv() {
		return bookAbbv;
	}

	public int getOrder() {
		return order;
	}
	
	
	
	
}
