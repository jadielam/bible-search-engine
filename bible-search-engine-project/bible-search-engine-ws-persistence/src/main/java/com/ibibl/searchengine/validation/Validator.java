// **************************************************
//
// Modification Tracking:
// $Revision:$
// $LastChangedBy:$
// $LastChangedDate:$
//
// **************************************************
package com.ibibl.searchengine.validation;

/**
 * This abstract class will be implemented by all the validators of
 * all the entities that we have defined in our application.
 * 
 * The implementing class only needs to implement the method validateAndTransform
 * 
 * This class will be used by the rest service. When receiving an entity
 * from the client, and before sending it to insertion in the database, 
 * it will first validate the entity using business rules, and it will 
 * transform it if it needs to do so.
 * 
 * @author jdearmas
 * @param <T> The type of the entity to be validated.
 *
 * @since 1.0
 */
public abstract class Validator<T> {

	/**
	 * 1. Validates that the T entity is in the correct format following
	 * rules defined by the business model of the application.  If it is not
	 * in the correct format and it cannot be fixed, it throws an 
	 * IllegalArgumentException
	 * 2. Transforms entity t into an equivalent entity that has the same
	 * meaning, but whose values have been normalizing using business rules.
	 * @param t The entity to be validated and transformed
	 * @return The entity that has been transformed
	 * @throws IllegalArgumentException if the entity t does not pass validation
	 */
	public final T validateAndTransform(T t) throws IllegalArgumentException{
		
		if (!validate(t)){
			throw new IllegalArgumentException("Entity did not pass validation");
		}
		return transform(t);
	};
	
	protected boolean validate(T t){
		return true;
	}
	
	protected T transform(T t){
		return t;
	}

}
