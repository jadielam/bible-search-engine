package com.ibibl.searchengine.viewmodel;

import java.util.List;

import com.ibibl.bible.BibleVersion;
import com.ibibl.bible.Book;
import org.apache.commons.lang3.Validate;

public class ChapterView {

	private final Book book;
	
	private final BibleVersion bibleVersion;
	
	private final List<BibleVerseViewInner> verses;
	
	private final Integer chapterNumber;

	/**
	 * 
	 * @param book
	 * @param version
	 * @param chapterNumber
	 * @param verses
	 * @throws IllegalStateException whenever the verses don't have the same chapter
	 * number, book or version as this chapter object.
	 */
	public ChapterView(Book book, BibleVersion version, Integer chapterNumber,
			List<BibleVerseViewInner> verses) throws IllegalStateException{
		super();
		Validate.notNull(book);
		Validate.notNull(version);
		Validate.notNull(chapterNumber);
		Validate.notNull(verses);
		
		this.book = book;
		this.bibleVersion = version;
		this.chapterNumber = chapterNumber;
		for (BibleVerseView verse : verses){
			Integer vchapter = verse.getChapterNumber();
			Book vbook = verse.getBook();
			BibleVersion vversion = verse.getBibleVersion();
			if (!vversion.equals(this.bibleVersion)){
				throw new IllegalStateException();
			}
			if (!vbook.equals(this.book)){
				throw new IllegalStateException();
			}
			
			if (!vchapter.equals(this.chapterNumber)){
				throw new IllegalStateException();
			}
		}
		
		this.verses = verses;

	}

	public Book getBook() {
		return book;
	}

	public BibleVersion getBibleVersion() {
		return this.bibleVersion;
	}

	public List<BibleVerseViewInner> getVerses() {
		return verses;
	}

	public Integer getChapterNumber() {
		return chapterNumber;
	}


	
	
}
