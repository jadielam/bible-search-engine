package com.ibibl.searchengine.viewmodel;

public abstract class BibleObject extends ViewObject{

	public static String TYPE = "BIBLE_OBJECT";
	
	public static String VERSION = "1.0";
	
	public BibleObject(String type, String version) {
		super(type, version);
	}
	
}
