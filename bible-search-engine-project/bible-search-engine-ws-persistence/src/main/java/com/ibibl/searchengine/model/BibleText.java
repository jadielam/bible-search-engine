package com.ibibl.searchengine.model;

import javax.persistence.Entity;
import javax.persistence.Table;

//TODO: FInish annotating this class.
//@Entity
//@Table(name = "Bible_text")
public class BibleText {

	private BibleVerseModel verse;
	
	private BibleVersionModel version;
	
	private String text;
	
	private Integer id;
	
	public BibleText(){
		
	}

	public BibleVerseModel getVerse() {
		return verse;
	}

	public void setVerse(BibleVerseModel verse) {
		this.verse = verse;
	}

	public BibleVersionModel getVersion() {
		return version;
	}

	public void setVersion(BibleVersionModel version) {
		this.version = version;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	
	
	
}
