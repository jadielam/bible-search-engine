package com.ibibl.searchengine.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity for table Bible_version
 * idBible_version(pk, nn, varchar(4)
 * description(varchar(45, nn)
 * 
 * @author jadiel
 *
 */
@Entity
@Table(name = "Bible_version")
public class BibleVersion {

	String id;
	
	String description;
	
	public BibleVersion(){
		
	}

	@Id
	@Column(name = "idBible_version", nullable = false)
	@NotNull
	@Size(min = 1, max = 4)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "description", nullable = false)
	@NotNull
	@Size(min = 1, max =45)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BibleVersion other = (BibleVersion) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
