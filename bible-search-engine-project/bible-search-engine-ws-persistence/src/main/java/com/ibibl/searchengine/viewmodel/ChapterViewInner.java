package com.ibibl.searchengine.viewmodel;

import java.util.List;

import javax.xml.bind.annotation.XmlTransient;

import com.ibibl.bible.BibleVersion;
import com.ibibl.bible.Book;

public class ChapterViewInner extends ChapterView{

	public ChapterViewInner(Book book, BibleVersion version,
			Integer chapterNumber, List<BibleVerseViewInner> verses)
			throws IllegalStateException {
		super(book, version, chapterNumber, verses);
	}

	@XmlTransient
	@Override
	public Book getBook(){
		return super.getBook();
	}
	
	@XmlTransient
	@Override
	public BibleVersion getBibleVersion(){
		return super.getBibleVersion();
	}
}
