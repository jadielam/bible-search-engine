package com.ibibl.searchengine.viewmodel;

import org.apache.solr.client.solrj.beans.Field;

public class BibleVerseSolrView extends BibleObject{

	@Field("book")
	private String bookName;
	
	@Field("chapter")
	private String chapterNumber;
	
	@Field("verse")
	private String verseNumber;
	
	private String text;
	
	private String bibleVersion;
	
	public static final String TYPE = "BIBLE_VERSE_SOLR_VIEW";
	
	public static final String VERSION = "1.0";
	
	public BibleVerseSolrView(){
		super(TYPE, VERSION);
	}


	public String getBookName() {
		return bookName;
	}


	public void setBookName(String bookName) {
		this.bookName = bookName;
	}


	public String getText() {	
		return text;
	}


	public String getChapterNumber() {
		return chapterNumber;
	}


	public void setChapterNumber(String chapterNumber) {
		this.chapterNumber = chapterNumber;
	}


	public String getVerseNumber() {
		return verseNumber;
	}


	public void setVerseNumber(String verseNumber) {
		this.verseNumber = verseNumber;
	}


	public void setText(String text) {
		this.text = text;
	}


	public String getBibleVersion() {
		return bibleVersion;
	}


	public void setBibleVersion(String bibleVersion) {
		this.bibleVersion = bibleVersion;
	}
	
}
