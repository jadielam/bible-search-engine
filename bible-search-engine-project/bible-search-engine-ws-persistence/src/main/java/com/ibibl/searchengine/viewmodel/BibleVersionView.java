package com.ibibl.searchengine.viewmodel;

import org.apache.commons.lang3.Validate;

import com.ibibl.bible.BibleVersion;

public class BibleVersionView extends ViewObject {

	public static final String TYPE = "BIBLE_VERSION_OBJECT";
	
	public static final String VERSION = "1.0";
	
	private final String versionName;
	
	private final String versionAbbv;
	
	private final String versionLanguage;
	
	private final Integer versionYear = null;
	
	public BibleVersionView(String versionAbbv){
		super(TYPE, VERSION);
		
		BibleVersion version = BibleVersion.fromAbbreviation(versionAbbv);
		Validate.notNull(version);
		this.versionAbbv = version.getAbv();
		this.versionName = version.getName();
		this.versionLanguage = version.getLanguage().name();
	}

	public String getVersionName() {
		return versionName;
	}

	public String getVersionAbbv() {
		return versionAbbv;
	}

	public String getVersionLanguage() {
		return versionLanguage;
	}

	public Integer getVersionYear() {
		return versionYear;
	}
	
	
	
}
