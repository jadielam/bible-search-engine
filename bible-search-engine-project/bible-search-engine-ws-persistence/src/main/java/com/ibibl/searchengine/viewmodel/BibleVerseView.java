package com.ibibl.searchengine.viewmodel;

import com.ibibl.bible.BibleReference;
import com.ibibl.bible.BibleVerse;
import com.ibibl.bible.BibleVersion;
import com.ibibl.bible.Book;
import com.ibibl.searchengine.model.BibleTextModel;
import com.ibibl.searchengine.model.BibleVerseModel;

import org.apache.commons.lang3.Validate;

public class BibleVerseView extends BibleObject{

	private final Book book;
	
	private String bookName;
	
	private final BibleVersion bibleVersion;
	
	
	private final Integer chapterNumber;
	
	private final Integer verseNumber;
	
	private final String text;
	
	public static final String TYPE = "BIBLE_VERSE_VIEW";
	
	public static final String VERSION = "1.0";
	
	public BibleVerseView(BibleVerse verse){
		this(verse.getReference(), verse.getText());
	}

	public BibleVerseView(BibleTextModel textModel){
		super(TYPE, VERSION);
		this.bibleVersion = BibleVersion.fromAbbreviation(textModel.getVersion().getId());
		BibleVerseModel model = textModel.getVerse();
		this.book = Book.fromAbbreviation(model.getBook());
		this.chapterNumber = model.getChapter();
		this.verseNumber = model.getVerse();
		this.text = textModel.getText();
	}
	
	public BibleVerseView(BibleVerseView verse){
		super(TYPE, VERSION);
		this.book = verse.getBook();
		this.bibleVersion = verse.getBibleVersion();
		this.chapterNumber = verse.getChapterNumber();
		this.verseNumber = verse.getVerseNumber();
		this.text = verse.getText();
	}
	
	public BibleVerseView(BibleReference ref, String text){
		super(TYPE, VERSION);
		Validate.notNull(ref);
		Validate.notNull(text);
		
		this.book = ref.getBook();
		this.bibleVersion = ref.getVersion();
		this.chapterNumber = new Integer(ref.getChapter());
		this.verseNumber = new Integer(ref.getVerse());
		this.text = text;
	}
	
	public BibleVerseView(Book book, BibleVersion version,
			Integer chapterNumber, Integer verseNumber, String text) {
		super(TYPE, VERSION);
		Validate.notNull(book);
		Validate.notNull(version);
		Validate.notNull(chapterNumber);
		Validate.notNull(verseNumber);
		Validate.notNull(text);
		this.book = book;
		this.bibleVersion = version;
		this.chapterNumber = chapterNumber;
		this.verseNumber = verseNumber;
		this.text = text;
	}

	public Book getBook() {
		return this.book;
	}
	
	public String getBookName(){
		return this.book.getName();
	}

	public BibleVersion getBibleVersion() {
		return bibleVersion;
	}

	public Integer getChapterNumber() {
		return chapterNumber;
	}

	public Integer getVerseNumber() {
		return verseNumber;
	}

	public String getText() {
		return text;
	}
}
