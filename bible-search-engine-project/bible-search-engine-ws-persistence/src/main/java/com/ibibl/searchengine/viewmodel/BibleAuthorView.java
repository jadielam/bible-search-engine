package com.ibibl.searchengine.viewmodel;

public class BibleAuthorView extends ViewObject {

	public static final String TYPE = "BIBLE_AUTHOR_OBJECT";
	
	public static final String VERSION = "1.0";
	
	private final String authorName;
	
	public BibleAuthorView(String authorName){
		super(TYPE, VERSION);
		this.authorName = authorName;
	}

	public String getAuthorName() {
		return authorName;
	}
	
}
