package com.ibibl.searchengine.model;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class BibleTextModelPk implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5537398424544015235L;

	private Integer bible_verse;
	
	private String bible_version;
	
	public BibleTextModelPk(){
		
	}

	public Integer getBible_verse() {
		return bible_verse;
	}

	public void setBible_verse(Integer bible_verse) {
		this.bible_verse = bible_verse;
	}

	public String getBible_version() {
		return bible_version;
	}

	public void setBible_version(String bible_version) {
		this.bible_version = bible_version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((bible_verse == null) ? 0 : bible_verse.hashCode());
		result = prime * result
				+ ((bible_version == null) ? 0 : bible_version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BibleTextModelPk other = (BibleTextModelPk) obj;
		if (bible_verse == null) {
			if (other.bible_verse != null)
				return false;
		} else if (!bible_verse.equals(other.bible_verse))
			return false;
		if (bible_version == null) {
			if (other.bible_version != null)
				return false;
		} else if (!bible_version.equals(other.bible_version))
			return false;
		return true;
	}
	
	
}
