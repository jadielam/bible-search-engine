package com.ibibl.searchengine.viewmodel;

import java.util.List;

public class ListView <T extends ViewObject> extends ViewObject{

	public static final String TYPE="LIST_VIEW";
	
	public static final String VERSION= "1.0";
	
	private final String itemsType;
	
	private final String itemsTypeVersion;
	
	private final List<T> items;
	
	public ListView(List<T> items, String itemsType, String itemsTypeVersion){
		super(TYPE, VERSION);
		
		this.itemsType = itemsType;
		this.items = items;
		this.itemsTypeVersion = itemsTypeVersion;
	}

	public String getItemsType() {
		return itemsType;
	}

	public String getItemsTypeVersion() {
		return itemsTypeVersion;
	}

	public List<T> getItems() {
		return items;
	}
	
	
}
