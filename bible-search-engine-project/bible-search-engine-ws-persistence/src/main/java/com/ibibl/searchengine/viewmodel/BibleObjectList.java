package com.ibibl.searchengine.viewmodel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BibleObjectList extends ViewObject {
	
	private static final String TYPE = "BIBLE_OBJECT_LIST";
	
	private static final String VERSION = "1.0";
	
	private final List<BibleObject> objects;
	
	public BibleObjectList(List<BibleObject> objects){
		super(TYPE, VERSION);
		this.objects = new ArrayList<BibleObject>(objects);
	}

	public List<BibleObject> getObjects() {
		return Collections.unmodifiableList(this.objects);
	}
}
