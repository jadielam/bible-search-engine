package com.ibibl.searchengine.viewmodel;


import java.util.ArrayList;
import java.util.List;

public class PaginationResultsView<T extends ViewObject> extends ViewObject{

	public static final String TYPE="PAGINATION_RESULTS_VIEW";
	
	public static final String VERSION="1.0";
	
	private final String itemsType;
	
	private final String itemsTypeVersion;
	
	private final int totalResults;
	
	private final int startIndex;
		
	private final List<T> items;
	
	
	public PaginationResultsView(int totalResults, int startIndex,
			List<T> items, String itemsType, String itemsTypeVersion){
		super(TYPE, VERSION);
		
		this.totalResults = totalResults;
		this.startIndex = startIndex;
		this.items = new ArrayList<T>(items);
		this.itemsType = itemsType;
		this.itemsTypeVersion = itemsTypeVersion;
	}

	public List<T> getItems() {
		return items;
	}

	public int getTotalResults() {
		return totalResults;
	}


	public int getStartIndex() {
		return startIndex;
	}

	public String getItemsType() {
		return itemsType;
	}

	public String getItemsTypeVersion() {
		return itemsTypeVersion;
	}
	
	
}
