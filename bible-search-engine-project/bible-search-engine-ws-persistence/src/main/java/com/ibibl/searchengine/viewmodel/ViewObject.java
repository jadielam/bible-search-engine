package com.ibibl.searchengine.viewmodel;

/**
 * Used to return all the objects to the view
 * @author jadiel
 *
 */
public abstract class ViewObject {

	private final String type;
	
	private final String version;
	
	public ViewObject(String type, String version){
		this.type = type;
		this.version = version;
	}

	public String getType() {
		return type;
	}

	public String getVersion() {
		return version;
	}
	
	
}
