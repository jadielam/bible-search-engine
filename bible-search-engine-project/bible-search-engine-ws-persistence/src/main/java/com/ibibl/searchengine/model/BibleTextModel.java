package com.ibibl.searchengine.model;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.AttributeOverrides;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name = "Bible_text")
public class BibleTextModel {

	private BibleTextModelPk id; 
	
	private BibleVerseModel verse;
	
	private BibleVersionModel version;
	
	private String text;
	
	public BibleTextModel(){
		this.id = new BibleTextModelPk();
	}
	
	@EmbeddedId
	@AttributeOverrides({
		@AttributeOverride(name = "bible_verse", column = @Column(name = "bible_verse")),
		@AttributeOverride(name = "bible_version", column = @Column(name = "bible_version"))
	})
	public BibleTextModelPk getId() {
		return id;
	}

	public void setId(BibleTextModelPk id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "bible_verse", referencedColumnName = "idBible_verse", insertable = false, updatable = false)
	@MapsId("bible_verse")
	public BibleVerseModel getVerse() {
		return verse;
	}

	public void setVerse(BibleVerseModel verse) {
		this.verse = verse;
	}

	@ManyToOne
	@JoinColumn(name = "bible_version", referencedColumnName = "idBible_version", insertable = false, updatable = false)
	@MapsId("bible_version")
	public BibleVersionModel getVersion() {
		return version;
	}

	public void setVersion(BibleVersionModel version) {
		this.version = version;
	}

	@Column(name = "text", nullable = false)
	@NotNull
	@Size(min = 1, max = 1200)
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BibleTextModel other = (BibleTextModel) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

		
	
	
	
}
