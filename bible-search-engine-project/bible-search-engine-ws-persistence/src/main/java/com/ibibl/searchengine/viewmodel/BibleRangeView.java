package com.ibibl.searchengine.viewmodel;

import java.util.ArrayList;
import java.util.List;

import com.ibibl.bible.BibleRange;
import com.ibibl.bible.BibleVerse;
import com.ibibl.bible.Book;
import com.ibibl.bible.BibleVersion;

import org.apache.commons.lang3.Validate;

public class BibleRangeView extends BibleObject{

	private final Book book;
	
	private final BibleVersion bibleVersion;
	
	private final List<ChapterViewInner> chapters;
	
	public static final String TYPE = "BIBLE_RANGE_VIEW";
	
	public static final String VERSION = "1.0";
	
	public BibleRangeView(BibleRange range) throws IllegalStateException {
		super(TYPE, VERSION);
		
		List<BibleVerse> bverses = range.getBibleVerses();
		List<BibleVerseView> verses = new ArrayList<BibleVerseView>();
		for (BibleVerse bverse : bverses){
			BibleVerseView verse = new BibleVerseView(bverse);
			verses.add(verse);
		}
		
		if (verses.size()>0){
			BibleVerseView firstVerse = verses.get(0);
			Book book = firstVerse.getBook();
			BibleVersion version = firstVerse.getBibleVersion();
			Integer lastChapter = firstVerse.getChapterNumber();
			List<BibleVerseViewInner> tempVerses = new ArrayList<BibleVerseViewInner>();
			List<ChapterViewInner> chapters = new ArrayList<ChapterViewInner>();
			
			//1. Create the chapters
			for (BibleVerseView verse : verses){
				Integer chapter = verse.getChapterNumber();
				if (!chapter.equals(lastChapter)){
					
					ChapterViewInner chapterView = new ChapterViewInner(book, version, 
							lastChapter, new ArrayList<BibleVerseViewInner>(tempVerses));
					chapters.add(chapterView);
					lastChapter = chapter;
					tempVerses.clear();
					tempVerses.add(new BibleVerseViewInner(verse));
				}
				else{
					tempVerses.add(new BibleVerseViewInner(verse));
				}
			}
			ChapterViewInner chapterView = new ChapterViewInner(book, version, 
					lastChapter, new ArrayList<BibleVerseViewInner>(tempVerses));
			chapters.add(chapterView);
						
			//2. Create the BibleRangeView
			Validate.notNull(book);
			Validate.notNull(version);
			Validate.notNull(chapters);
			
			this.book = book;
			this.bibleVersion = version;
			for (ChapterView chapter : chapters){
				Book vbook = chapter.getBook();
				BibleVersion vversion = chapter.getBibleVersion();
				if (!vversion.equals(this.bibleVersion)){
					throw new IllegalStateException();
				}
				if (!vbook.equals(this.book)){
					throw new IllegalStateException();
				}
			}
			this.chapters = chapters;
		}
		else{
			throw new IllegalStateException();
		}
	
	}
	
	
	public Book getBook() {
		return book;
	}

	public BibleVersion getBibleVersion() {
		return bibleVersion;
	}

	public List<ChapterViewInner> getChapters() {
		return chapters;
	}
	
	
}
