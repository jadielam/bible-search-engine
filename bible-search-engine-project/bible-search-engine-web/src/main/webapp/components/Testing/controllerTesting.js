angular.module("testingApp")
.controller("testingController", function ($scope, modClient){

	var clubname_clubid_dict = {};
	$scope.coordinators = [];
	$scope.clubs = [];
	$scope.club;
	$scope.coordinator;
	
	init();

	//Initialization
	function init() {
		
		//1. Get all the coordinators from database
		modClient.getAllCoordinators().
			then(function (data){
				$scope.coordinators = data;
			},
			function (error){
				console.log("Could not get the coordinators");
				console.log(error);
			});

		//2. Get all the clubs from database
		modClient.getAllClubs().
			then(function (data){
				$scope.clubs = data;
			},
			
			function(error) {
				console.log("Couldn't get the clubs");
				console.log(error);
			});

		//4. Getting club by id
		modClient.getClubById("111").
			then(function (data){
				$scope.coordinator = data;
			},
			function (error){
				console.log("Could not get the club");
				console.log(error);
			});

		//3. Getting coordinator by id.
		modClient.getCoordinatorById(632).
			then(function (data){
				$scope.coordinator = data;
			},
			function (error){
				console.log("Could not get the coordinator");
				console.log(error);
			});

			}

});