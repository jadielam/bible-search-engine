// This controller takes care of downloading the coordinators and the clubs
// from the rest service.
angular.module("securityApp")
.controller("controllerSecurity", function ($scope, $location, modClient){

	$scope.clubs = [];
	$scope.expandedClubs = [];  //Contain a new club called All
	$scope.coordinators = [];
	
	init();

	//Initialization of the entities
	function init() {
		
		$location.path("/search");

		//2. Get all the clubs from database
		modClient.getAllClubs().
			then(function (data){
				$scope.clubs = data;

				//This is a hack that will be needed by multiple views of this controller.
				angular.copy($scope.clubs, $scope.expandedClubs);
				$scope.expandedClubs.push({clubName : "All", clubId : ""})
			},

			function(error) {
				console.log("Couldn't get the clubs");
				console.log(error);
			});
	
	}

});