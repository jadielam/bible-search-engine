angular.module("securityApp")
.controller("controllerSecuritySearch", function ($scope, $location, $timeout, modClient, notifications){

	//This one contains all the clubs plus the All option.
	console.log("In controllerSecuritySearch");

	init();

	//Robert added this
	//To be used by the view:
	$scope.viewHeading = "Search";
	$scope.viewTitle = "Search";

	function init(){
		//Making a deep copy of the clubs array.
		
		//1. Get all the coordinators from database
		modClient.getAllCoordinators().
			then(function (data){
				$scope.$parent.coordinators = data;
			},
			function (error){
				
				console.log(error);
			});

	    //2. Display the notifications placed in the channel for you.
		$scope.notifications = notifications.popNotifications("security");
		$scope.showNotifications = true;
		$timeout(function () { $scope.showNotifications = false }, 10000);

        //3. Creating the variables for filtering.
		$scope.search = new Object();
		$scope.search.firstName = "";
		$scope.search.lastName = "";
		$scope.search.clubs = "";
		
	}
	

	//Deciding when to show the search and filtering results.
	//We show the results when there is one club selected,
	//or when any of the firstName or lastName fields have a 
	//length of greater than 0.
	$scope.showResults = function(){

		if ( 
			(angular.isDefined($scope.search.clubs) && $scope.search.clubs.length > 0) ||
			$scope.search.firstName.length > 0 ||
			$scope.search.lastName.length > 0) {
				return true;
			}	
			
		return false;
	}

	$scope.addCoordinator = function(){

		$location.path("/add/");
	}

	$scope.editCoordinator = function (coordinatorId) {

	    $location.path("/edit/" + coordinatorId);
	}

	$scope.deleteCoordinator = function (coordinatorId) {

	    $location.path("/delete/" + coordinatorId);
	}

    //Handling the delete logic here.
	$scope.showDelete = false;
	$scope.coordinatorToDeleteId = null;

	$scope.deleteCoordinator = function (coordinatorId, firstName, lastName) {
	    $scope.showDelete = true;
	    $scope.coordinatorToDeleteId = coordinatorId;
	    $scope.coordinatorToDeleteFirstName = firstName;
	    $scope.coordinatorToDeleteLastName = lastName;
	}

	$scope.delete = function () {

	    modClient.deleteCoordinator($scope.coordinatorToDeleteId).
		then(function (data) {
		    for (var i = 0; i < $scope.coordinators.length; ++i) {
		        coordinator = $scope.coordinators[i];
		        if (coordinator.id == $scope.coordinatorToDeleteId) {
		            $scope.coordinators.splice(i, 1);
		            i--;
		        }
		    }
		    $scope.showDelete = false;
		},

		function (error) {
		    $scope.showDelete = false;
		});
	}

	$scope.notdelete = function () {
	    $scope.showDelete = false;
	}

});