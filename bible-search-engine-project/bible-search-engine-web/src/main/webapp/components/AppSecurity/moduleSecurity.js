angular.module("securityApp", ["serviceModClient", "serviceValidation", "serviceNotifications", "ngRoute"])
	.config(function ($routeProvider, $locationProvider, $httpProvider){
		
	    var testing = false;
	//1. Setting the routes.
		// $locationProvider.html5Mode({
		// 	enabled : true,
		// 	requireBase : false
		// });

        //The routes for the documentation
	    if (testing) {
	        
	        $locationProvider.html5Mode({
                enabled : true,
	            requireBase: false
	        });

	        $routeProvider.when("/edit/:id", {
	         	templateUrl: "/webapp/components/AppSecurity/documentationSecurityAddModify.html",
	         	controller: "controllerSecurityEdit"
	        });

	        $routeProvider.when("/add", {
	        	templateUrl : "webapp/components/AppSecurity/documentationSecurityAddModify.html",
	         	controller: "controllerSecurityAdd"
	        });

	       //$routeProvider.when("/delete/:id", {
	       //  	templateUrl: "/webapp/components/AppSecurity/documentationSecurityDelete.html",
	       //  	controller: "controllerSecurityDelete"
	       // });

                $routeProvider.when("/search", {
	         	templateUrl: "webapp/components/AppSecurity/documentationSecuritySearch.html",
	         	controller: "controllerSecuritySearch"
	        });

                $routeProvider.otherwise({
                    templateUrl : "webapp/components/AppSecurity/documentationSecuritySearch.html",
                    controller: "controllerSecuritySearch"
                });

 }
        //The routes for the real application.
	    else {
	        $routeProvider.when("/edit/:id", {
	            templateUrl: "/webapp/components/AppSecurity/security-modify.html",
	            controller: "controllerSecurityEdit"
	        });
            $routeProvider.when("/add", {
			templateUrl: "security-modify.html",
			controller: "controllerSecurityAdd"
		});
          $routeProvider.when("/add", {
	            templateUrl: "security-modify.html",
	            controller: "controllerSecurityAdd"
	        });
    //	$routeProvider.when("/delete/:id", {
		//	templateUrl: "/webapp/components/AppSecurity/documentationSecurityDelete.html",
			//controller: "controllerSecurityDelete"
		//});

	        $routeProvider.when("/delete/:id", {
	            templateUrl: "/documentationSecurityDelete.html",
	            controller: "controllerSecurityDelete"
	        });

	        $routeProvider.when("/search", {
	            templateUrl: "security-search.html",
	            controller: "controllerSecuritySearch"
	        });

	        $routeProvider.otherwise({
	            templateUrl: "webapp/components/AppSecurity/security-search.html",
	            controller: "controllerSecuritySearch"
	        })
	    }


		
	    //disable IE ajax request caching
	    //$httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
	    // extra
	    //$httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
	    //$httpProvider.defaults.headers.get['Pragma'] = 'no-cache';

	    //2. Creating an interceptor that logs all the requests and responses to and from
		//the rest service
		$httpProvider.interceptors.push(function(){
			
			return {
				request: function (config){
					url = config.url;
					method = config.method;
					data = config.data;

					if (config.method == 'GET') {
					    var separator = config.url.indexOf('?') === -1 ? '?' : '&';
					    config.url = config.url + separator + 'noCache=' + new Date().getTime();
					}

					console.log(method + " request with url "+url + " was sent");
					
					return config;
				},

				response: function (response){

					status = response.status;

					config = response.config;
					url = config.url;
					method = config.method;

					console.log("Response with status "+status+ " received for request with method "+method+ " and url "+url);

					if (response.status>299) return $q.reject(response);
					return response;
				}


			}
		});

	});
