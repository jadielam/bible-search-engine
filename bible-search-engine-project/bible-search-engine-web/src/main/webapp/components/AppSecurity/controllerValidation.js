angular.module("securityApp")
.controller("controllerValidation", function ($scope){

	$scope.areaNumberPattern = new RegExp("[2-9][0-9][0-9]");
	$scope.exchangeNumberPattern = new RegExp("[0-9][0-9][0-9]");
	$scope.suffixNumberPattern = new RegExp("[0-9][0-9][0-9][0-9]");
    
    //Returns true if no element is checked
    //otherwise it returns false.
	$scope.checkboxRequired = function (items) {
	    for (var i = 0; i < items.length; i++){
	        item = items[i];
	        if (item.checked) return false;
	    }
	    return true;
	}
});
