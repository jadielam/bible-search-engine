angular.module("securityApp")
.controller("controllerSecurityDelete", function ($scope, $routeParams, $location, modClient){
	
    //Experimenting with branching.
	console.log("In controllerSecurityDelete");

	if ($location.path().indexOf("/delete/") == 0){
		$scope.id = $routeParams["id"];
	}
	else {
		console.log("The controller controllerSecurityDelete was active even when the path was not /delete");
		$location.path("/search");
	}
		
	$scope.delete = function (){
		modClient.deleteCoordinator($scope.id).
		then(function (data){
			$location.path("/search");
		},

		function (error){
			$location.path("/search");
		});
				
	};

	$scope.notdelete = function(){
		$location.path("/search");
	};
	
	
});