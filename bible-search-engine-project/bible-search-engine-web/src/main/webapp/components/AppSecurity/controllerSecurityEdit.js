angular.module("securityApp")
.controller("controllerSecurityEdit", function ($scope, $location, $routeParams, modClient, notifications){

	console.log("In controllerSecurityEdit");
	
	//Objects to get from database
	$scope.securityGroups = [];
	$scope.phoneTypes = [];
	$scope.oldCoordinatorClubs = [];
	$scope.coordinatorClubs = [];
	$scope.notifications = [];
	$scope.oldNotifications = [];
	
	//Objects to be created by the controller.
	$scope.newCoordinator = new Object();
	$scope.submitButtonText = "Save";
	
	//To be used by the view:
	$scope.viewHeading = "Edit";
	$scope.viewTitle = "Edit";

	$scope.checkboxRequired = function (items) {
	    for (var i = 0; i < items.length; i++) {
	        item = items[i];
	        if (item.checked) return false;
	    }
	    return true;
	}

	init();

	function init() {
		

		//0. Get all the fields of the coordinator from the parent scope.
		if ($location.path().indexOf("/edit/") == 0){
			$scope.id = $routeParams["id"];
		}
		else {
			console.log("The controller controllerSecurityEdit was active even when the path was not /edit");
			$location.path("/search");
		}
		for (var i = 0; i < $scope.coordinators.length; ++i){
			coordinator = $scope.coordinators[i];
			if (coordinator.id == $scope.id){
				$scope.newCoordinator = coordinator;
				
			}
		}

		angular.copy($scope.clubs, $scope.coordinatorClubs);

		//1. Get all the security groups from the database
		modClient.getAllSecurityGroups().
			then(function (data){
				$scope.securityGroups = data;
			}, 

			function(error) {
				
				console.log(error);
			});

		//2. Get all the phone types from the database.
		modClient.getAllPhoneTypes().
		    then(function(data){
		    	$scope.phoneTypes = data;
		    	
		    },

		    function(error) {
		    	
		    });

	    //4. Get all the notifications from the database.
		modClient.getAllNotifications().
            then(function (data) {
                $scope.notifications = data;

                //4. Get all the relationships to notifications from the database.
                modClient.getNotificationsFromCoordinator($scope.id).
                    then(function(data){
                        for (var i = 0 ; i < data.length ; ++i) {
                            association = data[i];

                            for (var j = 0; j < $scope.notifications.length; ++j) {
                                notification = $scope.notifications[j];

                                if (association.id.notifyId == notification.notifyId) {
                                    notification.checked = true;
                                }
                            }
                        }

                        angular.copy($scope.notifications, $scope.oldNotifications);
		        
                    },
            
                    function(error){

                    }
                );
            },

            function (error) {

            });
        

		//5. Get all the relationships to clubs from the database.
		//This one will be interesting, because I will have to update the scope in real time.
		modClient.getAllCoordinatedClubs($scope.id).
			then(function(data){	
				for (var i = 0 ; i < data.length; i++){
					association = data[i];
					for (var j = 0; j < $scope.coordinatorClubs.length ; ++j){
						club = $scope.coordinatorClubs[j];

						if (association.id.clb_id == club.clubId){
							club.checked = true;
							club.role = association.securityGroup.securityGroupCode;
						}
					}
				}

				angular.copy($scope.coordinatorClubs, $scope.oldCoordinatorClubs);

			},

			function(error){
				
			});
		

		
		
	};


	/**
	This function modifies a coordinator already present in the database.
	Note: The name of this function is misleading. I will change it later
	to be submit form
	*/
	$scope.submitForm = function() {
		//1. Add the coordinator to the database
		if (angular.isDefined($scope.newCoordinator) && angular.isDefined($scope.oldCoordinatorClubs)  && angular.isDefined($scope.coordinatorClubs) 
			&& angular.isArray($scope.oldCoordinatorClubs) && angular.isArray($scope.coordinatorClubs)){
			
		    var operationsCounter = 0;
		    var numberOfOperations = 0;

			modClient.updateCoordinator($scope.newCoordinator)
			.then(function ( updatedCoordinator){
				
			    coordinatorId = updatedCoordinator.id;
			    //1.1 updating notifications
			    for (var i = 0; i < $scope.notifications.length; ++i) {
			        notification = $scope.notifications[i];
			        oldNotification = $scope.oldNotifications[i];

                    //Remove the association
			        if (oldNotification.checked && !notification.checked) {

			            numberOfOperations++;

			            modClient.removeNotificationFromCoordinator(notification.notifyId, coordinatorId)
                        .then(function(data){
                            angular.copy($scope.notifications, $scope.oldNotifications);
                            operationsCounter++;

                            if (operationsCounter == numberOfOperations) {
                                notification = new Object();
                                notification.message = "Coordinator " + updatedCoordinator.firstName + " " + updatedCoordinator.lastName + " successfully edited";
                                notification.duration = 10000;
                                notification.type = "success";
                                notifications.addNotification(notification, "security");
                                $scope.close();
                            }

                        },

                        function (errorResponse){
                            $scope.showAddError = true;
                            $scope.addErrorMessage = "Something went wrong. Please try again later";
                            $timeout(function () { $scope.showAddError = false; }, 10000);
                        });
			        }

                    //Add the association
			        else if (!oldNotification.checked && notification.checked) {
			            numberOfOperations++;

			            modClient.addNotificationToCoordinator(notification.notifyId, coordinatorId)
                        .then(function (data) {
                            angular.copy($scope.notifications, $scope.oldNotifications);
                            operationsCounter++;

                                if (operationsCounter == numberOfOperations) {
                                    notification = new Object();
                                    notification.message = "Coordinator " + updatedCoordinator.firstName + " " + updatedCoordinator.lastName + " successfully edited";
                                    notification.duration = 10000;
                                    notification.type = "success";
                                    notifications.addNotification(notification, "security");
                                    $scope.close();
                                }

                            },
                        
                            function (errorResponse) {
                                $scope.showAddError = true;
                                $scope.addErrorMessage = "Something went wrong. Please try again later";
                                $timeout(function () { $scope.showAddError = false; }, 10000);
                        });
			        }
			    }
               
                //1.2 updating club roles
				for (var i = 0 ; i < $scope.coordinatorClubs.length; ++i){

					club = $scope.coordinatorClubs[i];
					oldClub = $scope.oldCoordinatorClubs[i];

					//Remove the association
					if (oldClub.checked && !club.checked){

					    numberOfOperations++;

					    modClient.removeCoordinatorFromClub(club.clubId, coordinatorId)
						.then (function(data){
						    angular.copy($scope.coordinatorClubs, $scope.oldCoordinatorClubs);
						    operationsCounter++;
						    
						    if (operationsCounter == numberOfOperations) {
						        notification = new Object();
						        notification.message = "Coordinator " + updatedCoordinator.firstName + " " + updatedCoordinator.lastName + " successfully edited";
						        notification.duration = 10000;
						        notification.type = "success";
						        notifications.addNotification(notification, "security");
						        $scope.close();
						    }
						},

						function (errorResponse){
						    if (errorResponse.status == 400) {
						        console.log("The request had some incorrect values.");
						        $scope.showAddError = true;
						        $scope.addErrorMessage = "The form had some incorrect values";
						        $timeout(function () { $scope.showAddError = false; }, 10000);
						    }
						    else {
						        console.log("The coordinator already exists in the database.");
						        $scope.addErrorMessage = "User not edited. Communication error with the database.";
						        $timeout(function () { $scope.showAddError = false; }, 10000);
						    }
						});
					}
					//Add the association
					else if (!oldClub.checked && club.checked) {

					    numberOfOperations++;

						modClient.addCoordinatorToClub(club.clubId, coordinatorId, club.role)
						.then (function(data){

						    angular.copy($scope.coordinatorClubs, $scope.oldCoordinatorClubs);
						    operationsCounter++;
						    if (operationsCounter == numberOfOperations) {
						        notification = new Object();
						        notification.message = "Coordinator " + updatedCoordinator.firstName + " " + updatedCoordinator.lastName + " successfully edited";
						        notification.duration = 10000;
						        notification.type = "success";
						        notifications.addNotification(notification, "security");
						        $scope.close();
						    }
				
						},

						function (errorResponse){
						    if (errorResponse.status == 400) {
						        console.log("The request had some incorrect values.");
						        $scope.showAddError = true;
						        $scope.addErrorMessage = "The form had some incorrect values";
						        $timeout(function () { $scope.showAddError = false; }, 10000);
						    }
						    else {
						        console.log("The coordinator already exists in the database.");
						        $scope.addErrorMessage = "User not edited. Communication error with the database.";
						        $timeout(function () { $scope.showAddError = false; }, 10000);
						    }
						});
					}
					//Modify the association
					else if (oldClub.checked && club.checked && oldClub.role != club.role){

					    numberOfOperations++;

					    modClient.addCoordinatorToClub(club.clubId, coordinatorId, club.role)
						.then (function(data){

						    angular.copy($scope.coordinatorClubs, $scope.oldCoordinatorClubs);
						    operationsCounter++;
						    if (operationsCounter == numberOfOperations) {
						        notification = new Object();
						        notification.message = "Coordinator " + updatedCoordinator.firstName + " " + updatedCoordinator.lastName + " successfully edited";
						        notification.duration = 10000;
						        notification.type = "success";
						        notifications.addNotification(notification, "security");
						        $scope.close();
						    }
						},

						function (errorResponse){
						    if (errorResponse.status == 400) {
						        console.log("The request had some incorrect values.");
						        $scope.showAddError = true;
						        $scope.addErrorMessage = "The form had some incorrect values";
						        $timeout(function () { $scope.showAddError = false; }, 10000);
						    }
						    else {
						        console.log("The coordinator already exists in the database.");
						        $scope.addErrorMessage = "User not edited. Communication error with the database.";
						        $timeout(function () { $scope.showAddError = false; }, 10000);
						    }
						});
					}
					
				}

				if (operationsCounter == numberOfOperations) {
				    notification = new Object();
				    notification.message = "Coordinator " + updatedCoordinator.firstName + " " + updatedCoordinator.lastName + " successfully edited";
				    notification.duration = 10000;
				    notification.type = "success";
				    notifications.addNotification(notification, "security");
				    $scope.close();
				}
				
			
			},

			//The coordinator could not be addded
			function (errorResponse){
				if (errorResponse.status == 400){
				    console.log("The request had some incorrect values.");
				    $scope.showAddError = true;
				    $scope.addErrorMessage = "The form had some incorrect values";
				    $timeout(function () { $scope.showAddError = false; }, 10000);
				}
				else {
				    console.log("The coordinator already exists in the database.");
				    $scope.addErrorMessage = "User not edited. Communication error with the database.";
				    $timeout(function () { $scope.showAddError = false; }, 10000);
				}
			})	
		}
		
	};

	$scope.close = function(){

		$location.path("/search");
	}

	
});
