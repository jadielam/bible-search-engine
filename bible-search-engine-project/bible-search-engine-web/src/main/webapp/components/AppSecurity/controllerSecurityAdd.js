angular.module("securityApp")
.controller("controllerSecurityAdd", function ($scope, $q, $location, $timeout, modClient, notifications){

	console.log("In controllerSecurityAdd");
	//Objects to get from database
	$scope.securityGroups = [];
	$scope.phoneTypes = [];
	$scope.coordinatorClubs = [];
	$scope.notifications = [];

	//Objects to be created by the controller.
	$scope.newCoordinator = new Object();
	$scope.submitButtonText = "Add";
	
	//To be used by the view
	$scope.viewHeading = "Add";
	$scope.viewTitle = "Add";
	
    //Initialize the controller with proper data.
	init();

	$scope.checkboxRequired = function (items) {
	    for (var i = 0; i < items.length; i++) {
	        item = items[i];
	        if (item.checked) return false;
	    }
	    return true;
	}

	var timeoutSetFalse = function (condition) {
	    condition = false;
	};

	function init() {
	
		angular.copy($scope.clubs, $scope.coordinatorClubs);

		//2. Get all the security groups from the database
		modClient.getAllSecurityGroups().
			then(function (data){
				$scope.securityGroups = data;
			}, 

			function(error) {
				
			});

		//3. Get all the phone types from the database.
		modClient.getAllPhoneTypes().
		    then(function(data){
		    	$scope.phoneTypes = data;
		    	
		    },

		    function(error) {
		    	
		    });

	    //4. Get all the notifications from the database.
		modClient.getAllNotifications().
            then(function (data) {
                $scope.notifications = data;
            },

            function (error) {

            });

		//Objects that the user selects or adds in the form
		
	};


	/**
	Adds a new coordinator-club association to the database
	*/
	$scope.submitForm = function () {

	    //1. Add the coordinator to the database
	    if (angular.isDefined($scope.newCoordinator) && angular.isDefined($scope.coordinatorClubs)
            && angular.isArray($scope.coordinatorClubs)) {

	        modClient.addCoordinator($scope.newCoordinator)
            .then(function (addedCoordinator) {

                coordinatorId = addedCoordinator.id;
                var roleAdditionsCounter = 0;
                var numberOfRoleAdditions = 0;

                if (angular.isDefined(coordinatorId)) {

                    //Adding the coordinator-notification relationship to the database
                    for (var i = 0 ; i < $scope.notifications.length; ++i) {
                        notification = $scope.notifications[i];

                        if (notification.checked) {

                            numberOfRoleAdditions++;
                            modClient.addNotificationToCoordinator(notification.notifyId, coordinatorId)
                            .then(function (data) {

                                roleAdditionsCounter++;

                                if (roleAdditionsCounter == numberOfRoleAdditions) {
                                    notification = new Object();
                                    notification.message = "Coordinator " + addedCoordinator.firstName + " " + addedCoordinator.lastName + " successfully added";
                                    notification.duration = 10000;
                                    notification.type = "success";
                                    notifications.addNotification(notification, "security");
                                    $scope.close();
                                }
                            },

                            //The association could not be added
                            function (errorResponse) {

                                if (errorResponse.status == 400) {
                                    console.log("The request had some incorrect values.");
                                    $scope.showAddError = true;
                                    $scope.addErrorMessage = "The notification association had some incorrect values.";
                                    $timeout(function () { $scope.showAddError = false; }, 10000);

                                }
                                else {
                                    $scope.showAddError = true;
                                    $scope.addErrorMessage = "Notification association not added. Communication error with the database.";
                                    $timeout(function () { $scope.showAddError = false; }, 10000);
                                }

                            });

                        }
                    }

                    //Adding the coordinator-club relationship to the database.
                    for (var i = 0 ; i < $scope.coordinatorClubs.length; ++i) {
                        club = $scope.coordinatorClubs[i];

                        if (club.checked) {

                            numberOfRoleAdditions++;

                            //3. Add a new coordinator club association to the database.
                            modClient.addCoordinatorToClub(club.clubId, coordinatorId, club.role)
                            .then(function (data) {
                                roleAdditionsCounter++;

                                if (roleAdditionsCounter == numberOfRoleAdditions) {
                                    notification = new Object();
                                    notification.message = "Coordinator " + addedCoordinator.firstName + " " + addedCoordinator.lastName + " successfully added";
                                    notification.duration = 10000;
                                    notification.type = "success";
                                    notifications.addNotification(notification, "security");
                                    $scope.close();
                                }


                            },

                            //The association could not be added
                            function (errorResponse) {

                                if (errorResponse.status == 400) {
                                    console.log("The request had some incorrect values.");
                                    $scope.showAddError = true;
                                    $scope.addErrorMessage = "The form had some incorrect values";
                                    $timeout(function () { $scope.showAddError = false; }, 10000);

                                }
                                else if (errorResponse.status == 409) {
                                    console.log("The coordinator already exists in the database.");
                                    $scope.showAddError = true;
                                    $scope.addErrorMessage = "The coordinator already exists in the database";
                                    $timeout(function () { $scope.showAddError = false; }, 10000);
                                }
                                else {
                                    $scope.showAddError = true;
                                    $scope.addErrorMessage = "User not added. Communication error with the database.";
                                    $timeout(function () { $scope.showAddError = false; }, 10000);
                                }

                            });

                        }
                    }

                    //Code needed here because it could be that no club is checked and no notification is checked
                    if (roleAdditionsCounter == numberOfRoleAdditions) {
                        notification = new Object();
                        notification.message = "Coordinator " + addedCoordinator.firstName + " " + addedCoordinator.lastName + " successfully added";
                        notification.duration = 10000;
                        notification.type = "success";
                        notifications.addNotification(notification, "security");
                        $scope.close();
                    }


                }

            },

            //The coordinator could not be addded
            function (errorResponse) {
                if (errorResponse.status == 400) {
                    console.log("The request had some incorrect values.");
                    $scope.showAddError = true;
                    $scope.addErrorMessage = "The form had some incorrect values";
                    $timeout(function () { $scope.showAddError = false; }, 10000);

                }
                else if (errorResponse.status == 409) {
                    console.log("The coordinator already exists in the database.");
                    $scope.showAddError = true;
                    $scope.addErrorMessage = "The coordinator already exists in the database";
                    $timeout(function () { $scope.showAddError = false; }, 10000);
                }
                else {
                    $scope.showAddError = true;
                    $scope.addErrorMessage = "User not added. Communication error with the database.";
                    $timeout(function () { $scope.showAddError = false; }, 10000);

                }
            })

	    }

	};

	$scope.close = function(){
		
		$location.path("/search");
	}
	

});
