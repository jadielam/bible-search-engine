//This controller takes care of submitting queries to server.
angular.module("searchApp")
.controller("controllerSearch", function ($scope, $interval, searchClient){

	var co = this;

	$scope.query=""
	$scope.results = []
	
	this.requestCounter = 0;
	this.lastResponse = -1;
	
	var lastQuery = "";
	var detectQueryChange = function(){
		
		if (lastQuery !== $scope.query){
			lastQuery = $scope.query;

			var requestNo = co.requestCounter;			
			co.requestCounter = co.requestCounter + 1;
	
			searchClient.getQueryResults($scope.query).
				then(function(data){
					if (requestNo > co.lastResponse){
						co.lastResponse = requestNo;
						$scope.results = data;	
					}
						
				},

				function(errorResponse){
					console.log('Error connecting to the server')
				});

		}
	}
	$interval(detectQueryChange, 400);

	

	// $scope.$watch("query", 
		// function(newValue, oldValue){

	
			// if (oldValue !== newValue){
	
				// var requestNo = co.requestCounter;			
				// requestCounter = requestCounter + 1;
	
				// searchClient.getQueryResults(newValue).
					// then(function(data){
						// if (requestNo > co.lastResponse){
							// co.lastResponse = requestNo;
							// $scope.results = data;	
						// }
						
					// },

					// function(errorResponse){
		// 				console.log('Error connecting to the server')
		// 			});
		// 	}
		// });
	

});