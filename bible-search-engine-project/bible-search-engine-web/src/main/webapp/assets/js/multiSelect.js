

$(document).ready(function() {
	$well = $('.well');
	$selectAll = $('multiselect-all');
    $('#clubselect').multiselect({
        includeSelectAllOption: true,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        maxHeight: 200,
        buttonWidth: '100%',
        selectAllText: 'All Clubs',
        allSelectedText: 'All Clubs Selected',
         onChange: function(option, checked, select) {
               $well.append($(option).val() + '<br />');
              // alert('Changed option ' + $(option).val() + '.');
            }
    });
  
});