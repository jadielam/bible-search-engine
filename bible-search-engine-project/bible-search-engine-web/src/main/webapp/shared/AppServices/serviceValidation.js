﻿angular.module("serviceValidation", []).factory("validation", function () {
    
    return {
        
        //Returns true if at least a checkbox is checked.
        //Otherwise it returns false.
        checkboxChecked : function (items) {
            for (var i = 0; i < items.length; i++){
                item = items[i];
                if (item.checked) return true;
            }
            return false;
        },

        //Returns a list with all the validation errors for that 
        //form name
        getErrors : function(formName) {

            var errors = [];

            if (angular.isDefined(formName.firstName)) {
                if (formName.firstName.$error.required) {
                    errors.push("Please, enter a valid first name.");
                }
            }

            if (angular.isDefined(formName.lastName)) {
                if (formName.firstName.$error.required) {
                    errors.push("Please, enter a valid last name.");
                }
            }

            if (angular.isDefined(formName.emailAddress)) {
                if (formName.emailAddress.$error.required || formName.emailAddress.$error.email) {
                    errors.push("Please, enter a valid email address");
                }
            }

            if (angular.isDefined(formName.areaNumber) && angular.isDefined(formName.exchangeNumber)
                && angular.isDefined(formName.suffixNumber)) {
                if (formName.areaNumber.$error.pattern || formName.exchangeNumber.$error.pattern
                    || formName.suffixNumber.$error.pattern) {
                    errors.push("Please, enter a valid phone number");
                }
            }

            if (angular.isDefined(formName.phoneType)) {
                if (formName.phoneType.$error.required) {
                    errors.push("A phone type is required");
                }
            }

            return errors;
        }
    }
    


});
