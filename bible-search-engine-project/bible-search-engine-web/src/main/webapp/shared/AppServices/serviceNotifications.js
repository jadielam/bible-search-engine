﻿angular.module("serviceNotifications", []).factory("notifications", function () {
    
    var chanels = {};
    
    //A notification should contain the following fields:
    //1. message: The message to be displayed
    //2. duration: For how long it should be displayed.
    //             If the value is null, it will last forever
    //             or until the user or program closes it.
    //3. closable: Determines if the user will have the abbility
    //             to close it or not.

    return {

        //Adds a new chanel that will store notifications
        addChanel: function (chanelName) {
            chanels[channelName] = [];
        },

        //Removes a chanel
        removeChanel: function (chanelName) {
            delete chanels[chanelName];
        },

        //Adds notification to chanel.  If chanel does not
        //exist, it creates a new one before adding the notification
        addNotification: function (notification, chanelName) {
            if (!angular.isDefined(chanels[chanelName])){
                chanels[chanelName]=[];
            }
            chanels[chanelName].push(notification);
        },

        //Returns and removes all the notifications from 
        //a chanel and removes the channel
        popNotifications: function (chanelName){
            toReturn = [];
            if (angular.isDefined(chanels[chanelName])){
                angular.copy(chanels[chanelName], toReturn);
                delete chanels[chanelName];
            }
            return toReturn;
        }
   }
    
});
