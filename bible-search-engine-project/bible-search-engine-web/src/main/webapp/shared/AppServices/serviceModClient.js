angular.module("serviceModClient", []).factory("modClient", function($q, $http){
	
	console.log("modClient service created");
	var host = "http://localhost:8080/";
	var service_address = "member-offer-database-ws-api/mod/";

	var put_content = function (link, type){
		
		return $http.put(link)
		.then(function (response){
			return response.data;
		}, function (response){
			return $q.reject(response);
		});
	};

	var put_content = function (link, type, payload){
		
		return $http.put(link, payload)
		.then(function (response){
			return response.data;
		}, function (response){
			return $q.reject(response);
		}); 
	};

	var post_content = function (link, type, payload){

		return $http.post(link, payload)
		.then(function (response){
			return response.data;
		}, function (response){
			return $q.reject(response);
		}); 
	};

	var get_content = function (link, type, rel){
		
		return $http.get(link)
		.then(function (response){
			return response.data;
		}, function (response){
			return $q.reject(response);
		});
	};

	var delete_content = function (link, type) {
		 
		return $http.delete(link)
		 .then (function (response){
		 	return response;
		 },
		 function (response){
		 	return $q.reject(response);
		 });

	}

	return {

		/**
		Gets a club entry by its id.
		*/
		getClubById: function(id) {
			var link = host + service_address + "club/" + String(id);
			var type = "application/json";
			var rel = "get";

			return get_content(link, type, rel);
		},

		/**
		Gets a coordinator object by its id.
		*/
		getCoordinatorById: function(id) {
			var link = host + service_address + "coordinator/" + String(id);
			var type = "application/json";
			var rel = "get";

			return get_content(link, type, rel);
		},

		/**
		Returns all the clubs in the system.
		It returns an array of club elements
		*/
		getAllClubs: function() {

			
				var link = host + service_address + "club/";
				var type = "application/json";
				var rel = "get";

				return get_content(link, type, rel);
					
			
		},

		/**
		Returns all the coordinators in the system.
		It returns an array of coordinator elements
		*/
		getAllCoordinators: function() {

				var link = host + service_address + "coordinator/";
				var type = "application/json";
				var rel = "get";

				return get_content(link, type, rel);	
		},

		getAllSecurityGroups: function() {
		
				var link = host + service_address + "securitygroup/";
				var type = "application/json";
				var rel = "get";

				return get_content(link, type, rel);
		
		},

		getAllPhoneTypes: function(){

			var link = host + service_address + "phonetype/";
			var type = "application/json";
			var rel = "get";

			return get_content(link, type, rel);
		},

		addCoordinator: function(coordinator) {

			var link = host + service_address + "coordinator/";
			var type = "application/json";
			var rel = "post";

			return post_content(link, type, coordinator);
		},

		updateCoordinator: function(coordinator) {

			var link = host + service_address + "coordinator/";
			var type = "application/json";
			var rel = "put";

			return put_content(link, type, coordinator);
		},

		deleteCoordinator: function(id) {
			var link = host + service_address + "coordinator/" + id;
			var type = "application/json";
			var rel = "delete";

			return delete_content(link, type);
		},

		addCoordinatorToClub: function(clubId, coordinatorId, securityGroupId) {

			var link = host + service_address + "coordinator/" + coordinatorId +"/club/" + clubId +"/securitygroup/" + securityGroupId;
			var type = "application/json";
			var rel = "put";

			return put_content(link, type);
		},

		removeCoordinatorFromClub: function (clubId, coordinatorId){
			var link = host + service_address + "coordinator/" + coordinatorId + "/club/" + clubId;
			var type = "application/json";
			var rel = "delete";

			return delete_content(link, type);
		},

		getAllCoordinatedClubs: function(coordinatorId) {
			var link = host + service_address + "coordinator/" + coordinatorId + "/club";
			var type = "application/json";
			var rel = "get";

			return get_content(link, type, rel);
		},

		getAllNotifications: function () {

		    var link = host + service_address + "notify/";
		    var type = "application/json";
		    var rel = "get";

		    return get_content(link, type, rel);
		},

		addNotificationToCoordinator: function (notificationId, coordinatorId) {
		    var link = host + service_address + "coordinator/" + coordinatorId + "/notify/" + notificationId;
		    var type = "application/json";
		    var rel = "put";

		    return put_content(link, type);

		},

		removeNotificationFromCoordinator: function (notificationId, coordinatorId) {
		    var link = host + service_address + "coordinator/" + coordinatorId + "/notify/" + notificationId;
		    var type = "application/json";
		    var rel = "delete";

		    return delete_content(link, type);
		},

		getNotificationsFromCoordinator: function (coordinatorId) {
		    var link = host + service_address + "coordinator/" + coordinatorId + "/notify/";
		    var type = "application/json";
		    var rel = "get";

		    return get_content(link, type, rel);
		}

	}
});