﻿angular.module("serviceTrie", []).factory("trie", function () {

    var rootNode = {word : null, children : {}};
    
    return {
        insert : function(word){
            var node = rootNode;
            length = word.length
            for (var i = 0; i < length; ++i) {
                letter = word.charAt(i);

                if (undefined === node['children'][letter]) {
                    node['children'][letter]={word : null, children : {}};
                }
                node = node['children'][letter];
            }
            node['word'] = word;
        },

        empty: function () {
            rootNode = { word: null, children: {} };
        }
    }

});