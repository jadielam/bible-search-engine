angular.module("serviceSearchClient", []).factory("searchClient", function($q, $http){
	
	console.log("Search Client service created");
	var host = "http://localhost:8080/";
	var service_address = "bible-search-engine-ws-api/bible/";

	var put_content = function (link, type){
		
		return $http.put(link)
		.then(function (response){
			return response.data;
		}, function (response){
			return $q.reject(response);
		});
	};

	var put_content = function (link, type, payload){
		
		return $http.put(link, payload)
		.then(function (response){
			return response.data;
		}, function (response){
			return $q.reject(response);
		}); 
	};

	var post_content = function (link, type, payload){

		return $http.post(link, payload)
		.then(function (response){
			return response.data;
		}, function (response){
			return $q.reject(response);
		}); 
	};

	var get_content = function (link, type, rel){
		
		return $http.get(link)
		.then(function (response){
			return response.data;
		}, function (response){
			return $q.reject(response);
		});
	};

	var delete_content = function (link, type) {
		 
		return $http.delete(link)
		 .then (function (response){
		 	return response;
		 },
		 function (response){
		 	return $q.reject(response);
		 });

	}

	return {

		/**
		Submits a query to the server.
		*/
		getQueryResults: function(query) {
			var link = host + service_address + "query?query=" + query;
			var type = "application/json";
			var rel = "get";

			return get_content(link, type, rel);
		}

	}
});