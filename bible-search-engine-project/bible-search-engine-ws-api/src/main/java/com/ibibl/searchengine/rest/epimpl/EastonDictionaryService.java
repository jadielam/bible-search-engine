package com.ibibl.searchengine.rest.epimpl;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.solr.client.solrj.SolrServerException;

import com.ibibl.searchengine.ejb.bibledictionary.EastonDictionaryEJB;
import com.ibibl.searchengine.rest.ep.ResponseHelper;
import com.ibibl.searchengine.viewmodel.BibleDictionaryView;
import com.ibibl.searchengine.viewmodel.ListView;

@Path("/dictionary/easton")
public class EastonDictionaryService {

	@EJB
	private EastonDictionaryEJB ejb;
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getDictionaryEntries(@QueryParam("query") String query){
		
		ListView<BibleDictionaryView> listView;
		try {
			listView = this.ejb.getEastonEntries(query);
		} catch (SolrServerException e) {
			return ResponseHelper.errorResponse();
		} catch (IOException e) {
			return ResponseHelper.errorResponse();
		}
		
		List<BibleDictionaryView> items = listView.getItems();
		if (null == items) return ResponseHelper.emptyResponse();
		if (0 == items.size()) return ResponseHelper.emptyResponse();
		ResponseBuilder rb = Response.ok(listView);
		
		return rb.build();
	}
}
