// **************************************************
//
// Modification Tracking:
// $Revision:$
// $LastChangedBy:$
// $LastChangedDate:$
//
// **************************************************
package com.ibibl.searchengine.rest.ep;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.NotSupportedException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Link.Builder;

import com.ibibl.searchengine.ejb.OwningEJB;

import org.apache.log4j.Logger;


/**
 * This class will be extended by all the services that will
 * implement the following methods in OWNING entities:
 * 
 * <ul>
 * <li>findById</li>
 * <li>findAll</li>
 * <li>add</li>
 * <li>update</li>
 * <li>remove</li>
 * <li>advancedSearch</li>
 * </ul>
 * 
 * Every class that implements this abstract class MUST:
 * <ol type="a">
 * <li>Add a path annotation to the class that has the name of the 
 * owned entity, the id of the owned entity as a template,
 * and the name of the owning entity: @Path("/ownedEntityName/{id1}/owningEntityName").
 * </li>
 * <li>Implement a default constructor with no parameters that calls the constructor
 * OwningEntityService(Class<S> theOwnedType, Class<T> theOwningType). </li>
 * </ol>
 * 
 * For example:
 * @Path("/merchant/{id1}/contact")
 * class MerchantContactService extends OwningEntityService<Merchant, Contact> {
 * 
 * 		public MerchantContactService(){
 * 			super(Merchant.class, Contact.class);
 * 		}
 * 
 * }
 * 
 * @author jdearmas
 *
 * @since 0.1
 */
@Stateless
public abstract class OwningEntityService <S extends Object, T extends Object> {

	/**
	 * Logger for this class
	 */
	private static Logger LOGGER = Logger.getLogger(OwningEntityService.class);
	
	/**
	 * Class type of the S parameter of the implementing class
	 */
	private final Class<S> ownedType;
	
	/**
	 * Class type of the T parameter of the implementing class
	 */
	private final Class<T> owningType;
	
	/**
	 * The EJB for owning entities
	 */
	@EJB
	OwningEJB<S, T> ejb;
	
	/**
	 * Constructor that cannot be used. Here only to play tricks to Java.
	 */
	OwningEntityService(){
		this.ownedType = null;
		this.owningType = null;
		throw new UnsupportedOperationException("This constructor cannot be used.");
	}
	
	/**
	 * Sets the type of the parameter. This follows the pattern described in this Stack Overflow question:
	 * http://stackoverflow.com/questions/3403909/get-generic-type-of-class-at-runtime
	 * 
	 * @param theOwnedType the class type of the owned entity.
	 * @param theOwningType the class type of the owning entity.
	 */
	public OwningEntityService(Class<S> theOwnedType, Class<T> theOwningType){
		this.ownedType = theOwnedType;
		this.owningType = theOwningType;
	}
	
	/**
	 * Finds all the second level entities that have a many to one relationship
	 * with the entity in the first level with id <source>id1</source> 
	 * @param uriInfo The uri info of the request just made by the client
	 * @param ownedEntityId The first-level entity id.
	 * @param start Determines from where to return the entities. If not included
	 * in the url, will be null. If start is null, it will return the entities
	 * from the beginning.
	 * @param size Determines how many entities to be returned, if not included
	 * in the url it will be null. If size is null, it will return all entities from
	 * <code>start</code> until the end.
	 * @return A Response that includes the list of entities as json in the payload.
	 * The list could be empty.
	 */
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll(@Context UriInfo uriInfo, @PathParam("id1") Long ownedEntityId,
			@QueryParam("startIndex") Integer start, @QueryParam("size") Integer size){
		
		LOGGER.debug("Request to find all "+this.owningType.getSimpleName()+" of entity "+
				this.ownedType.getSimpleName()+" received with parameters: "+
				"start("+start+") and size("+size+")");
		
		//1. Getting the entities from the java bean
		Collection<T> entitiesList = new ArrayList<T>();
		
		try{
			entitiesList =  this.ejb.findAll(this.ownedType, this.owningType, ownedEntityId,
					start, size);
			LOGGER.debug("Found "+entitiesList.size()+" "+this.owningType.getSimpleName()+" types"+
					" of type "+this.ownedType.getSimpleName());
		}
		catch(Exception e){
			LOGGER.error("Error retrieving the "+this.owningType.getSimpleName()+" types"+
					" of type "+this.ownedType.getSimpleName());
			
			throw new WebApplicationException(500);
		}
		
		//2. Creating the previous and next link headers to be returned
		Link previous = createPreviousLink(uriInfo, start, size);
		Link next = createNextLink(uriInfo, start, size, entitiesList.size());
		
		ResponseBuilder responseBuilder = Response.ok(entitiesList);
		responseBuilder.header("Link", next);
		responseBuilder.header("Link", previous);
		
		return responseBuilder.build();	
	}
	
	/**
	 * Given a request with query parameters that represent
	 * filters on the child entity queried,
	 * it returns all entities in the database that satisfy those parameters.
	 * 
	 * The user of the rest API can send any parameters that they want.
	 * If at least one of the parameters is incorrect, it will return an
	 * error message back to the user.
	 * 
	 * @param uriInfo The uri info of the request just made by the client
	 * @param ownedEntityId the id of the parent entity
	 * @return a list of entities that satisfy the query (filtering) submitted
	 * by the request.
	 */
	@GET
	@Path("/advancedSearch")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<T> advancedSearch(@Context UriInfo uriInfo,
			@PathParam("id1") Long ownedEntityId){
		
		throw new NotSupportedException();
		
		//Collection<T> toReturn = new ArrayList<T>();
		//MultivaluedMap<String, String> queryParameters = uriInfo.getQueryParameters();
		//LOGGER.debug("Request of advanced Search on child entity "+this.owningType.getSimpleName()+
		//		" with parent entity of type "+this.ownedType.getSimpleName()+" and id "+
		//		ownedEntityId + " received with query parameters: "+queryParameters.toString());
		
		//try{
		//	toReturn = this.ejb.advancedSearch(this.ownedType, ownedEntityId,
		//			this.owningType, queryParameters);
			
		//}
		//return toReturn;
	}
	
	
	/**
	 * Finds the child entity with id <code>owningEntityId</code>
	 * whose parent entity has id <code>ownedEntityId</code>
	 * @param ownedEntityId The id of the parent entity
	 * @param owningEntityId The id of the child entity.
	 * @return The child entity if found, or a NOT FOUND error message
	 * if not found.
	 */
	@GET
	@Path("byId/{id2}")
	@Produces(MediaType.APPLICATION_JSON)
	public T byId(@PathParam("id1") Long ownedEntityId, @PathParam("id2") Long owningEntityId){
		
		LOGGER.debug("Request for finding "+this.owningType.getSimpleName()+" with id "+
				owningEntityId+" of entity "+this.ownedType.getSimpleName()+
				" with id "+ownedEntityId);
		
		T toReturn = null;
		try{
			toReturn = this.ejb.getById(this.ownedType, ownedEntityId, 
					this.owningType, owningEntityId);
			
			if (null != toReturn){
				LOGGER.debug("Found entity "+this.owningType.getSimpleName()+
						" with id "+owningEntityId.toString());
			}
			else{
				LOGGER.debug("Did not find entity "+this.owningType.getSimpleName()+
						" with id "+owningEntityId.toString());
			}
		}
		catch(IllegalArgumentException e){
			LOGGER.error("Error finding "+this.owningType.getSimpleName()+" with id "+
					owningEntityId+". "+e.getMessage(), e);
			
		}
		catch(Exception e){
			LOGGER.error("Error finding "+this.owningType.getSimpleName()+" with id "+
					owningEntityId+". "+e.getMessage(), e);
			
		}
		
		if (null == toReturn) throw new WebApplicationException(404);
		
		return toReturn;
	}
	

	/**
	 * Creates the URI for previous page for the functionality of pagination of the entities
	 * @param uriInfo The uri info of the request just made by the client
	 * @param start THe startIndex query parameter of the request just made by the client
	 * @param size The size query parameter of the request just made by the client
	 * @return Returns null if there is no previous link; otherwise it returns the previous link, 
	 * whose <code>startIndex</code> will be: Max(0, start - size)
	 */
	Link createPreviousLink(UriInfo uriInfo, Integer start, Integer size){
		
		UriBuilder previousURIBuilder = uriInfo.getAbsolutePathBuilder();
		Integer previousStart = null;
		Integer previousSize = null;
		
		//If there are no previous elements
		if (null == start || start.intValue() <= 0) {
			return null;
		}
		
		//If the size is the maximum allowed, keep it like so and return all the entities from the be-
		//ginning up to this point (this is a very unlikely case, but needed for correctness).
		if (null == size) {
			previousStart = new Integer(0);
		}
		//Else, everything is positive, and return Max(0, start - size)
		else {
			previousSize = size;
			previousStart = new Integer(Math.max(0, start.intValue() - size.intValue()));
		}
		
		previousURIBuilder.queryParam("startIndex", previousStart);
		if (null != previousSize) previousURIBuilder.queryParam("size", previousSize);
		
		URI previousURI = previousURIBuilder.build();
		Builder linkBuilder = Link.fromUri(previousURI);
		linkBuilder.rel("previous");
		linkBuilder.type("text/plain");
		
		return linkBuilder.build();
		
	}
	
	/**
	 * Creates the URI for next page for the functionality of pagination
	 * of the entities
	 * @param uriInfo The uri info of the request just made by the client
	 * @param start THe startIndex query parameter of the request just made by the client
	 * @param size The size query parameter of the request just made by the client
	 * @param listSize The size of the list of entities that will be returned as a response
	 * of the current request.
	 * @return Returns null if there is no next link.  Otherwise it returns the 
	 */
	Link createNextLink(UriInfo uriInfo, Integer start, Integer size, int listSize){
		UriBuilder nextURIBuilder = uriInfo.getAbsolutePathBuilder();
		Integer nextStart = null;
				
		//If there are no next elements to return, return null
		//It is not possible to detect if there are no next elements all the time
		//But if any of the following two cases are true, then there are no next elements to return:
		//a) If the size parameter is null
		//b) if the size of the list to be returned now is less than the size parameter
		if (null == size || listSize < size.intValue() ){
			return null;
		}
		
		//Otherwise, size is not null. If start is null, start is 0.  Then nextStart is
		//start + size
		//Different to previousLink, I don't have a way to bound the maximum start Index.
		nextStart = (null == start) ? new Integer(0): start;
		nextStart = nextStart + size;
		
		nextURIBuilder.queryParam("startIndex", nextStart);
		nextURIBuilder.queryParam("size", size);
		URI nextURI = nextURIBuilder.build();
		
		Builder linkBuilder = Link.fromUri(nextURI);
		linkBuilder.rel("next");
		linkBuilder.type("text/plain");
		
		return linkBuilder.build();
		
	}

}
