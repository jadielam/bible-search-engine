// **************************************************
//
// Modification Tracking:
// $Revision:$
// $LastChangedBy:$
// $LastChangedDate:$
//
// **************************************************


package com.ibibl.searchengine.rest;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.ibibl.searchengine.rest.epimpl.BibleAdvancedSearchService;
import com.ibibl.searchengine.rest.epimpl.BibleMetadataService;
import com.ibibl.searchengine.rest.epimpl.BibleReferenceService;
import com.ibibl.searchengine.rest.epimpl.BibleSimilarVersesService;
import com.ibibl.searchengine.rest.epimpl.BibleVerseSearchEngineService;
import com.ibibl.searchengine.rest.epimpl.BibleVersionsService;
import com.ibibl.searchengine.rest.epimpl.EastonDictionaryService;
import com.ibibl.searchengine.rest.epimpl.Test;

/**
 * 
 * @author jdearmas
 *
 * @since 1.0
 */
@ApplicationPath("/v1.0")
public class ApplicationConfig extends Application{
	
	/**
	 * 
	 */
    private Set<Object> resourceObjects = new HashSet<Object>();  
    
    /**
     * 
     */
    private Set<Class<?>> resourceClasses = new HashSet<Class<?>>();  
  
    /**
     * 
     */
    public ApplicationConfig() { 
    	
    	this.resourceClasses.add(Test.class);
    	this.resourceClasses.add(BibleVerseSearchEngineService.class);
    	this.resourceClasses.add(BibleVersionsService.class);
    	this.resourceClasses.add(BibleReferenceService.class);
    	this.resourceClasses.add(BibleMetadataService.class);
    	this.resourceClasses.add(BibleSimilarVersesService.class);
    	this.resourceClasses.add(EastonDictionaryService.class);
    	this.resourceClasses.add(BibleAdvancedSearchService.class);
    }  
    
    /**
     * 
     */
    @Override  
    public Set<Class<?>> getClasses() {  
        return this.resourceClasses;  
    }  
    
    /**
     * 
     */
    @Override  
    public Set<Object> getSingletons() {  
        return this.resourceObjects;  
    } 
    
}


