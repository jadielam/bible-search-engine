package com.ibibl.searchengine.rest.epimpl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.ibibl.searchengine.ejb.BibleMetadataEJB;
import com.ibibl.searchengine.rest.ep.ResponseHelper;
import com.ibibl.searchengine.viewmodel.BibleAuthorView;
import com.ibibl.searchengine.viewmodel.BibleBookView;
import com.ibibl.searchengine.viewmodel.BibleSectionView;
import com.ibibl.searchengine.viewmodel.BibleVersionView;
import com.ibibl.searchengine.viewmodel.ListView;

@Stateless
@Path("/bible/metadata")
public class BibleMetadataService {

	@EJB
	private BibleMetadataEJB bibleMetadata;
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/books")
	public Response getBibleBooks(){
		
		ListView<BibleBookView> books = this.bibleMetadata.getBibleBooks();
		if (null == books) return ResponseHelper.emptyResponse();
		@SuppressWarnings("rawtypes")
		List items = books.getItems();
		if (null == items) return ResponseHelper.emptyResponse();
		if (items.size() == 0) return ResponseHelper.emptyResponse();
		ResponseBuilder rb = Response.ok(books);
		
		return rb.build();
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/authors")
	public Response getBibleAuthors(){
		ListView<BibleAuthorView> authors = this.bibleMetadata.getBibleAuthors();
		if (null == authors) return ResponseHelper.emptyResponse();
		@SuppressWarnings("rawtypes")
		List items = authors.getItems();
		if (null == items) return ResponseHelper.emptyResponse();
		if (items.size() == 0) return ResponseHelper.emptyResponse();
		ResponseBuilder rb = Response.ok(authors);
		
		return rb.build();
	}
	
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/sections")
	public Response getBibleSections(){
		
		ListView<BibleSectionView> sections = this.bibleMetadata.getBibleSections();
		if (null == sections) return ResponseHelper.emptyResponse();
		@SuppressWarnings("rawtypes")
		List items = sections.getItems();
		if (null == items) return ResponseHelper.emptyResponse();
		if (items.size() == 0) return ResponseHelper.emptyResponse();
		ResponseBuilder rb = Response.ok(sections);
		
		return rb.build();
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/versions")
	public Response getBibleVersions(){
		
		ListView<BibleVersionView> versions = this.bibleMetadata.getBibleVersions();
		if (null == versions) return ResponseHelper.emptyResponse();
		@SuppressWarnings("rawtypes")
		List items = versions.getItems();
		if (null == items) return ResponseHelper.emptyResponse();
		if (items.size() == 0) return ResponseHelper.emptyResponse();
		ResponseBuilder rb = Response.ok(versions);
		
		return rb.build();
	}
	
}
