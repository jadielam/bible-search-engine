// **************************************************
//
// Modification Tracking:
// $Revision:$
// $LastChangedBy:$
// $LastChangedDate:$
//
// **************************************************
package com.ibibl.searchengine.rest.ep;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.TransactionRequiredException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.Link.Builder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import org.jboss.resteasy.annotations.GZIP;

import com.ibibl.searchengine.ejb.SimpleEJB;
import com.ibibl.searchengine.validation.Validator;


/**
 * <p>This class will be extended by all the services that
 * will implement the following methods in OWNED entities.:</p>
 * 
 * <ul>
 * <li>findById</li>
 * <li>findAll</li>
 * <li>add</li>
 * <li>update</li>
 * <li>remove</li>
 * </ul>
 * 
 * Every class that implements this abstract class MUST:
 * 
 * <ol type = "a">
 * <li>Add a path annotation to the class that has the name of the 
 * owned entity: @Path("/ownedEntityName")</li>
 * 
 * <li>Implement a default constructor with no parameters that calls the constructor
 * OwnedEntityService(Class<T> type). </li>
 * </ol>
 * 
 * For example:
 * @Path("/merchant")
 * class MerchantService extends OwnedEntityService<Merchant>{
 * 
 * 		public MerchantService(){
 * 			super(Merchant.class);
 * 		}
 * }
 * @author jdearmas
 * @param <T> The entity type
 *
 * @since 0.1 
 */
@Stateless
public abstract class SimpleEntityService <T extends Object>{

	/**
	 * Logger for this class.
	 */
	private static Logger LOGGER = Logger.getLogger(SimpleEntityService.class);

	/**
	 * Validates the entity of type t, and transforms it if necessary too.
	 */
	private final Validator<T> validator;
	
	/**
	 * type of the T parameter of the implementing class.
	 */
	private final Class<T> type;
	
	/**
	 * The ejb of owned entities
	 */
	@EJB
	SimpleEJB<T> ejb;
	
	/**
	 * This constructor is needed here but must never be used.
	 */
	SimpleEntityService() {
		this.type = null;
		throw new UnsupportedOperationException("Constructor cannot be used.");
	}
	
	/**
	 * Sets the type of the parameter. This is follows the pattern described
	 * in this Stack Overflow question:
	 * 
	 * http://stackoverflow.com/questions/3403909/get-generic-type-of-class-at-runtime
	 * 
	 * @param theType the class type of the entity.
	 * @param aValidator for the entity of type T.
	 */
	public SimpleEntityService(Class<T> theType, Validator<T> aValidator){
		this.type = theType;
		this.validator = aValidator;
	}
	
	/**
	 * Finds all of the entities in the persistence context. If the query parameters for paging
	 * are included, it will return the range of entities defined by the query indexes.
	 * @param start Determines from where to return the entities.  If not included in the url,
	 * will be null. If start is null, it will return the entities from the beginning.
	 * @param size Determines how many entities to be returned. If not included in url, 
	 * it will be null.  If size is null, it will return all entities from <code>start</code> til the end.
	 * @param uriInfo The uri info of the request just made by the client.
	 * @return A response with the list with all the entities as json in the payload. The list could be empty.  It also returns a previous and next link headers,
	 * that the client can use to request the previous or next page of entities.
	 * @throws Exception 
	 */
	@GET
	@GZIP
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll(@QueryParam("startIndex") Integer start, @QueryParam("size") Integer size,
			@Context UriInfo uriInfo) throws Exception{
		
		LOGGER.debug("Request to find all "+this.type.getSimpleName()+" received with parameters: start("+
				start+") and size("+size+")");
		
		//1. Getting the entities from the java bean.
		Collection<T> entitiesList = new ArrayList<T>();
		try{
			entitiesList = this.ejb.findAll(this.type, start, size);
			LOGGER.debug("Found "+entitiesList.size()+" elements of type "+this.type.getSimpleName());
		}
		catch (Exception e){
			LOGGER.error("Error retrieving the "+this.type.getName()+".  "+e.getMessage(), e);
			//Internal server error response
			throw new WebApplicationException(500);
		}
		
		//2. Creating the previous and next link headers to be returned.
		Link previous = createPreviousLink(uriInfo, start, size);
		Link next = createNextLink(uriInfo, start, size, entitiesList.size());
		
		//3. Attaching the headers to the response to be sent.
		//TODO: Check that the parameterized entitiesList object is handled well.
		ResponseBuilder responseBuilder = Response.ok(entitiesList);
		responseBuilder.header("Link", next);
		responseBuilder.header("Link", previous);
		
		return responseBuilder.build();
	}
	
	
	/**
	 * Finds the entity by the id provided by the user.
	 * Responses:
	 * 1. 200 OK with payload if the requested object was found.
	 * 2. 404 NOT_FOUND if the id does not exist in the database.
	 * 3. 400 BAD_REQUEST if the string provided as id does not match
	 * the type of the id in the database.
	 * @param id the id of the entity.
	 * @return the entity found.
	 * @throws WebApplicationException whenever something was wrong.
	 * See the Responses above. 
	 */
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public T byId(@PathParam("id") String id) throws WebApplicationException{
		
		if (null == id) throw new WebApplicationException(Response.Status.BAD_REQUEST);
		
		LOGGER.debug("Request for finding "+this.type.getName()+" by ID received");
		T toReturn = null;
		try{
			toReturn = this.ejb.getById(this.type, id);
			if (null != toReturn){
				LOGGER.debug("Found entity with id "+id.toString());
			}
			else{
				LOGGER.debug("Did not find any entity with id "+id.toString());
			}
		}
		catch (javax.ejb.EJBException e){
			Throwable t = e.getCause();
			if (t instanceof IllegalArgumentException){
				throw new WebApplicationException(Response.Status.BAD_REQUEST);
			}
			
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}
		catch (Exception e){
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}
				
		if (null == toReturn) throw new WebApplicationException(Response.Status.NOT_FOUND);
		
		return toReturn;
			
	}
	
	/**
	 * Receives in the payload the entity to be updated in the database.
	 * If the entity exists, it updates it. If the entity does not exist,
	 * it inserts a new one.  The insertion functionality should only be used by entities
	 * whose primary key is NOT autogenerated, but we will be soft on this restriction.
	 * @param entity the entity with the up-to-date data.
	 * @return Ok, with the payload containing the entity persisted to the database.
	 * The entity persisted to the database must be returned for the following reason:
	 * If no entity exists and the entity is inserted to the database, then
	 * the primary key of the entity will most likely have changed without the user
	 * knowing.
	 */
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public T update(T entity){
		
		LOGGER.debug("Request for updating "+this.type.getName()+" received");
		T validatedEntity = null;
		try{
			validatedEntity = this.validator.validateAndTransform(entity);
		}
		catch(IllegalArgumentException e){
			LOGGER.debug("Entity failed validation: "+this.type.getName()+": "+validatedEntity);
			throw new WebApplicationException(500);
		}
		
		try{
			this.ejb.update(validatedEntity);
			LOGGER.debug("Successfully added "+this.type.getName()+": "+validatedEntity);
		}
		catch(Exception e){
			LOGGER.error("Error updating "+this.type.getName()+": "+validatedEntity+
					". Error message: "+e.getMessage(), e);
			throw new WebApplicationException(500);
		}
		
		return validatedEntity;	
	}
	
	/**
	 * Receives in the payload the entity to be inserted to the database.
	 * If the entity exists, it returns an error message to the user.
	 * If the entity does not exist, it inserts it.
	 * 
	 * Response codes:
	 * 1. 200 OK with the payload
	 * 2. 400 BAD_REQUEST if the entity to insert did not pass validation
	 * 3. 409 CONFLICT If the entity to insert already exists in the database
	 * 4. 500 INTERNAL_SERVER_ERROR if something else is wrong with the server.
	 * 
	 * @param entity The entity to be inserted.
	 * @return the newly inserted payload, because if an autogenerated id was
	 * created, the user might want to have it.
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public T insert(T entity){
		LOGGER.debug("Request for inserting "+this.type.getName()+" received");
		T validatedEntity = null;
		
		try{
			validatedEntity = this.validator.validateAndTransform(entity);
		}
		catch(IllegalArgumentException e){
			LOGGER.debug("Entity failed validation: "+this.type.getName()+": "+validatedEntity);
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
		
		try{
			this.ejb.add(validatedEntity);
		}
		catch (javax.ejb.EJBException e){
			Throwable t = e.getCause();
			if (t instanceof javax.persistence.PersistenceException){
				throw new WebApplicationException(Response.Status.CONFLICT);
			}
			else if (t instanceof IllegalArgumentException){
				throw new WebApplicationException(Response.Status.BAD_REQUEST);
			}
			else if (t instanceof javax.validation.ConstraintViolationException){
				throw new WebApplicationException(Response.Status.BAD_REQUEST);
			}
			else if (t instanceof TransactionRequiredException){
				throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
			}
			else if (t instanceof EntityExistsException){
				throw new WebApplicationException(Response.Status.CONFLICT);
			}
			else if (t instanceof Exception){
				throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
			}
		}
		catch (Exception e){
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}
		
		return validatedEntity;
	}
	
	/**
	 * Deletes the entity specified by the id in the path. Returns a 204 No Content response if:
	 * a) the entity was deleted.
	 * b) the entity was not found and hence is not in the database.
	 * 
	 * Returns a 500 error if any other error happened. 
	 * @param id the id of the entry to delete
	 *  
	 */
	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public void removeById(@PathParam("id") String id){
		
		try{
			this.ejb.remove(this.type, id);
		}
		catch(Exception e){
			//Internal server error.
			throw new WebApplicationException(500);
		}
	}
	
	/**
	 * Creates the URI for previous page for the functionality of pagination of the entities
	 * @param uriInfo The uri info of the request just made by the client
	 * @param start THe startIndex query parameter of the request just made by the client
	 * @param size The size query parameter of the request just made by the client
	 * @return Returns null if there is no previous link; otherwise it returns the previous link, 
	 * whose <code>startIndex</code> will be: Max(0, start - size)
	 */
	Link createPreviousLink(UriInfo uriInfo, Integer start, Integer size){
		
		UriBuilder previousURIBuilder = uriInfo.getAbsolutePathBuilder();
		Integer previousStart = null;
		Integer previousSize = null;
		
		//If there are no previous elements
		if (null == start || start.intValue() <= 0) {
			return null;
		}
		
		//If the size is the maximum allowed, keep it like so and return all the entities from the be-
		//ginning up to this point (this is a very unlikely case, but needed for correctness).
		if (null == size) {
			previousStart = new Integer(0);
		}
		//Else, everything is positive, and return Max(0, start - size)
		else {
			previousSize = size;
			previousStart = new Integer(Math.max(0, start.intValue() - size.intValue()));
		}
		
		previousURIBuilder.queryParam("startIndex", previousStart);
		if (null != previousSize) previousURIBuilder.queryParam("size", previousSize);
		
		URI previousURI = previousURIBuilder.build();
		Builder linkBuilder = Link.fromUri(previousURI);
		linkBuilder.rel("previous");
		linkBuilder.type("text/plain");
		
		return linkBuilder.build();
		
	}
	
	/**
	 * Creates the URI for next page for the functionality of pagination
	 * of the entities
	 * @param uriInfo The uri info of the request just made by the client
	 * @param start THe startIndex query parameter of the request just made by the client
	 * @param size The size query parameter of the request just made by the client
	 * @param listSize The size of the list of entities that will be returned as a response
	 * of the current request.
	 * @return Returns null if there is no next link.  Otherwise it returns the 
	 */
	Link createNextLink(UriInfo uriInfo, Integer start, Integer size, int listSize){
		UriBuilder nextURIBuilder = uriInfo.getAbsolutePathBuilder();
		Integer nextStart = null;
				
		//If there are no next elements to return, return null
		//It is not possible to detect if there are no next elements all the time
		//But if any of the following two cases are true, then there are no next elements to return:
		//a) If the size parameter is null
		//b) if the size of the list to be returned now is less than the size parameter
		if (null == size || listSize < size.intValue() ){
			return null;
		}
		
		//Otherwise, size is not null. If start is null, start is 0.  Then nextStart is
		//start + size
		//Different to previousLink, I don't have a way to bound the maximum start Index.
		nextStart = (null == start) ? new Integer(0): start;
		nextStart = nextStart + size;
		
		nextURIBuilder.queryParam("startIndex", nextStart);
		nextURIBuilder.queryParam("size", size);
		URI nextURI = nextURIBuilder.build();
		
		Builder linkBuilder = Link.fromUri(nextURI);
		linkBuilder.rel("next");
		linkBuilder.type("text/plain");
		
		return linkBuilder.build();
		
	}
	
}
