package com.ibibl.searchengine.rest.ep;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

public class ResponseHelper {

	public static Response emptyResponse(){
		ResponseBuilder rb = Response.noContent();
		return rb.build();
	}
	
	public static Response errorResponse(){
		ResponseBuilder rb = Response.serverError();
		return rb.build();
	}
}
