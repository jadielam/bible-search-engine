// **************************************************
//
// Modification Tracking:
// $Revision:$
// $LastChangedBy:$
// $LastChangedDate:$
//
// **************************************************
package com.ibibl.searchengine.rest.ep;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
















import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.ibibl.searchengine.ejb.SearchEJB;
import com.ibibl.searchengine.index.Pair;

/**
 * @author jdearmas
 *
 * @since 1.0
 */
@Stateless
public class SearchService<T extends Object> {

	/**
	 * The EJB that will get the elements that we need to search
	 * from the database.
	 */
		
	private String fieldName;
	
	private Class<T> entityType;
			
	@EJB
	@Singleton
	private SearchEJB<T> ejb;
	
	/**
	 * This constructor cann't ever be used
	 */
	SearchService(){
		throw new UnsupportedOperationException("Constructor cannot be used");
	}
	
	public SearchService(Class<T> theType, String aFieldName) {
		this.fieldName = aFieldName;
		this.entityType = theType;
	
		//0. Create the entity manager factory, which is thread safe
				
		//1. Build the index from the database data.
							
		//2. Schedule task that builds the index every 30 minutes.
	}
	
	/**
	 * Returns the count of strings that are at a distance distance
	 * from the target string
	 * @param likeString
	 * @return
	 * @throws Exception 
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/count")
	public Integer getCount(@QueryParam("target") String target,
			@QueryParam("distance") Integer distance) throws Exception{
		
	
		int maxDistance = (null == distance) ? 0 : distance;
		try {
			Collection closeWords = this.ejb.getCloseEntries(this.entityType, 
					this.fieldName, target, maxDistance);
			return new Integer(closeWords.size());
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * Returns the list of strings that are at a distance distance
	 * from the target string
	 * @param target
	 * @param distance
	 * @return
	 * @throws Exception 
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/strings")
	public Collection<Pair<Object, String>> getStrings(@QueryParam("target") String target,
			@QueryParam("distance") Integer distance) throws Exception{
		
		
		int maxDistance = (null == distance) ? 0 : distance;
		try {
			Collection<Pair<Object, String>> closeWords = this.ejb.getCloseEntries(this.entityType, 
					this.fieldName, target, maxDistance);
			return closeWords;
		} catch (Exception e) {
			throw e;
		}
				
	}
	
	/**
	 * Returns the list of objects whose strings are at a distance from
	 * the target string.
	 * @param target
	 * @param distance
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<T> getObjects(@QueryParam("target") String target,
			@QueryParam("distance") Integer distance){
		
		//1. Spawns a thread that gets the strings with the 
		//exact match from the database
		
		//2. Runs the algorithm to get the list of strings that
		//are close to the target string
		
		//3. As soon as both the algorithm and the results from the
		//database are back, it goes back to the database and 
		//gets all those elements by the ids.
		
		//4. Returns all the elements back to the user.
		return null;
	}

}



