package com.ibibl.searchengine.rest.epimpl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.log4j.Logger;

import com.ibibl.searchengine.ejb.BibleReferenceEJB;
import com.ibibl.searchengine.rest.ep.ResponseHelper;
import com.ibibl.searchengine.viewmodel.BibleRangeView;
import com.ibibl.searchengine.viewmodel.BibleVerseView;
import com.ibibl.searchengine.viewmodel.ListView;

@Stateless
@Path("/bible/reference")
public class BibleReferenceService {

	@EJB
	@Singleton
	private BibleReferenceEJB bibleReference;
	
	private static Logger LOGGER = Logger.getLogger(BibleReferenceService.class);

	
	/**
	 * The query submitted to the service that returns results
	 * @param query a string
	 * @param start pagination parameter
	 * @param size pagination parameter
	 * @param versionAbbv the version to use in the display of results
	 * @return Returns either a ListView<BibleRangeView> object 
	 * @throws Exception
	 */
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response getReferenceVersesImplicit(
			@QueryParam("query") String query, 
			@QueryParam("version") String versionAbbv) throws Exception{
		
		LOGGER.info("QUERY SUBMITTED: "+query);
		
		ListView<BibleVerseView> listView = this.bibleReference.getReferencesBibleVerseView(query, versionAbbv);
		
		if (null == listView) return ResponseHelper.emptyResponse();
		@SuppressWarnings("rawtypes")
		List items = listView.getItems();
		if (null == items) return ResponseHelper.emptyResponse();
		if (items.size() == 0) return ResponseHelper.emptyResponse();
		ResponseBuilder rb = Response.ok(listView);
		
		return rb.build();	
	}

	
	/**
	 * The query submitted to the service that returns results
	 * @param query a string
	 * @param start pagination parameter
	 * @param size pagination parameter
	 * @param versionAbbv the version to use in the display of results
	 * @return Returns either a ListView<BibleRangeView> object 
	 * @throws Exception
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{contentType}")
	public Response getReferenceVersesExplicit(@PathParam("contentType") String contentType,
			@QueryParam("query") String query, 
			@QueryParam("version") String versionAbbv) throws Exception{
		
		LOGGER.info("QUERY SUBMITTED: "+query);
		
		@SuppressWarnings("rawtypes")
		ListView listView = null;
		
		if (contentType.equals("rangeview")){
			listView = this.bibleReference.getReferencesBibleRangeView(query, versionAbbv);
		}
		else  if (contentType.equals("verseview")){
			listView = this.bibleReference.getReferencesBibleVerseView(query, versionAbbv);
		}
		//Default to BibleVerseView
		else{
			listView = this.bibleReference.getReferencesBibleRangeView(query, versionAbbv);
		}
		
		if (null == listView) return ResponseHelper.emptyResponse();
		@SuppressWarnings("rawtypes")
		List items = listView.getItems();
		if (null == items) return ResponseHelper.emptyResponse();
		if (items.size() == 0) return ResponseHelper.emptyResponse();
		ResponseBuilder rb = Response.ok(listView);
		return rb.build();	
	}
	
	/**
	 * The query submitted to the service that returns results
	 * @param query a string
	 * @param start pagination parameter
	 * @param size pagination parameter
	 * @param versionAbbv the version to use in the display of results
	 * @return Returns a ListView<BibleVerseView> object.
	 * @throws Exception
	 */
	
	/**
	@GET
	@Produces("application/ListView.BibleVerseView-v1+json")
	public Response getReferenceVersesBibleVerseView(@QueryParam("query") String query, 
			@QueryParam("version") String versionAbbv) throws Exception{
		
		LOGGER.info("QUERY SUBMITTED: "+query);
		
		ListView<BibleVerseView> listView = this.bibleReference.getReferencesBibleVerseView(query, versionAbbv);
		if (null == listView) return ResponseHelper.emptyResponse();
		List<BibleVerseView> items = listView.getItems();
		if (null == items) return ResponseHelper.emptyResponse();
		if (items.size() == 0) return ResponseHelper.emptyResponse();
		ResponseBuilder rb = Response.ok(listView);
		return rb.build();
	}
	**/
}
