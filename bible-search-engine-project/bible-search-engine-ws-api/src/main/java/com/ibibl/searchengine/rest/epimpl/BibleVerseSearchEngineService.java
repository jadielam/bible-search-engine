package com.ibibl.searchengine.rest.epimpl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.log4j.Logger;

import com.ibibl.searchengine.ejb.BibleReferenceEJB;
import com.ibibl.searchengine.ejb.BibleSearchEngineEJB;
import com.ibibl.searchengine.viewmodel.BibleObject;
import com.ibibl.searchengine.viewmodel.BibleRangeView;
import com.ibibl.searchengine.viewmodel.BibleVerseSolrView;
import com.ibibl.searchengine.viewmodel.BibleVerseView;
import com.ibibl.searchengine.viewmodel.ListView;
import com.ibibl.searchengine.viewmodel.PaginationResultsView;

@Stateless
@Path("/query")
public class BibleVerseSearchEngineService {

	private static Logger LOGGER = Logger.getLogger(BibleVerseSearchEngineService.class);
	
	@EJB
	@Singleton
	private BibleSearchEngineEJB ejb;
	
	@EJB
	private BibleReferenceEJB bibleReference;
	
	/**
	 * The query submitted to the service that returns results
	 * @param query a string
	 * @param start pagination parameter
	 * @param size pagination parameter
	 * @param versionAbbv the version to use in the display of results
	 * @return Returns either a PaginationResultsView<BibleVerseView> or
	 * a List<BibleObject>, depending on if the query was a reference or
	 * a verses similarity.
	 * @throws Exception
	 */
	@GET
	@Produces({
			MediaType.APPLICATION_JSON
		})
	public Response getVerses(
			@QueryParam("query") String query, 
			@QueryParam("startIndex") Integer start,
			@QueryParam("size") Integer size, 
			@QueryParam("version") String versionAbbv) throws Exception{
		
		LOGGER.info("QUERY SUBMITTED: "+query);
		
		try{
			
			ListView<BibleRangeView> listView = null;
			listView = this.bibleReference.getReferencesBibleRangeView(query, versionAbbv);
					
			if (null == listView) throw new Exception();
			@SuppressWarnings("rawtypes")
			List items = listView.getItems();
			if (null == items) throw new Exception();
			if (items.size() == 0) throw new Exception();
			ResponseBuilder rb = Response.ok(listView);
			rb.header("Content-Type", MediaType.APPLICATION_JSON);
			return rb.build();
		}
		catch(Exception e){
						
			PaginationResultsView<BibleVerseView> toReturn = null;
			try{
				toReturn = this.ejb.getRankedBibleVerses(query, start, size, versionAbbv);	
			}
			catch(Exception e1){
				throw e1;
			}
			ResponseBuilder builder = Response.ok(toReturn);
			return builder.build();
			
		}
	}
	
	
	
}
