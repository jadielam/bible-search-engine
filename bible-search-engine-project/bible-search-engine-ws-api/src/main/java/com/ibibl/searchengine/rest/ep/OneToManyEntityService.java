// **************************************************
//
// Modification Tracking:
// $Revision:$
// $LastChangedBy:$
// $LastChangedDate:$
//
// **************************************************
package com.ibibl.searchengine.rest.ep;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.TransactionRequiredException;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Link.Builder;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.log4j.Logger;
import org.jboss.resteasy.annotations.GZIP;

import com.ibibl.searchengine.ejb.OneToManyEJB;
import com.ibibl.searchengine.validation.Validator;

/**
 * @author jdearmas
 *
 * @since 1.0
 */
@Stateless
public class OneToManyEntityService <S extends Object, T extends Object>{
	
	private static Logger LOGGER = Logger.getLogger(OneToManyEntityService.class);
	
	private final Class<S> parentEntityType;
	
	private final Class<T> childEntityType;
	
	private final Validator<T> childEntityValidator;
	
	@EJB
	OneToManyEJB<S, T> ejb;
	
	OneToManyEntityService() {
		this.parentEntityType = null;
		this.childEntityType = null;
		this.childEntityValidator = null;
		
		throw new UnsupportedOperationException("Constructor cannot be used");
	}
	
	public OneToManyEntityService(Class<S> theParentEntityType,
			Class<T> theChildEntityType, Validator<T> theChildEntityValidator){
		this.parentEntityType = theParentEntityType;
		this.childEntityType = theChildEntityType;
		this.childEntityValidator = theChildEntityValidator;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public T postChildToParent(@PathParam("id1") String parentId, T entity) throws Exception{
		T validatedEntity = null;
		try{
			validatedEntity = this.childEntityValidator.validateAndTransform(entity);
		}
		catch (IllegalArgumentException e){
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
		
		try{
			this.ejb.postChildToParent(this.parentEntityType, this.childEntityType, 
					parentId, validatedEntity);
		}
		catch(Exception e){
			throw e;
		}

		return validatedEntity;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id2}")
	public T getChildById(@PathParam("id1") String parentId,
			@PathParam("id2") String childId) throws Exception{
		
		try{
			T t = this.ejb.findById(this.parentEntityType, this.childEntityType, 
					parentId, childId);
			return t;
		}
		catch (Exception e){
			throw e;
		}
	}
	
	@GET
	@GZIP
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllChildren(@PathParam("id1") String parentId,
			@Context UriInfo uriInfo, @QueryParam("startIndex") Integer start, 
			@QueryParam("size") Integer size){

		LOGGER.debug("Request to find all "+this.childEntityType.getSimpleName()+" received with parameters: start("+
				start+") and size("+size+")");
		
		//1. Getting the entities from the java bean.
		Collection<T> entitiesList = new ArrayList<T>();
		try{
			entitiesList = this.ejb.findAll(this.parentEntityType, this.childEntityType, parentId, start, size);
			LOGGER.debug("Found "+entitiesList.size()+" elements of type "+this.childEntityType.getSimpleName());
		}
		catch (Exception e){
			LOGGER.error("Error retrieving the "+this.childEntityType.getName()+".  "+e.getMessage(), e);
			
			//TODO: Work on this later on.
			//Internal server error response
			throw new WebApplicationException(500);
		}
		
		//2. Creating the previous and next link headers to be returned.
		Link previous = createPreviousLink(uriInfo, start, size);
		Link next = createNextLink(uriInfo, start, size, entitiesList.size());
		
		//3. Attaching the headers to the response to be sent.
		//TODO: Check that the parameterized entitiesList object is handled well.
		ResponseBuilder responseBuilder = Response.ok(entitiesList);
		responseBuilder.header("Link", next);
		responseBuilder.header("Link", previous);
		
		return responseBuilder.build();

	}
	
	/**
	 * Creates the URI for previous page for the functionality of pagination of the entities
	 * @param uriInfo The uri info of the request just made by the client
	 * @param start THe startIndex query parameter of the request just made by the client
	 * @param size The size query parameter of the request just made by the client
	 * @return Returns null if there is no previous link; otherwise it returns the previous link, 
	 * whose <code>startIndex</code> will be: Max(0, start - size)
	 */
	Link createPreviousLink(UriInfo uriInfo, Integer start, Integer size){
		
		UriBuilder previousURIBuilder = uriInfo.getAbsolutePathBuilder();
		Integer previousStart = null;
		Integer previousSize = null;
		
		//If there are no previous elements
		if (null == start || start.intValue() <= 0) {
			return null;
		}
		
		//If the size is the maximum allowed, keep it like so and return all the entities from the be-
		//ginning up to this point (this is a very unlikely case, but needed for correctness).
		if (null == size) {
			previousStart = new Integer(0);
		}
		//Else, everything is positive, and return Max(0, start - size)
		else {
			previousSize = size;
			previousStart = new Integer(Math.max(0, start.intValue() - size.intValue()));
		}
		
		previousURIBuilder.queryParam("startIndex", previousStart);
		if (null != previousSize) previousURIBuilder.queryParam("size", previousSize);
		
		URI previousURI = previousURIBuilder.build();
		Builder linkBuilder = Link.fromUri(previousURI);
		linkBuilder.rel("previous");
		linkBuilder.type("text/plain");
		
		return linkBuilder.build();
		
	}
	
	/**
	 * Creates the URI for next page for the functionality of pagination
	 * of the entities
	 * @param uriInfo The uri info of the request just made by the client
	 * @param start THe startIndex query parameter of the request just made by the client
	 * @param size The size query parameter of the request just made by the client
	 * @param listSize The size of the list of entities that will be returned as a response
	 * of the current request.
	 * @return Returns null if there is no next link.  Otherwise it returns the 
	 */
	Link createNextLink(UriInfo uriInfo, Integer start, Integer size, int listSize){
		UriBuilder nextURIBuilder = uriInfo.getAbsolutePathBuilder();
		Integer nextStart = null;
				
		//If there are no next elements to return, return null
		//It is not possible to detect if there are no next elements all the time
		//But if any of the following two cases are true, then there are no next elements to return:
		//a) If the size parameter is null
		//b) if the size of the list to be returned now is less than the size parameter
		if (null == size || listSize < size.intValue() ){
			return null;
		}
		
		//Otherwise, size is not null. If start is null, start is 0.  Then nextStart is
		//start + size
		//Different to previousLink, I don't have a way to bound the maximum start Index.
		nextStart = (null == start) ? new Integer(0): start;
		nextStart = nextStart + size;
		
		nextURIBuilder.queryParam("startIndex", nextStart);
		nextURIBuilder.queryParam("size", size);
		URI nextURI = nextURIBuilder.build();
		
		Builder linkBuilder = Link.fromUri(nextURI);
		linkBuilder.rel("next");
		linkBuilder.type("text/plain");
		
		return linkBuilder.build();
		
	}
	
}

