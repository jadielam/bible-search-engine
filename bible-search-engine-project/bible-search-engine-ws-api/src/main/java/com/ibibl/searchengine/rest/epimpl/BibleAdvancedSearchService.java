package com.ibibl.searchengine.rest.epimpl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.ibibl.bible.Book;
import com.ibibl.searchengine.ejb.BibleAdvancedSearchEJB;
import com.ibibl.searchengine.ejb.BibleMetadataEJB;
import com.ibibl.searchengine.rest.ep.ResponseHelper;
import com.ibibl.searchengine.viewmodel.BibleVerseView;
import com.ibibl.searchengine.viewmodel.PaginationResultsView;

@Stateless
@Path("/bible/advancedsearch")
public class BibleAdvancedSearchService {

	private static String FILTER_SEPARATOR_REGEX = ",";
	
	@EJB
	private BibleMetadataEJB bibleMetadata;
	
	@EJB
	private BibleAdvancedSearchEJB advancedSearch;
	
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/filtered")
	public Response filteredAdvancedSearch(@QueryParam("query") String query,
			@QueryParam("bookf") String bookfilters,
			@QueryParam("authorf") String authorfilters,
			@QueryParam("sectionf") String sectionfilters,
			@QueryParam("sort") String sortOption,
			@QueryParam("version") String versionAbbv,
			@QueryParam("startIndex") Integer start,
			@QueryParam("size") Integer size){
		
		try{
			ArrayList<String> filteredBookAbbvs = new ArrayList<String>();
			ArrayList<String> filteredAuthors = new ArrayList<String>();
			ArrayList<String> filteredSections = new ArrayList<String>();
			if (null != bookfilters){
				String [] bookAbbvs = bookfilters.split(FILTER_SEPARATOR_REGEX);
				filteredBookAbbvs = this.filterBookAbbvs(bookAbbvs);
			}
						
			if (null != authorfilters ){
				String [] authors = authorfilters.split(FILTER_SEPARATOR_REGEX);
				filteredAuthors = this.filterAuthors(authors);
			}
			
			if (null != sectionfilters){
				String [] sections = sectionfilters.split(FILTER_SEPARATOR_REGEX);
				filteredSections = this.filterSections(sections);
			}
			
			PaginationResultsView<BibleVerseView> toReturn = this.advancedSearch.getFilteredResults(query, 
					filteredAuthors, 
					filteredBookAbbvs, 
					filteredSections, 
					versionAbbv, 
					sortOption, 
					start, 
					size);
			
			ResponseBuilder rb = Response.ok(toReturn);
			return rb.build();
		}
		catch(Exception e){
			return ResponseHelper.errorResponse();
		}
	}
	
	/**
	 * Filters the list of sections of the Bible, keeping only the ones
	 * that are correct.
	 * @param sections
	 * @return
	 */
	private ArrayList<String> filterSections(String[] sections) {
		ArrayList<String> toReturn = new ArrayList<String>();
		for (String section : sections){
			if(this.bibleMetadata.isSection(section)){
				toReturn.add(section);
			}
		}
		
		return toReturn;
	}

	/**
	 * Filters the list of book authors, keeping only the ones that are correct.
	 * @param authors
	 * @return
	 */
	private ArrayList<String> filterAuthors(String[] authors) {
		ArrayList<String> toReturn = new ArrayList<String>();
		for (String author : authors){
			if (this.bibleMetadata.isAuthor(author)){
				toReturn.add(author);
			}
		}
		return toReturn;
	}

	/**
	 * Filters the list of book abbvs and returns it filtered, keeping only
	 * the book abbvs that are correct.
	 * @param bookAbbvs
	 * @return
	 * @throws Exception
	 */
	private ArrayList<String> filterBookAbbvs(String[] bookAbbvs) {
		ArrayList<String> toReturn = new ArrayList<String>();
		for (String bookAbbv : bookAbbvs){
			Book book = Book.fromAbbreviation(bookAbbv);
			if (null != book){
				toReturn.add(bookAbbv);	
			}
		}
		return toReturn;
	}

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/groupby")
	public Response groupbyAdvancedSearch(){
		return null;
	}
}
