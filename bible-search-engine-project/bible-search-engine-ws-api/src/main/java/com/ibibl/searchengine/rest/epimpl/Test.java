// **************************************************
//
// Modification Tracking:
// $Revision:$
// $LastChangedBy:$
// $LastChangedDate:$
//
// **************************************************
package com.ibibl.searchengine.rest.epimpl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;


/**
 * @author agattamaneni
 *
 */
@Stateless
@Path("/test")
public class Test {

	private static Logger LOGGER = Logger.getLogger(Test.class);

	
	public Test() {
		
	}

	// http://localhost:8080/successful-practices-ws-api/scp/applicationUser/all
	/**
	 * Finds and returns all the Application Users available in system
	 *
	 * @return the list of all Application Users
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String findAllApplicationUsers(){
		LOGGER.debug("Request for finding all Application Users received");
		
		String toReturn = "this is a test.";
		
		return toReturn;
		
	}

	
}