package com.ibibl.searchengine.rest.epimpl;

import javax.ejb.Stateless;
import javax.ws.rs.Path;

import com.ibibl.searchengine.rest.ep.SimpleEntityService;
import com.ibibl.searchengine.validation.BibleVerseValidator;
import com.ibibl.searchengine.model.BibleVerseModel;

@Stateless
@Path("/verse")
public class BibleVerseService extends SimpleEntityService<BibleVerseModel>{

	
	public BibleVerseService() {
		super(BibleVerseModel.class, new BibleVerseValidator());
	}

	

}
