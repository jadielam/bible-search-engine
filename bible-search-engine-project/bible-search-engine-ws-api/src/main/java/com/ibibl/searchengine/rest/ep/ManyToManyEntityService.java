// **************************************************
//
// Modification Tracking:
// $Revision:$
// $LastChangedBy:$
// $LastChangedDate:$
//
// **************************************************
package com.ibibl.searchengine.rest.ep;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.ibibl.searchengine.ejb.ManyToManyEJB;

/**
 * @author jdearmas
 *
 * @since  
 */
@Stateless
public abstract class ManyToManyEntityService <R extends Object, S extends Object, T extends Object> {
	
	private static Logger LOGGER = Logger.getLogger(ManyToManyEntityService.class);
	
	private final Class<R> associationType;
	
	private final Class<S> firstEntityType;
	
	private final Class<T> secondEntityType;
	
	@EJB
	ManyToManyEJB<R, S, T> ejb;
	
	ManyToManyEntityService() {
		this.associationType = null;
		this.firstEntityType = null;
		this.secondEntityType = null;
		
		throw new UnsupportedOperationException("Constructor cannot be used");
	}
	
	public ManyToManyEntityService(Class<R> theAssociationType, 
			Class<S> theFirstEntityType, Class<T> theSecondEntityType){
		
		this.associationType = theAssociationType;
		this.firstEntityType = theFirstEntityType;
		this.secondEntityType = theSecondEntityType;
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id2}")
	public void addRelationship(@PathParam("id1") String firstEntityKey,
			@PathParam("id2") String secondEntityKey, @Context UriInfo info) throws IllegalAccessException, ClassNotFoundException, InstantiationException{
			
			//Get other values also set with the association, if it is that the
			//association table also has other fields.
			MultivaluedMap<String, String> multiQueryParams = info.getQueryParameters();
			Map<String, String> queryParams = new HashMap<String, String>();
			Set<Entry<String, List<String>>> entrySet = multiQueryParams.entrySet();
			for (Entry<String, List<String>> entry : entrySet){
				String parameterName = entry.getKey();
				List<String> parameterValues = entry.getValue();
				if (parameterValues.size() > 0){
					queryParams.put(parameterName, parameterValues.get(0));
				}
			}
			
			//Pass the data to the EJB.
			try {
				this.ejb.addRelationship(this.associationType, this.firstEntityType, firstEntityKey, this.secondEntityType, secondEntityKey, queryParams);
			} catch (InstantiationException e) {
				throw e;
			} catch (ClassNotFoundException e) {
				throw e;
			} catch (IllegalAccessException e) {
				
				throw e;
			}
		
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id2}")
	public void removeRelationship(@PathParam("id1") String firstEntityKey,
			@PathParam("id2") String secondEntityKey) throws Exception {
	
		try{
			this.ejb.removeRelationship(this.associationType, this.firstEntityType, firstEntityKey, this.secondEntityType, secondEntityKey);
		}
		catch (Exception e){
			//TODO Used so far for testing purposes. Remove and put proper exceptions here
			throw e;
		}
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<R> getAllByFirstEntity(@PathParam("id1") String firstEntityKey) throws Exception{
		
		List<R> entities = new ArrayList<R>();
		try{
			entities = this.ejb.getAssociationsByFirstEntity(this.associationType, this.firstEntityType, firstEntityKey);
		}
		catch(Exception e){
			//TODO Used so far for testing purposes. Remove and put proper exceptions here
			throw e;
		}
		return entities;
	}

}
