package com.ibibl.searchengine.rest.epimpl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.ibibl.bible.BibleVersion;

/**
 * Used to get the available versions and some extra
 * information about them.
 * @author jadiel
 *
 */
@Stateless
@Path("/bible/version")
public class BibleVersionsService {

	private static Logger LOGGER = Logger.getLogger(BibleVersionsService.class);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<BibleVersion> getVersions(){
		
		LOGGER.info("QUERY SUBMITTED - get available versions");
		List<BibleVersion> versions = new ArrayList<BibleVersion>();
		versions.add(BibleVersion.NIV);
		versions.add(BibleVersion.CEB);
		versions.add(BibleVersion.ASV);
				
		return versions;
	}

}
