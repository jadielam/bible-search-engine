// **************************************************
//
// Modification Tracking:
// $Revision:$
// $LastChangedBy:$
// $LastChangedDate:$
//
// **************************************************
package com.ibibl.searchengine.rest.ep;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.TransactionRequiredException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.Link.Builder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.ibibl.searchengine.ejb.OwnedEJB;
import com.ibibl.searchengine.ejb.SimpleEJB;


/**
 * <p>This class will be extended by all the services that
 * will implement the following methods in OWNED entities.:</p>
 * 
 * <ul>
 * <li>findById</li>
 * <li>findAll</li>
 * <li>add</li>
 * <li>update</li>
 * <li>remove</li>
 * <li>advancedSearch</li>
 * </ul>
 * 
 * Every class that implements this abstract class MUST:
 * 
 * <ol type = "a">
 * <li>Add a path annotation to the class that has the name of the 
 * owned entity: @Path("/ownedEntityName")</li>
 * 
 * <li>Implement a default constructor with no parameters that calls the constructor
 * OwnedEntityService(Class<T> type). </li>
 * </ol>
 * 
 * For example:
 * @Path("/merchant")
 * class MerchantService extends OwnedEntityService<Merchant>{
 * 
 * 		public MerchantService(){
 * 			super(Merchant.class);
 * 		}
 * }
 * @author jdearmas
 * @param <T> The entity type
 *
 * @since 0.1 
 */
@Stateless
public abstract class OwnedEntityService <T extends Object>{

	/**
	 * Logger for this class.
	 */
	private static Logger LOGGER = Logger.getLogger(OwnedEntityService.class);

	/**
	 * type of the T parameter of the implementing class.
	 */
	private final Class<T> type;
	
	/**
	 * The ejb of owned entities
	 */
	@EJB
	OwnedEJB<T> ejb;
	
	/**
	 * This constructor is needed here but must never be used.
	 */
	OwnedEntityService() {
		this.type = null;
		throw new UnsupportedOperationException("Constructor cannot be used.");
	}
	
	/**
	 * Sets the type of the parameter. This is follows the pattern described
	 * in this Stack Overflow question:
	 * 
	 * http://stackoverflow.com/questions/3403909/get-generic-type-of-class-at-runtime
	 * 
	 * @param theType the class type of the entity.
	 */
	public OwnedEntityService(Class<T> theType){
		this.type = theType;
	}
	
	/**
	 * Finds all of the entities in the persistence context. If the query parameters for paging
	 * are included, it will return the range of entities defined by the query indexes.
	 * @param start Determines from where to return the entities.  If not included in the url,
	 * will be null. If start is null, it will return the entities from the beginning.
	 * @param size Determines how many entities to be returned. If not included in url, 
	 * it will be null.  If size is null, it will return all entities from <code>start</code> til the end.
	 * @param uriInfo The uri info of the request just made by the client.
	 * @return A response with the list with all the entities as json in the payload. The list could be empty.  It also returns a previous and next link headers,
	 * that the client can use to request the previous or next page of entities.
	 */
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll(@QueryParam("startIndex") Integer start, @QueryParam("size") Integer size,
			@Context UriInfo uriInfo){
		
		LOGGER.debug("Request to find all "+this.type.getSimpleName()+" received with parameters: start("+
				start+") and size("+size+")");
		
		//1. Getting the entities from the java bean.
		Collection<T> entitiesList = new ArrayList<T>();
		try{
			entitiesList = this.ejb.findAll(this.type, start, size);
			LOGGER.debug("Found "+entitiesList.size()+" elements of type "+this.type.getSimpleName());
		}
		catch (Exception e){
			LOGGER.error("Error retrieving the "+this.type.getName()+".  "+e.getMessage(), e);
			//Internal server error response
			throw new WebApplicationException(500);
		}
		
		//2. Creating the previous and next link headers to be returned.
		Link previous = createPreviousLink(uriInfo, start, size);
		Link next = createNextLink(uriInfo, start, size, entitiesList.size());
		
		//3. Attaching the headers to the response to be sent.
		//TODO: Check that the parameterized entitiesList object is handled well.
		ResponseBuilder responseBuilder = Response.ok(entitiesList);
		responseBuilder.header("Link", next);
		responseBuilder.header("Link", previous);
		
		return responseBuilder.build();
	}
	
	/**
	 * Given a request with query parameters that represent filters on the entity queried,
	 * it returns all the entities in the database that satisfy those parameters.
	 * 
	 * The user of the rest API can send any parameters that they want.  If at least
	 * one of the parameters is incorrect, it will return an error message back to the user. 
	 * 
	 * @param uriInfo The uri info of the request just made by the client 
	 * @return a list of entities that satisfy the query submitted.
	 */
	@GET
	@Path("/advancedSearch")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<T> advancedSearch(@Context UriInfo uriInfo){
			
		Collection<T> entitiesList = new ArrayList<T>();
		MultivaluedMap<String, String> queryParameters = uriInfo.getQueryParameters();
		LOGGER.debug("Request of advanced Search on entity "+this.type.getName()+" received with +"
				+ "query parameters: "+queryParameters.toString());
		
		try{
			entitiesList = this.ejb.advancedSearch(this.type, queryParameters);
			LOGGER.debug("Found "+entitiesList.size()+" elements of type "+this.type.getName());
		}
		catch(Exception e){
			LOGGER.error("Error retrieving the "+this.type.getName()+".  "+e.getMessage(), e);
			//Internal server error response
			throw new WebApplicationException(500);
		}
		
		
		return entitiesList;
	}
	
	/**
	 * Finds the entity by the id provided by the user.
	 * If it cannot find it, it returns null.
	 * @param id the id of the entity.
	 * @return the entity found or null if nothing is found.
	 */
	@GET
	@Path("/byId/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public T byId(@PathParam("id") Long id){
		
		LOGGER.debug("Request for finding "+this.type.getName()+" by ID received");
		T toReturn = null;
		try{
			toReturn = this.ejb.getById(this.type, id);
			if (null != toReturn){
				LOGGER.debug("Found entity with id "+id.toString());
			}
			else{
				LOGGER.debug("Did not find any entity with id "+id.toString());
			}
		}
		catch (IllegalArgumentException e){
			LOGGER.error("Error finding "+this.type.getName()+" by ID "+id+". "+e.getMessage(), e);
		}
		
		if (null == toReturn) throw new WebApplicationException(404);
		
		return toReturn;
			
	}
	
	/**
	 * Receives in the payload the entity to be updated in the database.
	 * If the entity exists, it updates it. If the entity does not exist,
	 * it inserts a new one.
	 * @param entity the entity with the up-to-date data.
	 * @return Ok, with the payload containing the entity persisted to the database.
	 * The entity persisted to the database must be returned for the following reason:
	 * If no entity exists and the entity is inserted to the database, then
	 * the primary key of the entity will most likely have changed without the user
	 * knowing.
	 */
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public T update(T entity){
		LOGGER.debug("Request for updating "+this.type.getName()+" received");
		try{
			this.ejb.update(entity);
			
			LOGGER.debug("Successfully added "+this.type.getName()+": "+entity);
		}
		catch(Exception e){
			LOGGER.error("Error updating "+this.type.getName()+": "+entity+
					". Error message: "+e.getMessage(), e);
			throw new WebApplicationException(500);
		}
		
		return entity;
			
	}
	
	/**
	 * Receives in the payload the entity to be inserted to the database.
	 * If the entity exists, it returns an error message to the user.
	 * If the entity does not exist, it inserts it.
	 * @param entity The entity to be inserted.
	 * @return OK with the newly inserted payload, or FAIL if the entity existed
	 * in the database
	 * TODO: Properly define the return type message when failing to insert.
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public T insert(T entity){
		LOGGER.debug("Request for inserting "+this.type.getName()+" received");
		try{
			this.ejb.add(entity);
		}
		//The messages returned to the user should vary depending on the exception.
		catch(EntityExistsException e){
			//TODO: Return the proper message to the user.
		}
		catch(IllegalArgumentException e){
			//TODO: Return the proper message to the user.
		}
		catch(TransactionRequiredException e){
			//TODO: Return the proper message to the user.
		}
		
		return entity;
	}
	
	/**
	 * Deletes the entity specified by the id in the path. Returns a 204 No Content response if:
	 * a) the entity was deleted.
	 * b) the entity was not found and hence is not in the database.
	 * 
	 * Returns a 500 error if any other error happened. 
	 * @param id the id of the entry to delete
	 *  
	 */
	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public void removeById(@PathParam("id") Long id){
		
		try{
			this.ejb.remove(this.type, id);
		}
		catch(Exception e){
			//Internal server error.
			throw new WebApplicationException(500);
		}
	}
	
	/**
	 * Creates the URI for previous page for the functionality of pagination of the entities
	 * @param uriInfo The uri info of the request just made by the client
	 * @param start THe startIndex query parameter of the request just made by the client
	 * @param size The size query parameter of the request just made by the client
	 * @return Returns null if there is no previous link; otherwise it returns the previous link, 
	 * whose <code>startIndex</code> will be: Max(0, start - size)
	 */
	Link createPreviousLink(UriInfo uriInfo, Integer start, Integer size){
		
		UriBuilder previousURIBuilder = uriInfo.getAbsolutePathBuilder();
		Integer previousStart = null;
		Integer previousSize = null;
		
		//If there are no previous elements
		if (null == start || start.intValue() <= 0) {
			return null;
		}
		
		//If the size is the maximum allowed, keep it like so and return all the entities from the be-
		//ginning up to this point (this is a very unlikely case, but needed for correctness).
		if (null == size) {
			previousStart = new Integer(0);
		}
		//Else, everything is positive, and return Max(0, start - size)
		else {
			previousSize = size;
			previousStart = new Integer(Math.max(0, start.intValue() - size.intValue()));
		}
		
		previousURIBuilder.queryParam("startIndex", previousStart);
		if (null != previousSize) previousURIBuilder.queryParam("size", previousSize);
		
		URI previousURI = previousURIBuilder.build();
		Builder linkBuilder = Link.fromUri(previousURI);
		linkBuilder.rel("previous");
		linkBuilder.type("text/plain");
		
		return linkBuilder.build();
		
	}
	
	/**
	 * Creates the URI for next page for the functionality of pagination
	 * of the entities
	 * @param uriInfo The uri info of the request just made by the client
	 * @param start THe startIndex query parameter of the request just made by the client
	 * @param size The size query parameter of the request just made by the client
	 * @param listSize The size of the list of entities that will be returned as a response
	 * of the current request.
	 * @return Returns null if there is no next link.  Otherwise it returns the 
	 */
	Link createNextLink(UriInfo uriInfo, Integer start, Integer size, int listSize){
		UriBuilder nextURIBuilder = uriInfo.getAbsolutePathBuilder();
		Integer nextStart = null;
				
		//If there are no next elements to return, return null
		//It is not possible to detect if there are no next elements all the time
		//But if any of the following two cases are true, then there are no next elements to return:
		//a) If the size parameter is null
		//b) if the size of the list to be returned now is less than the size parameter
		if (null == size || listSize < size.intValue() ){
			return null;
		}
		
		//Otherwise, size is not null. If start is null, start is 0.  Then nextStart is
		//start + size
		//Different to previousLink, I don't have a way to bound the maximum start Index.
		nextStart = (null == start) ? new Integer(0): start;
		nextStart = nextStart + size;
		
		nextURIBuilder.queryParam("startIndex", nextStart);
		nextURIBuilder.queryParam("size", size);
		URI nextURI = nextURIBuilder.build();
		
		Builder linkBuilder = Link.fromUri(nextURI);
		linkBuilder.rel("next");
		linkBuilder.type("text/plain");
		
		return linkBuilder.build();
		
	}
	
}
