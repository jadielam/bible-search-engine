<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<artifactId>member-offer-database-project</artifactId>
		<groupId>com.aaa.aicweb.memberoffer</groupId>
		<version>1.0-SNAPSHOT</version>
	</parent>

	<groupId>com.aaa.aicweb.memberoffer</groupId>
	<artifactId>member-offer-database-ws-api</artifactId>
	<version>1.0-SNAPSHOT</version>
	<packaging>war</packaging>

	<name>member-offer-database-API</name>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<jackson.version>2.4.2</jackson.version>
		<jaxrs.version>2.0.1</jaxrs.version>
		<servlet.api.version>3.1.0</servlet.api.version>
		<javax.ejb.version>3.2</javax.ejb.version>
		<resteasy.jaxrs.version>3.0.7.Final</resteasy.jaxrs.version>
		<javax.web.api.version>6.0</javax.web.api.version>
	</properties>


	<dependencies>
		<dependency>
			<groupId>com.aaa.aicweb.memberoffer</groupId>
			<artifactId>member-offer-database-ws-dao</artifactId>
			<version>1.0-SNAPSHOT</version>
			<type>ejb</type>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>com.aaa.aicweb.memberoffer</groupId>
			<artifactId>member-offer-database-ws-persistence</artifactId>
			<version>1.0-SNAPSHOT</version>
			<type>jar</type>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>javax</groupId>
			<artifactId>javaee-web-api</artifactId>
			<version>${javax.web.api.version}</version>
			<scope>provided</scope>
		</dependency>

		<!-- core library -->
		<dependency>
			<groupId>org.jboss.resteasy</groupId>
			<artifactId>resteasy-jaxrs</artifactId>
			<version>${resteasy.jaxrs.version}</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>com.fasterxml.jackson.jaxrs</groupId>
			<artifactId>jackson-jaxrs-json-provider</artifactId>
			<version>${jackson.version}</version>
		</dependency>
		<dependency>
			<groupId>javax.ws.rs</groupId>
			<artifactId>javax.ws.rs-api</artifactId>
			<version>${jaxrs.version}</version>
		</dependency>
		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>javax.servlet-api</artifactId>
			<version>${servlet.api.version}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>javax.ejb</groupId>
			<artifactId>javax.ejb-api</artifactId>
			<version>${javax.ejb.version}</version>
		</dependency>
		<!-- Javaee API -->
		<dependency>
			<groupId>javax</groupId>
			<artifactId>javaee-api</artifactId>
			<version>6.0</version>
			<scope>test</scope>
		</dependency>
		<!-- Javaee API -->
		<dependency>
			<groupId>javax</groupId>
			<artifactId>javaee-api</artifactId>
			<version>6.0</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<version>3.0</version>
		</dependency>
		<dependency>
			<groupId>log4j</groupId>
			<artifactId>log4j</artifactId>
			<version>1.2.15</version>
			<scope>provided</scope>
			<exclusions>
				<exclusion>
					<groupId>javax.jms</groupId>
					<artifactId>jms</artifactId>
				</exclusion>
				<exclusion>
					<groupId>com.sun.jmx</groupId>
					<artifactId>jmxri</artifactId>
				</exclusion>
				<exclusion>
					<groupId>com.sun.jdmk</groupId>
					<artifactId>jmxtools</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<!-- JAXB support -->
		<dependency>
			<groupId>org.jboss.resteasy</groupId>
			<artifactId>resteasy-jaxb-provider</artifactId>
			<version>3.0.7.Final</version>
		</dependency>
		<dependency>
			<groupId>org.jboss.resteasy</groupId>
			<artifactId>jaxrs-api</artifactId>
			<version>3.0.7.Final</version>
		</dependency>
		<dependency>
			<groupId>net.sf.scannotation</groupId>
			<artifactId>scannotation</artifactId>
			<version>1.0.2</version>
		</dependency>
		
		<!-- Filter needed to allow requesting data 
		from my local server-->
		<dependency>
			<groupId>com.thetransactioncompany</groupId>
			<artifactId>cors-filter</artifactId>
		<version>2.1</version>
	</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>2.3.2</version>
				<configuration>
					<source>1.6</source>
					<target>1.6</target>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-war-plugin</artifactId>
				<version>2.1.1</version>
				<configuration>
					<failOnMissingWebXml>false</failOnMissingWebXml>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-dependency-plugin</artifactId>
				<version>2.1</version>
				<executions>
					<execution>
						<phase>validate</phase>
						<goals>
							<goal>copy</goal>
						</goals>
						<configuration>
							<outputDirectory>${endorsed.dir}</outputDirectory>
							<silent>true</silent>
							<artifactItems>
								<artifactItem>
									<groupId>javax</groupId>
									<artifactId>javaee-endorsed-api</artifactId>
									<version>6.0</version>
									<type>jar</type>
								</artifactItem>
							</artifactItems>
						</configuration>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>

</project>
